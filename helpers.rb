module Helpers
  TITLE = "Meagan Waller"

  def title
    return TITLE if @title.nil?
    "#{TITLE} &middot; #{@title}"
  end

  def latest_posts
    posts = Dir.glob("posts/*.md").map do |post|
      post = post[/posts\/(.*?).md$/,1]
      Content.new("posts", post)
    end
    posts.reject! { |post| post.date > Date.today }
    posts.sort_by(&:date).reverse
  end

  def latest_talks
    talks = Dir.glob("talks/*.md").map do |talk|
      talk = talk[/talks\/(.*?).md$/,1]
      Content.new("talks", talk)
    end
    talks.sort_by(&:date).reverse
  end

  def partial(page, options={})
    erb "_#{page}".to_sym, options.merge!(:layout => false)
  end

  def url_base
    "http://#{request.host_with_port}"
  end
end

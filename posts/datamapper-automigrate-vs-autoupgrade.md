---
title: "Datamapper AutoMigrate vs AutoUpgrade"
date: 2014-01-19
categories: ["Ruby", "Code"]
---

Taryn and I are using DataMapper for our ORM in our application that we're working on. We ran into a problem when we wanted to add new properties to the database. We weren't sure if we were supposed to use `auto_migrate` or `auto_upgrade`. They both seemed relatively similar, but there are differences.

Both methods are used to generate schema in the datastores that matches your model definition. However, `auto_migrate` destructively drops and recreates tables to match your model definitions and `auto_upgrade!` supports upgrading your datastore to match your model definitions, without actually destroying any already existing data.

If you want to completely drop everything and rebuild it, you'd want to use `auto_migrate!`, and if you want to upgrade without dropping things you'd want to use `auto_upgrade`. However, you can't always use `auto_upgrade`, the DataMapper documentation uses this example for when you can't use `auto_upgrade`: You can't change column length constraints from `VARCHAR(100)` to `VARCHAR(50)`, for example.

Here's an example utilizing the two methods from the documentation

````
require 'rubygems'
require 'dm-core'
require 'dm-migrations'

DataMapper::Logger.new($stdout, :debug)
DataMapper.setup(:default, 'mysql://localhost/test')

class Person
  include DataMapper::Resource
  property :id,   Serial
  property :name, String, :required => true
end

DataMapper.auto_migrate!

# ~ (0.015754) SET sql_auto_is_null = 0
# ~ (0.000335) SET SESSION sql_mode = 'ANSI,NO_BACKSLASH_ESCAPES,NO_DIR_IN_CREATE,NO_ENGINE_SUBSTITUTION,NO_UNSIGNED_SUBTRACTION,TRADITIONAL'
# ~ (0.283290) DROP TABLE IF EXISTS `people`
# ~ (0.029274) SHOW TABLES LIKE 'people'
# ~ (0.000103) SET sql_auto_is_null = 0
# ~ (0.000111) SET SESSION sql_mode = 'ANSI,NO_BACKSLASH_ESCAPES,NO_DIR_IN_CREATE,NO_ENGINE_SUBSTITUTION,NO_UNSIGNED_SUBTRACTION,TRADITIONAL'
# ~ (0.000932) SHOW VARIABLES LIKE 'character_set_connection'
# ~ (0.000393) SHOW VARIABLES LIKE 'collation_connection'
# ~ (0.080191) CREATE TABLE `people` (`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, `name` VARCHAR(50) NOT NULL, PRIMARY KEY(`id`)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci
# => #<DataMapper::DescendantSet:0x101379a68 @descendants=[Person]>
class Person
   property :hobby, String
end

DataMapper.auto_upgrade!

# ~ (0.000612) SHOW TABLES LIKE 'people'
# ~ (0.000079) SET sql_auto_is_null = 0
# ~ (0.000081) SET SESSION sql_mode = 'ANSI,NO_BACKSLASH_ESCAPES,NO_DIR_IN_CREATE,NO_ENGINE_SUBSTITUTION,NO_UNSIGNED_SUBTRACTION,TRADITIONAL'
# ~ (1.794475) SHOW COLUMNS FROM `people` LIKE 'id'
# ~ (0.001412) SHOW COLUMNS FROM `people` LIKE 'name'
# ~ (0.001121) SHOW COLUMNS FROM `people` LIKE 'hobby'
# ~ (0.153989) ALTER TABLE `people` ADD COLUMN `hobby` VARCHAR(50)
# => #<DataMapper::DescendantSet:0x101379a68 @descendants=[Person]>
````

Basically this shows that when using `auto_migrate` it's going to drop the table `people` if it exists, and then recreate it. With `auto_upgrade` it alters the table `people` and adds the `hobby` column to it.

---
title: "Using JRuby IRB"
date: 2014-01-02
categories: ["Code", "Tutorial", "Java", "Ruby", "JRuby"]
---

The Interactive Ruby Shell, or IRB, is a REPL for programming Ruby. You launch the program from the command line with `$ irb`, and you can execute Ruby commands with immediate responses in real-time.

If you're writing JRuby and want access to something like IRB, you're in luck, because of JRuby IRB. Follow along to learn how how to: use JRuby IRB, require jar files from Java applications, access Java classes and their methods, and see the obstacles I ran into to and how I fixed[<sup>1</sup>](#footnote-1) them.

Let's get started

## Requirements
If you'd like to follow along with this post you need to have:

- JRuby 1.7 (if you don't have this, run `$ rvm install jruby`)
- Java 7 [You can download it here](http://java.com/en/download/index.jsp)
- Ruby 1.9.3 or Ruby 2.0.0 (I'm using Ruby 2.0.0) (`$ rvm install ruby 2.0.0 / ruby 1.9.3`)

## Using JRuby IRB

We can now start JRuby IRB with `$ jruby -S irb`

Once we're inside of the jIRB session we can use `require` like we would use a Java `import`.  Check out what I did with the `BufferedReader` class:

````
irb(main):006:0> reader = java.io.BufferedReader
=> Java::JavaIo::BufferedReader
irb(main):007:0> reader
=> Java::JavaIo::BufferedReader
irb(main):008:0> reader.methods
=> [:java_send, :java_method, :inherited, :new, :singleton_class, :java_class, :field_writer, :"__persistent__=", :new_array, :__persistent__, :[], :field_reader, :field_accessor, :java_class=, :allocate, :superclass, :const_get, :private_constant, :public_instance_methods, :autoload?, :freeze, :class_variable_get, :const_missing, :included_modules, :==, :class_exec, :public_class_method, :ancestors, :instance_method, :<, :protected_method_defined?, :>, :===, :private_instance_methods, :hash, :class_variables, :public_constant, :<=, :method_defined?, :instance_methods, :class_variable_defined?, :name, :private_method_defined?, :const_set, :autoload, :include?, :protected_instance_methods, :module_exec, :module_eval, :<=>, :constants, :private_class_method, :class_variable_set, :to_s, :public_method_defined?, :class_eval, :>=, :remove_class_variable, :const_defined?, :handle_different_imports, :include_class, :java_kind_of?, :java_signature, :methods, :define_singleton_method, :initialize_clone, :extend, :nil?, :tainted?, :method, :is_a?, :instance_variable_defined?, :instance_variable_get, :instance_variable_set, :public_method, :display, :send, :private_methods, :enum_for, :com, :to_java, :public_send, :instance_of?, :taint, :class, :java_annotation, :instance_variables, :!~, :org, :untrust, :=~, :protected_methods, :trust, :inspect, :java_implements, :tap, :frozen?, :initialize_dup, :java, :respond_to?, :java_package, :untaint, :respond_to_missing?, :clone, :java_name, :to_enum, :singleton_methods, :untrusted?, :eql?, :dup, :kind_of?, :javafx, :java_require, :javax, :public_methods, :instance_exec, :__send__, :instance_eval, :equal?, :object_id, :__id__, :!, :!=]
irb(main):009:0> reader.class.ancestors
=> [Class, Module, Object, Kernel, BasicObject]
````

You can also call `#to_a` then use `Array#[]` on them.

````
irb(main):015:0> reader_methods = reader.methods.to_a
=> [:java_send, :java_method, :inherited, :new, :singleton_class, :java_class, :field_writer, :"__persistent__=", :new_array, :__persistent__, :[], :field_reader, :field_accessor, :java_class=, :allocate, :superclass, :const_get, :private_constant, :public_instance_methods, :autoload?, :freeze, :class_variable_get, :const_missing, :included_modules, :==, :class_exec, :public_class_method, :ancestors, :instance_method, :<, :protected_method_defined?, :>, :===, :private_instance_methods, :hash, :class_variables, :public_constant, :<=, :method_defined?, :instance_methods, :class_variable_defined?, :name, :private_method_defined?, :const_set, :autoload, :include?, :protected_instance_methods, :module_exec, :module_eval, :<=>, :constants, :private_class_method, :class_variable_set, :to_s, :public_method_defined?, :class_eval, :>=, :remove_class_variable, :const_defined?, :handle_different_imports, :include_class, :java_kind_of?, :java_signature, :methods, :define_singleton_method, :initialize_clone, :extend, :nil?, :tainted?, :method, :is_a?, :instance_variable_defined?, :instance_variable_get, :instance_variable_set, :public_method, :display, :send, :private_methods, :enum_for, :com, :to_java, :public_send, :instance_of?, :taint, :class, :java_annotation, :instance_variables, :!~, :org, :untrust, :=~, :protected_methods, :trust, :inspect, :java_implements, :tap, :frozen?, :initialize_dup, :java, :respond_to?, :java_package, :untaint, :respond_to_missing?, :clone, :java_name, :to_enum, :singleton_methods, :untrusted?, :eql?, :dup, :kind_of?, :javafx, :java_require, :javax, :public_methods, :instance_exec, :__send__, :instance_eval, :equal?, :object_id, :__id__, :!, :!=]
irb(main):016:0> reader_methods[4]
=> :singleton_class
irb(main):017:0> reader_methods[6]
=> :field_writer
````

## Accessing and Importing Jar Files

For my JRuby tic tac toe game that will allow a user to play either the Ruby or the Java version of the game with Sinatra, I need to be able to access my Java tic tac toe jar file. I wanted to test how this worked with jIRB, here's how it went:

````
irb(main):018:0> require 'java-ttt/play.jar'
=> true
irb(main):002:0> Board = Java::com.ttt.Board
=> Java::ComTtt::Board
irb(main):003:0> java_import 'com.ttt.Board'
=> [Java::ComTtt::Board]
irb(main):004:0> board = Java::com.ttt.Board.new(3)
=> #<Java::ComTtt::Board:0x79cda784>
irb(main):005:0> board = Board.new(3)
=> #<Java::ComTtt::Board:0x1a5f5e4d>
irb(main):006:0> board.methods
=> [:getSize, :empty, :getEmptySpaces, :is_empty?, :set_move, :setMove, :setSpaces, :set_moves, :"__jcreate!", :get_empty_spaces, :==, :undo_move, :"__jsend!", :getSpaces, :isEmpty, :create_spaces, :setMoves, :get_spaces, :emptySpaces, :equals, :set_spaces, :is_empty, :createSpaces, :spaces=, :spaces, :get_size, :undoMove, :empty?, :size, :empty_spaces, :get_class, :notifyAll, :notify, :toString, :notify_all, :clone, :finalize, :to_string, :hash_code, :getClass, :wait, :hashCode, :equals?, :initialize, :equal?, :java_send, :marshal_dump, :marshal_load, :java_method, :java_class, :to_s, :synchronized, :java_object=, :java_object, :to_java_object, :hash, :inspect, :eql?, :handle_different_imports, :include_class, :java_kind_of?, :java_signature, :methods, :define_singleton_method, :initialize_clone, :freeze, :extend, :nil?, :tainted?, :method, :is_a?, :instance_variable_defined?, :instance_variable_get, :singleton_class, :instance_variable_set, :public_method, :display, :send, :private_methods, :enum_for, :com, :to_java, :public_send, :instance_of?, :taint, :class, :java_annotation, :instance_variables, :!~, :org, :untrust, :=~, :protected_methods, :trust, :java_implements, :tap, :frozen?, :initialize_dup, :java, :respond_to?, :===, :java_package, :untaint, :respond_to_missing?, :java_name, :to_enum, :singleton_methods, :untrusted?, :<=>, :dup, :kind_of?, :javafx, :java_require, :javax, :public_methods, :instance_exec, :__send__, :instance_eval, :object_id, :__id__, :!, :!=]
irb(main):007:0>
irb(main):007:0> board.getSize()
=> 3
irb(main):008:0> board.empty
=> true
irb(main):009:0> board.getEmptySpaces
=> #<Java::ComTtt::Board:::0x32632915>
irb(main):010:0> board.is_empty?
=> true
````

However, I ran into some problems when I wanted to check out my `#setMove` method. The method takes a `char` and an `int` as arguments (or `char` and `String`), to place a move on the board.

````
irb(main):011:0> board.setMove('X', 5)
NameError: no method 'setMove' for arguments (org.jruby.RubyString,org.jruby.RubyFixnum) on Java::ComTtt::Board
  available overloads:
    (char,int)
    (char,java.lang.String)
	from (irb):11:in `evaluate'
	from org/jruby/RubyKernel.java:1119:in `eval'
	from org/jruby/RubyKernel.java:1519:in `loop'
	from org/jruby/RubyKernel.java:1282:in `catch'
	from org/jruby/RubyKernel.java:1282:in `catch'
	from /usr/local/Cellar/jruby/1.7.9/libexec/bin/jirb:13:in `(root)'
````

It seems that JRuby doesn't support `char` the way Java does, it casts them to a `RubyString`, and casts `int` to a `RubyFixnum`. I reached out to my Twitter followers to see if they had insight on what to do. I got a few responses saying to cast the `'X'` as a `char`, and I got an answer saying to use `#ord`. Using `#ord` got the behavior to work as expected[<sup>2</sup>](#footnote-2)

````
irb(main):012:0> board.getSpaces()
=> "---------"
irb(main):013:0> move = 'X'.ord
=> 88
irb(main):014:0> board.setMove(move, 4)
=> nil
irb(main):015:0> board.getSpaces()
=> "---X-----"
````
The output is what I expected, an 'X' on the board (`spaces`) in the 4th spot, like it is. However, using `#ord` to accomplish this didn't seem ideal. I want to avoid having to modify my existing code, because the Java game works as expected, the problem comes with JRuby.

## Reaching out for Help

I emailed my mentor, Mike, and asked for input on where to go from here. This issue I've run into is an interesting problem because my library, my Java TTT game, has a public API that requires a specific type that isn't the default support for JRuby. If I didn't own the jarfile for the game, I wouldn't be able to just simply modify the code, and I might be stuck.

There are basically two approaches to this problem. The first one would be to change my Java code to use a type more easily consumed by the client, in this case that's Sinatra. The second option would be to keep the conversion on the Sinatra side.


### Option One - Changing the API

If I were dealing with a 3rd party API I wouldn't even have this option, I'd have to conform to the API. But, I control the API so the question to ask myself is if there was a reason I am using `char` in the Java code, and if my code would be any less expressive if I used a type that was more easily consumed by the Ruby project.

However, the reason I'm using `char` is because in Java `String` is immutable, which means that you can't modify it without creating a new `String` object. My `#setMove()` method looks like this:

````
      public void setMove(char marker, int move) {
                StringBuilder newSpaces = new StringBuilder(spaces);
                newSpaces.setCharAt(move - 1, marker);
                spaces = newSpaces.toString();
        }
````

I'm using `StringBuilder` to create `newSpaces` from `spaces`. On the `newSpaces` I'm calling `#setCharAt(move - 1, marker)`, this will set the `char` ('X' or 'O') in the proper space (`move - 1` is to account for the 0-indexing). Finally I'm setting `spaces` equal to `newSpaces.toString();`

Even if I used `#substring` on a String, I'd still need the `marker` to be a `char` to modify the `String`. I could potentially just do the `String` to `char` conversion in  `#setMove` but that doesn't seem very clean. Especially because the Java game works correctly, it's the JRuby that requires the change.

### Option Two - Sinatra Conversion

With option two I would be doing the conversion on the Sinatra side, I could accomplish this by creating a wrapper class using the [Adapter Pattern](http://en.wikipedia.org/wiki/Adapter_pattern) to do the conversion to the correct type. This allows my Sinatra code to remain expressive because the conversion of the type is encapsulated in class that interacts directly with my Java code.

If you're not familiar with the Adapter Pattern it is a design pattern that translates one interface for a class into a compatible interface. The adapter allows classes to work together that normally could not because of incompatible interfaces, by providing its interface to clients while using the original interface. The adapter will translate calls to its interface into calls to the original interface, and the amount of code necessary to do this is typically pretty small.

## Now What?
I think that option number two, using the Adapter pattern, is probably the way to go. It feels the most clean and is a practice that is good to be familiar with since I'm sure I'll be dealing with 3rd party APIs at some point in my software career. I will give both a try and see which one I like more, which is more expressive, and which follows the Clean code/SOLID principles more.



<sup id="footnote-1">1</sup> Fixed is maybe not the right word -- how I got everything to somewhat play nicely and give me the expected behavior.

<sup id="footnote-2">2</sup> Well, it gave me the output I was looking for, I wasn't expecting `char` and `int` to be cast to `RubyString` or `RubyFixnum`.

---
title: "Sinatra and Datamapper"
date: 2014-01-13
categories: ["Ruby", "Sinatra"]
---

Once a week I'll be pairing with [Taryn](http://tarynsauer.tumblr.com/) on an internal application for 8th Light that we're working on. We're writing the application in Ruby and using Sinatra as the framework for now. The application pulls information in from a database so we had to figure out how we wanted to communicate with the database, this is where DataMapper comes in. Right now we're still not pushing any code to even a staging server so everything is using SQLite for now, however, I'm creating my blog in Sinatra alongside that and I am using PostgreSQL and SQLite3 for that.

DataMapper is an Object Relational Mapper, or ORM, written in Ruby. An ORM converts data been incompatible type systems in object-oriented programming languages. This basically creates a virtual object database that can used from within the programming language.

I use both PostgreSQL and SQLite3, PostgreSQL for production and SQLite3 for testing.

## Setting Up

I have been using a [this Sinatra template]( https://github.com/meaganewaller/sinatra-template) I made for my Sinatra projects, you can clone it/fork it.

````
# Gemfile

source "https://rubygems.org"

gem "sinatra"
gem "data_mapper"
gem "dm-postgres-adapter"
gem "pg"

group :test do
  ...
  gem "sqlite3"
  gem "dm-sqlite-adapter"
end
````

You're going to want to include the `data_mapper` and `dm-postgres-adapter` and `pg` gems for production, and in your test group, you'll want to include the `sqlite3` and `dm-sqlite-adapter`.

I use RSpec so in my `spec_helper` file I have

````
require 'sinatra'
require 'bundler'
Bundler.require(:test)

RSpec.configure do |c|
  c.include Rack::Test::Methods
  DataMapper::setup(:default, "sqlite3://#{Dir.pwd}/db/post_test.db")
  DataMapper.finalize
  Post.auto_migrate!
end
````

Next we need to set up our Postgres database, if you don't have Postgres installed already you can do so through homebrew

`$ brew install postgres`

Next you're going to want to initialize your database:

`$ initdb /usr/local/var/postgres`

And now we can create our database, mine is called `blog`

`$ createdb blog`

We can set up our postgres connection now:

My Postgres setup in my `init.rb` file from my Sinatra template looks like this:

````
require 'sinatra'
require 'data_mapper'

DataMapper.setup(:default, ENV['DATABASE_URL'] || 'postgres://localhost/myblog')

class Post
  include DataMapper::Resource

  property :id, Serial
  property :title, String, :required => true
  property :body, Text, :required => true
  property :created_at, DateTime, :required => true
end
DataMapper.finalize.auto_upgrade!
...
````

The `DataMapper.setup(:default, ENV['DATABASE_URL'] || 'postgres://localhost/myblog')` is creating my database connection, and the `Post` class is where I define my models. You can see more about that on the [DataMapper getting started page](http://datamapper.org/getting-started.html).

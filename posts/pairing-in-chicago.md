---
title: "Pairing in Chicago"
date: 2013-11-12
categories: ["Apprenticeship"]
---

Today I got to pair with another resident apprentice, Kelly, in the Chicago office. We paired on 8th Light's "Dojo Dashboard". The Dojo Dashboard is a dashboard that is on display in the Chicago office on the TV screen. It has multiple widgets, including weather of all of the 8th Light Dojos, the train schedule in Chicago, Twitter, and our Travis Build statuses for our projects.

I haven't had a lot of opportunities to come into a project and get to pair with someone on it, so I was nervous but also really excited to be doing something new to me. The application is written in Clojure and ClojureScript, both languages are ones that I'm not very familiar with. However, I picked them up pretty quickly, and had a lot of fun solving some problems with Kelly.

We decided to tackle adding a weather widget for our London office, as well as add icons for weather conditions. At the moment all we had was the temperature, and it'd be nice to know if it was raining, snowing, or sunny outside with a graphical representation.

The first thing we did was Kelly got me introduced to the project, she walked me through some of what was happening, and I asked a lot of questions and then we dug right in. We have silhouettes of the different states for our weather widgets, so we have one of Florida for the Tampa office, and one of Illinois for the Chicago office. So we attempted to find a vector silhouette of London, but didn't have much luck, so I threw one together in Illustrator, and Kelly introduced me to Live Trace, which is a pretty radical feature that I wasn't aware of.

After we got the silhouette finished, we added the widget to the dashboard, and we had a London widget showing up. We're using the Wunderground API for our weather and time, so we set it up for London as well, and then we ran into some trouble. We sat there dumbfonded for a bit, recompiling our ClojureScript, and meticulously checking for typos in our code. Kelly then checked her e-mail and found out that apparently we had been hitting the API too frequently and got our account suspended (uh-oh...). Well, I set up an account so we could at least get this tested and finished. After setting up my account, we were able to get the weather, time, and weather condition icons for all three offices! It was really exciting and lots of high fives and smiles were had.

Tomorrow I think we are going to attempt to figure out how to be smarter about how we use the API. Right now, we're hitting it 3 times per request, and if we could hit it once and get all three cities, that'd be ideal, since we only get 10 requests a minute.

I had a really great day pairing, and walked away feeling like I was starting to really understand Clojure and Clojurescript, which is an awesome feeling, since Clojure isn't something I've dabbled in too much.

Oh also, it snowed today(!!) and I saw snow for the first time ever, so that was pretty exciting too.

---
title: "Cargo Cult Programming"
date: 2013-05-21
categories: ["Apprenticeship"]
---

I like to write blog posts that are instructional. Will, my mentor, and I get together and he asks me questions about my posts. He wants to make sure that I actually understand what I wrote, as well as understand why the code works that way. I find these talks very informative. I usually discover that even though I thought I had a very good understanding of the subject I wrote about sometimes I have a hard time explaining why I approached the solution in a particular way. Will mentioned that I should look up the term cargo-cult programming. Upon reading the definition it was obvious to me why Will wanted me to learn about it.

> Cargo cult programming is an incompetent style of programming dominated by ritual inclusion of code or program structure that serves no real purpose. A cargo cult programmer will usually explain the extra code as a way of working around some bug encountered in the past. Usually neither the bug or the reason the code apparently avoided the bug was fully understood.

Right there in the first sentence of the definition is the term incompetent. That should be the first indication that cargo-cult programming is not desirable. The part of the definition that should also set off your red flashing lights that this is bad is: "usually neither the bug or the reason the code apparently avoided the bug was fully understood".

How could it possibly be okay to not understand how your code "fixed" a bug? When you don't understand the code how can you guarantee that it won't break something else? Can you be sure that you could remedy it if breaks in the future? If you don't understand what the code is doing today, how could you possibly expect to understand it months down the road if you're presented with another bug? The answer to these questions is a resounding "YOU CANNOT". Let me repeat that, YOU CANNOT GUARANTEE ANYTHING WHEN YOU PRACTICE CARGO CULT PROGRAMMING. Now that we're on the same page, let's move on.

## Back to Basics: Syntax and Semantics

If you're like me, you read the above definition and it hit a little too close to home. It seems harmless at first, you might even justify it by saying that you'll look into it later, how terrible could it be? You probably even have an understanding of what the code is doing. However, not understanding how the code works should be unacceptable for a developer. There are too many resources at our fingertips to feign ignorance.

If you look at a piece of code you shouldn't accept that it just works. The most important thing to understand is that programming languages should be seen as a flexible medium for expressing ideas and coming to a solution. However, I guarantee there is someone in the world right now that is trying to remedy a bug by making random changes, testing those changes, crossing their fingers, and if it doesn't fix the problem they just repeat the cycle until they manage to spit out something that works.

Imagine if you told a child that it was time to hit the road, they would most likely think they should literally hit the road. That is why when you first started learning English you didn't start off by learning idioms and entendres. Learning that way would be ridiculously confusing with no understanding of the syntax or semantics. Once you learn the syntax, the grammar and spelling rules that a language is comprised of, you are able to decipher sentences. The meaning of sentences is called semantics.

Learning a programming language is very similar to learning any new language. The syntax of a programming language describes the possible combinations of symbols that form a syntactically correct program. Semantics is the meaning of a combination of symbols. Just like how in English, syntax was the rules and structure, and semantics defined sentences. If you don't know the semantics that's when things start to get a bit hairy. In order to solve problems you must have a clear idea of the semantics needed to solve said problem, then you implement those semantics.

## How to Help Others

Maybe you are not a cargo cult programmer, perhaps you know someone who is guilty of it. You might wonder why don't they just get it. It's hard to remember what it's like to be in a beginner's shoes. Remember, there was a time when even the most experienced developer didn't know how to properly use a for loop, or maybe the idea of parameters eluded them for a bit. Being a cargo-cult programmer is rarely as simple as having a terrible habit that could be broken, instead it is usually a symptom of underlying confusion that the programmer is feeling.

When you come across a cargo-cult programmer, it could be someone who simply searches Google for their problems and clicks links until someone posts something that could possibly solve the problem. That behavior should be an indicator that they are overwhelmed by the complexity of either their own personal expectations, or the expectations of someone else.

A craftsman will read other's code when stuck on a problem. They might even come across a solution that could possibly be the solution. However, a craftsman will use their knowledge gained while reading the solution, not the actual work itself. Whereas, a cargo cult programmer might simply copy and paste a piece of code from a place like a blog or a forum. The programer is instinctively using someone else's work as a platform on which to build their program. When that happens we can determine they have a gap in their knowledge. They have broken down the code as far as their understanding will allow and they are now treating the large blocks of code as atomic units. This happens when knowledge only encompasses what the blocks of code do and not an understanding of how they work.

## Practical Application

You have two choices, you can either be taught to fish, or someone can just give you the fish. Being taught to fish will require hard work, patience, personal responsibility, and possibly failure. Being handed a fish will satisfy your appetite temporarily. However, being taught to fish will ensure that next time you get hungry you will be able to fend for yourself. Being handed a fish is great, until you inevitably get hungry again.

Every piece of code has a meaning. Your responsibility as a developer is to understand the meaning. You could keep tweaking your code, poking it and prodding it until it does what you want it to do. Or, you could learn how the code works and solve the problem easier next time, and even easier the time after that.

I am going to work on exercises that I think will help me to widen my knowledge:

Writing code is important, but it's not all that you should do as a developer. Another key skill of craftsmen is being able to read other people's code. This is an exercise that should be done with a person who is also a developer. They should preferably be a craftsman. Your exercise is to read their code. You should use comments in plain English, do it line by line if necessary, which explains the program.

After you've explained the code to your reviewing developer. They should instruct you to modify the code to make a specific change. It can be something as minor as having a sort method that sorts by last name, first name now sort by first name, last name. Or it can be more demanding. The most important part of deciding on the modification is that it should require understanding the code.

This next exercise can involve a peer, mentor, or even a family member (they don't need to be techy!) You will share with them code you've worked on. It doesn't have to be extravagant, or groundbreaking. It can be a simple game, solving a math formula, maybe do a code kata. The point is that you're going to explain, line by line what the code is doing. I have found that when I took the time to explain a program I wrote to my (totally non-technical) mother I was able to pinpoint things I didn't quite understand because I couldn't explain them. I also found out that I knew a lot more than I thought I did because I was able to explain it to my mother in a way that she was able to understand and ask intelligent questions about it.

Pairing is also going to be an important exercise in widening your knowledge. Pair with someone and try and solve a problem that should take a maximum of 15 minutes. Repeat this problem, introducing new twists as you become more proficient.

Along with pairing you should be doing code reviews. THey don't have to be formal, or even in person. There are plenty of people all over the world who would be happy to exchange and review code.

The most important thing for me to remember is that when working as an apprentice, the goal isn't to simply crank out project after project. The actual process is crucial in these beginning stages. Understanding why you took a certain step is more important now than just solving the problem. I need to remember that when it comes to these exercises I'm not doing them because the world needs another Tic-Tac-Toe game. I'm doing these exercises because they help to build the knowledge necessary to build apps that a client needs, and perhaps maybe even one the world needs.

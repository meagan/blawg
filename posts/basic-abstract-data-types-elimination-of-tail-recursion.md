---
title: "Elimination of Tail Recursion"
date: 2013-07-09
categories: ["Tutorials"]
---

In my last post, we talked about basic abstract data types. We touched on stacks, queues and the Array implementation of them both. This blog post is going to assume that you're familiar with those concepts, if you're not I suggest reading my last post. If you'd rather a quick refresher, I will include that here:

## Quick Refresher

A stack is a type of list where insertions and deletions take place at the top. Stacks are also known as LIFO, or Last-In-First-Out. Meaning, that the last (most recent) item placed on the stack is the first to come off the stack.

A queue is FIFO or First-In-First-Out. Queues are much like lines that we form at an amusement park. The first people that enter the line, are the first ones that get to go on the ride.

## Stacks and Recursive Procedures

An important application of stacks is in the implementation of recursive procedures. Every language that allows recursive procedures uses a stack of activation records to record the values for all the variables belonging to each active procedure of a program. But, what does that mean?

An activation record is a lot like a return address on an envelope. You put the invitation in the envelope, but the recipient's name and address on the front and center of the envelope, press a stamp into the top right corner, and in the top left corner you put your address as the return address. The return address lets the post office know where the mail came from, and if it needs to be sent back to the sender the address tells where to send it to.

Recursion simplifies the structure of many programs. However, sometimes a program may run faster by a large constant factor if we eliminate recursive calls from it. However, this is not to say that we should always be eliminating recursion, because often times the simplicity of the code is worth the running time. However, in the most frequently executed parts of the program it may be beneficial to eliminate recursion. This blog post is going to go in depth on how to convert recursive procedures into non-recursive ones by the introduction of user-defined stacks.

## Elimination of Tail Recursion

In tail recursion, the calculations are performed first, and then the recursive call is executed, passing in the results of the calculations.

An example is usually the best way to see this in action, so let's get to it:


````
def tailrecursivesum(x, total = 0)
  if x == 0
    return total
  else
    return tailrecursivesum(x - 1, total + x)
  end
end

tailrecursivesum(5)
#=> tailrecursivesum(5, 0)
#=> tailrecursivesum(4, 5)
#=> tailrecursivesum(3, 9)
#=> tailrecursivesum(2, 12)
#=> tailrecursivesum(1, 14)
#=> tailrecursivesum(0, 15)

#=> 15
````

After I made the call to the method, I showed what exactly is going on. In this program, we are adding all the numbers, counting down to 0.

Now, that we see what tail recursion is, let's see how we can eliminate it.

````

def eliminatetailrecursion(x)
  if x == 1
    return 1
  else
    return x + eliminatetailrecursion(x - 1)
  end
end
````

Now, we don't have tail recursion anymore since the last thing we are doing isn't a recursive call, it's actually a calculation of a sum. Let's use a user-defined stack to eliminate this recursion now


````
def example(x)
  result = 0
  stack = []
  stack.push(x)
  while !stack.empty?
    x = stack.pop
    if 1 == x
      result += 1
    else
      result += x
      stack.push(x - 1)
    end
  end
  return result
end
````

Now, with the introduction of user defined stacks we have eliminated the tail-recursion.

## Further Reading

However, Ruby doesn't really perform Tail Recursion Elimination, or Tail-Call Optimization (TCO). To read more about it, there's a great stackoverflow answer here, as well as a presentation here that is about how to use Ruby like a functional language and includes information about TCO, and Recursion.

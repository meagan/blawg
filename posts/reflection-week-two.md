---
title: "Reflection on Week Two"
date: 2013-04-19
categories: ["Apprenticeship"]
---
## Ways I Grew
I think I've made a lot of progress in the last week. Last week I was having a very hard time grasping tests, and some basic aspects of Ruby.
However, this week I made an effort to make every post I made informative so I could learn more about Ruby. My time management skills improved some this month,
however, I still have a ways to go. I started on my book review for the book Software Craftsmanship by Pete McBreen. My goals for last week were to ask more questions, this week I did a lot better on that.
I asked questions when I needed help, and now I am starting to use vim over my IDE. I could definitely do better on keeping up with housekeeping and chores, however, I'll continue to work on that.
I haven't gotten more sleep, maybe less than last week actually, I just get so caught up in what I'm doing and when I look at the clock it's past midnight. I need to do better still in reading my books in small, manageable chunks, I still am waiting until the last minute which needs to stop.
I did write my blog posts every single day this week despite being sick yesterday and staying home from work. Writing my blog posts is the punctuation on my day, it's my way of wrapping up my work day, be right when I get home, or close to midnight. I
now thoroughly look forward to writing my posts because it's an opportunity to learn more about Ruby, or testing. I've also done a lot better with using TDD over BDUF. I currently have 19 passing tests for my tic tac toe, and plan on finishing it up in the near future. My next goal will be to tackle the AI.

## Challenges I Faced
My biggest challenges this week continued to be time management, as well as managing my home and work life, including my sleep schedule and other work assignments. I can tell this is going to be one of my biggest challenges, like I said last week, I can not overwork myself for fear of possibly burning out.
I need to get more organized, I know that this will help with how I feel about my self, as well as improve the quality of my work. This is an important thing I need to work on.

## Goals for Next Week

My goals for next week are

1. Continue asking questions when I have them
2. Keep up with my chores and housekeeping
3. Start becoming more organized, keep track of appointments, assignments, and bills.
4. Keep up with my reading assignments
5. Write a blog post every day during the week
6. Finish my Tic-Tac-Toe and start on my AI.

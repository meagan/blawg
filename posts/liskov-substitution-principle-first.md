---
title: "Liskov Substitution Principle"
date: 2013-10-29
categories: ["Java", "SOLID Principles", "Code", "Tutorial"]
---

 The SOLID principles are very important principles when it comes to object-orientated programming and design. The principles, when applied correctly and together help to create systems that are easier to maintain and extend over time. The principles of SOLID are guidelines that can be applied while working on software to remove code smells by showing the programmer when and where to refactor code until it is both readable and extensible.

This post is going to be about the L in SOLID. The L stands for Liskov Substitution Principle, or LSP. LSP states that if S is a subtype of T, then objects of T may be replaced with objects of type S without altering any of the desirable properties of that program.

What this means is that functions that are designed to operate on a given type of object should work without modification when they operate on objects that belong to a subtype of the original type. With a dynamic language, like Ruby, LSP works slightly different because Ruby has "duck typing", which is different than Java, where type if enforced by the compiler. This means that LSP winds up applying more to which messages an object responds to and not really its type.

A typical violation of LSP that gets brought up frequently is this example:

A Square class derives from a Rectangle class, assuming we have getter and setter methods for both width and height. The Square class will always assume that width is equal with height. But, if a Square object is used where we expect a Rectangle, unexpected behavior happens because the dimensions of a Square shouldn't be modified independently. However, if Square and Rectangle had only getter methods, making them immutable then no violation of LSP could occur.

I've wrote a quick example of the Square/Rectangle problem, using RSpec to test:

````
class Rectangle
  attr_reader :width, :height

  def initialize(width, height)
    @width = width
    @height = height
  end

  def area
    @width * @height
  end
end

class Square < Rectangle
  def initialize(side)
    super(side, side)
  end
end


describe "testing LSP" do
  it "returns right area for a rectangle" do
    rect = Rectangle.new(10, 8)
    rect.area.should == 80
  end

  it "returns the right area for a square" do
    sq = Square.new(5)
    sq.area.should == 25
  end
end
````

Using attr_reader gives getters but not setters, immutability. This implementation means that both Rectangle and Square can get height and width, for a Square those will always be the same, but the area is always height * width.

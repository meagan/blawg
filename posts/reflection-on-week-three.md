---
title: "Reflection on Week Three"
date: 2013-04-23
categories: ["Apprenticeship"]
---

## Ways I Grew

From last week until this week I feel like I've progressed a lot. However, I think that if I don't end up saying that in each of my Friday reflections posts I'm kind of missing the whole point of an apprenticeship. I used the website [Codecademy](http://www.codecademy.com/) briefly before my apprenticeship. It was my way of getting my toes wet with Ruby. I didn't continue on past the first 3 courses or so. However, today I found myself poking around on there and finishing lessons and courses with ease. Every problem was something that I knew how to solve, or at least knew what to look for to solve. That was exciting because just 3 weeks ago I was having a difficult time with something, and today it wasn't a problem at all. This week I decided to take a step back from the Tic Tac Toe and work on some Katas. I think I needed to not look at it for a day. Working on the Katas has helped me to try and *fail faster*, it's something I still need to work on, but at least I'm recognizing when I'm *not* doing it and can make the conscious effort to do it.

## Challenges I Faced
My biggest challenges are probably the same that they have been: my time management skills. I find myself staying up later because my brain is racing with ideas and possibilities. I find myself thinking in code constantly. I wouldn't say either of those things are necessarily bad in and of themselves, but when they keep me awake at night, they just might be. I'm also still struggling with my expectations not being in line with reality. I've touched on that so many times that I'm not going to rehash it here, but just know that it is still a concern. My other challenge, I mentioned it in the first paragraph, is not failing fast enough. I kept hearing it, and seeing it *fail fast to get better*, I *was* failing, why did I still feel so frustrated? Well, when I'm getting in my head and thinking up every single way to avoid this code from doing something way down the line when I haven't even typed a single line is not the best way to cure frustration. Failing fast seems simple in theory, just write whatever I think will work to pass that one particular test, the simplest thing, **even if it might not be applicable further down the road**. If all I need for that one particular test to pass is for it to print the number 1, I'll have it literally print the number 1. I don't have it print anything except the simplest thing to pass that test, even if that number 1 will eventually stand for a variable. I don't worry about it right now, I just need this one particular test to pass. Thinking like that is a challenge I am currently facing, but I think with some practice I'll find myself failing fast, and getting better (never thought I'd say that sentence).

## Goals for Next Week

1. Fail Faster!
2. Complete atleast one Kata using only TDD
3. Read "The Pragmatic Programmer"

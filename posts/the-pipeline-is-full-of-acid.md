---
title: The Pipeline is Full of Acid
date: 2014-06-19
categories: ["Technology", "Diversity"]
---

**This blog post originally appeared on [8th Light's Blog](http://blog.8thlight.com/meagan-waller/2014/06/19/the-pipeline-is-full-of-acid.html)**


Last week, my co-worker Doug Bradbury posted on the 8th Light blog to [ask about
gender equality on programming
teams](http://blog.8thlight.com/doug-bradbury/2014/06/09/gender-equality-is-not-enough.html).
I've had some thoughts about his post for a couple of days, but I'd like to take a step back first. Before we ask whether our teams would be srtonger with more diversity, we should ask why our teams lack diversity in the first place.

This past spring semester I taught a Girls Who Code club class at The Chicago
Tech Academy. It was an incredibly rewarding experience. Being given the
opportunity to teach a group of high school girls some basic programming skills
and see them flourish and get excited over it nearly brought me tears of joy a
couple of times. However I started thinking that putting a bandaid on the
problem, simply teaching girls to code, isn’t going to solve it.
There are amazing women programmers who have still been threatened, sent death
and rape threats, pushed out of the field, and have left technology all
together. Being able to program didn’t help them. Being able to write code, even
write code really well, didn’t prevent it from happening.

<center><blockquote class="twitter-tweet" lang="en"><p>OH: Should we be encouraging
women to get into the pipeline when we know the pipeline leads to a sewage
treatment plant?</p>&mdash; despondent (@ashedryden) <a
href="https://twitter.com/ashedryden/statuses/392052865709002752">October 20,
2013</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></center>

Before we can start asking why we need more marginalized folks on our teams, we
have to ask how we can fix the toxicity that is the technology workforce.
Throwing more marginalized people on our teams but never addressing the root
factors, the ingrained sexism, misogyny, racism, homophobia, transphobia, and
ableism, means we’re putting the responsibility of fixing the culture solely on
those who are most affected by it.

We already know that the attrition rates for women in technology are [way too high](http://www.forbes.com/sites/bruceupbin/2014/05/12/the-tech-sector-needs-more-women-heres-how-you-can-make-it-happen/). Why do we think that just adding more women to our team will solve this problem? I never hear anyone say that they should fix their totaled car by adding a new coat of paint, or giving it a new set of tires.

I'd be lying if I said that the thought of leaving the technology field hasn't crossed my mind more than once. Navigating online and offline spaces is very different as a woman. I've been harassed, stalked, and threatened when I denied advances from men at conferences, Meet Up groups, on Twitter, and email. These are spaces that many people take for granted, and in many cases these are the tools that professionals use to network, find a job, and even just make friends. When someone feels uncomfortable or unwelcome in a group, they're unlikely to continue showing up. And when they don't show up to these public settings, their professional career suffers because they don't have the same opportunities to advance. Many marginalized people leave the field entirely because of this.

To answer Doug's question: no, gender equality isn't enough. But maybe not for the reasons that you think. Simply demanding a ratio to fulfill some diversity quota isn't going to fix the broken culture that we work in. Adding more women to our teams, adding more marginalized folks to our teams, and patting ourselves on the back isn't helpful, and it's not a solution.

For members of the dominant group in the technology field (a white, straight, cisgender, heterosexual male), questions about diversity can quickly verge on thought exercises, questions that feel good to ask. Instead, let’s ask questions that make us uncomfortable. Instead of asking why more women on our teams would be good, let’s ask why there aren’t women on our teams already. Instead of saying we should focus on individuals and not on entire groups of people, let's ask why all of our recent hires are from the same group of people. It’s great to get more marginalized people into the technology field, but if we’re not actively working to remove the acid from the pipeline, what’s the point?

If you’re interested in helping to fix the toxic culture, check out my website:
[Days Since Last Tech Incident](http://www.dayssincelasttechincident.com) for resources, blog posts, and information on how to do just that.

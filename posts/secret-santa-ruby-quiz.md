---
title: "Secret Santa Ruby Quiz"
date: 2014-01-20
categories: ["Ruby", "Code"]
---

During this phase in my apprenticeship my main focus is to write lots of Ruby code. Mike wants me to be familiar with lots of different problems in Ruby and how to solve them. I've been doing some of the [Rubeque exercises](http://rubeque.com) and have picked out some of the [Ruby Quiz exercises](http://rubyquiz.com) to work on as well.

I decided to tackle the [Secret Santa Ruby quiz](http://rubyquiz.com/quiz2.html). The problem seems pretty easy, given an input that looks like this `FirstName LastName <Email>` for multiple people you need to match everyone up with a secret santa, and no one can get their own family member as a secret santa. As an added feature, you should send each person an email with their santa assignments.

My ways of approaching this problem varied. I tried to tackle it from different angles, should I do the input and parsing first or should I focus on how to send emails? Should I start with a person class, a family class, a Santa class? Should I use classes at all? How do I deal with randomness, how do I test randomness? These are all questions I had before I even wrote a single line of code.

Yesterday Mike and I met up at this coffee shop near my house called Cafe Mustache and talked about this problem. We also were able to pair on it a bit and Mike helped out with a possible way to approach this problem since I felt like I was just running into walls at every turn. He suggested that instead of focusing on sending the email or even parsing the input we should tackle the logic of it all. How does someone get assigned to someone else?

After writing some simple tests, and with TDD you write the simplest test, we had a nice test suite shaping up.  We tested and asserted correctly that given two people it assigns them to each other for secret santa. When we got to three people we noticed some resistance and our first problem to tackle that maybe wasn't considered going into it.

Say you have Alice, Bob and Carol. None of these three people can have themselves for secret santas. Now based on the method we were using to assign Santas sometimes it would work out fine:

   - Alice gets Bob
   - Bob gets Carol
   - Carol gets Alice

However, sometimes we were running into this:

   - Alice gets Bob
   - Bob gets Alice
   - Carol gets nil

Since Carol can't have herself as a secret santa and there was no one left, nil would be returned and Carol would be stuck with no one. This was tricky because upon running the tests the first time everything was passing, but we just got lucky.

In order to remedy this I needed to do something like this:

````
def assign_santas
   @santas = @people.map do |person|
    {
        :assignee => person,
        :assignment => assignment_for(person)
    }
   end
   if not_everyone_assigned?
      @assignments = []
      assign_santas
  end
  @santas
end
````
The `assignments` array is built up in the `assignment_for` method, so after the mapping is done and everyone has been assigned we check the method and it will return true if `nil` is included in the `assignments` array. If that returns true we reset the `assignments` array and rerun the `assign_santas` method.

Our next problem was that no one can be assigned to their family members, in order to solve this it took a bit of ugly code and I'm still working on refactoring that. Basically I had to do another check after the `not_everyone_assigned?`. That looks like this:

````
(0..@santas.size - 1).each do |num|
    if @santas[num][:assignee][:last_name] == @santas[num][:assignment][:last_name]
        assign_santas
    end
end
````
This method is checking to make sure that for each santa in the `santas` array that the `assignee`'s last name isn't the same as their `assignment`'s last name. If it is, then we need to redo the `assign_santas` method.

Now when I run my test suite to verify that it assigns for 2 people, for 3 people, that everyone always has a match, and that it doesn't assign family members to each other all my tests pass.

This problem made me really think about TDD and made me think about Ruby in a different way. I have shied away from hashes and have opted to use arrays instead in the past for whatever reason. So using an Array of hashes and figuring out how to access the keys inside of it for comparisons required me having to learn and put into practice some debugging skills and how to take advantage of `p` statements to print what's happening when a `binding.pry` simply just won't cut it.

I had a lot of fun (and frustrations) with this Ruby Quiz and although I'd love to finish off the problem and get emails going I think it might be time to move on to a new Ruby Quiz tomorrow.

---
title: "Liskov Substitution Principle"
date: 2014-01-13
categories: ["Ruby", "SOLID Principles", "Code", "Tutorial"]
---

The Liskov Substitution Principle (LSP) is the "L" in the SOLID principles. It states that:

> "If S is a subtype of T, then object of the type T in a program may be replaced with objects of type S without altering any of the desirable properties of that program."

Basically, objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program. The way we use LSP in Ruby is different than the way it's used in Java. With Ruby types aren't strictly enforced, but in Java they are. In Ruby LSP applies more to which messages an object responds to and not really its type.

Let's say, for example, we have a `Rectangle` and a `Square` class, where `Rectangle` is a superclass of `Square`.  `Rectangle` and `Square` both have width and height, but the way the width of the `Rectangle` and the width of the `Square` are set is different.

````
class Rectangle
  attr_accessor :width, :height
end

class Square
  attr_accessor :side

  def height=(height)
     @side = height
  end

  def width=(width)
     @side = width
  end

  def width
    @side
  end

  def height
    @side
  end
end
````

With the LSP we say that since we consider `Square` a subtype of `Rectangle` any function that acts on a `Rectangle` should work with classes, however this wouldn't work:

````
shapes = [Square.new, Rectangle.new]

def set_rectangle_width(shapes)
   shapes.each do |shape|
      shape.width = 5
      shape.height = 6
   end
end
````

This won't work for the `Square`. We could fix this fairly easily -- but if a derived class makes us change our base class our design might not be the best. This is why LSP and OCP (Open Closed Principle) are so closely related. .

Maybe we should reconsider `Square` inheriting from `Rectangle` to begin with. [Uncle Bob says this on the topic](http://cleancoders.com/codecast/clean-code-episode-11-p1/show):

> "The problem is this is not a Rectangle. It's a piece of code! It's a representation of a Rectangle. Representatives do not share the relationships of the things they represent."

Objects in a program are not objects, they are representations of objects.

## Read more on this:

- [Article on LSP on Object Mentor](http://www.objectmentor.com/resources/articles/lsp.pdf)

- [Subtyping, Subclassing and Trouble with OOP](http://okmij.org/ftp/Computation/Subtyping/)

- [SOLID - Liskov Substitution Principle](http://blog.sanaulla.info/2011/11/28/solid-liskov-substitution-principle/)

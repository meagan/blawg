---
title: "JRuby with Sinatra"
date: 2013-12-30
categories: ["Ruby", "JRuby", "Code", "Tutorial"]
---


My task this week is to take my [existing Sinatra/Ruby implementation of Tic Tac Toe](http://tttsinatra.herokuapp.com/) and add a feature so a user can choose whether they want to play the Java version of the Tic Tac Toe game, or the Ruby version. In order to implement this I need to use JRuby. If I run my Sinatra application with JRuby I can run my Tic Tac Toe jarfile and call out to use its routes if a user wants to play the Java version of the game.

## What is JRuby
JRuby is an implementation of Ruby on top of the Java Virtual Machine, and its written largely in Java. JRuby, being integrated with Java, allows the embedding of the interpreter into any Java application with full two-way access between the Java and Ruby code. This also means that JRuby hangs on to a lot of the things that make Ruby so awesome to write, like dynamic typing, while allowing Ruby programs to use Java classes.

## Running Sinatra with JRuby
Now that we know a little bit more about JRuby, let's look into how to run a Sinatra application with JRuby.

### A New Sinatra Application
First, you need to install JRuby:

````
$ rvm install jruby
````

After JRuby is installed, we need to make sure we're using JRuby with rvm

````
$ rvm use jruby
````

Now, create a new directory where you'll store the project, I'm going to use `/jrubysinatra`. Now, install Bundler using JRuby

````
$ jruby -S gem install bundler
````

*You can use `jruby -S` for any Ruby command-line tool.*

Now inside our project directory, we need to create our [Gemfile](https://gist.github.com/meaganewaller/8192550#file-gemfile).

````
# Gemfile
source "https://rubygems.org"
gem 'sinatra'
````

Now, we need to use bundler to install our Sinatra gem.

````
$ jruby -S bundle install
````

Now we need to write our Sinatra application. Name it [app.rb](https://gist.github.com/meaganewaller/8192550#file-app-rb) in the directory we created earlier.

````
require "sinatra"

get "/" do
    "Hello, world this is JRuby!"
end
````

Now, all we have to do is just run our application, we do this like so:

````
$ ruby path/to/app.rb
[2013-12-30 23:07:54] INFO  WEBrick 1.3.1
[2013-12-30 23:07:54] INFO  ruby 1.9.3 (2013-12-06) [java]
== Sinatra/1.4.4 has taken the stage on 4567 for development with backup from WEBrick
[2013-12-30 23:07:54] INFO  WEBrick::HTTPServer#start: pid=78406 port=4567
````

Now, open [localhost:4567](http://localhost:4567) in your browser and your Sinatra application should display your message using JRuby.

We can also use [config.ru](https://gist.github.com/meaganewaller/8192550#file-config-ru) and rackup to run our application

````
# config.ru
require 'sinatra'
require File.expand_path '../app.rb', __FILE__
run Sinatra::Application

````

Then launch with this command

````
$ rackup config.ru
[2013-12-30 23:11:02] INFO  WEBrick 1.3.1
[2013-12-30 23:11:02] INFO  ruby 1.9.3 (2013-12-06) [java]
[2013-12-30 23:11:02] INFO  WEBrick::HTTPServer#start: pid=78540 port=9292
````

Now navigate to [localhost:9292](http://localhost:9292) and you should see the game message as you saw when we ran the app without a config.ru file.

## Wrap Up
This is the first step towards completing my task. Now that my Tic Tac Toe Sinatra application can run using JRuby my next task is to figure out how to run a jar file with JRuby so I can implement the feature.

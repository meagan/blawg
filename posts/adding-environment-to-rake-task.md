---
title: "Adding Environment to Rake Task"
date: 2014-02-16
categories: ["Tutorial", "Ruby", "Code"]
---

This week Taryn and I wrote some custom rake tasks to pull in applicants from Highrise to populate our database. While we were testing our rake tasks we kept running into errors, `path doesn't exist for Nil:NilClass`. We dug into ActiveResource source code putting `puts` statements everywhere to see what was going on to no avail. Eventually we stumbled on our answer, our rake task didn't include the `environment` variable. By creating a rake task that looks like this:

````
desc "Rake task description"
task :task => :environment do
   puts "My rask taks does this thing!"
end
````

You get access to your models, and your entire environment by making your tasks dependent on the environment task. This allows us to do things like: `rake RAILS_ENV=staging db:task`. Our task wasn't properly running because we weren't getting access to our api url path because it was loading in an initializer, but our rake task didn't know about it. After adding `environment` to our rake task we were able to successfully pull in our applicant information from Highrise.

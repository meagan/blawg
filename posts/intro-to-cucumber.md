---
title: "Intro to Cucumber"
date: 2013-04-16
categories: ["Apprenticeship"]
---

Yesterday's blog post was really fun for me to write. I find that through explaining something it has a tendency to be something that I'll remember, imagine that.
Anyway, I woke up to a tweet from one of my mentors, Cory that said "Starting her 2nd week as an apprentice and w/Ruby, and I think @meaganewaller already knows more than many Ruby devs!" and included a link to my blog.
That was so encouraging and an awesome way to wake up, especially given that last week I was freaking out thinking I wasn't making enough progress. Today I got to talk to Cory briefly where he continued to tell me that I'm making
a lot of progress, I guess that just goes to show just how high my expectations are for myself. This morning I had my other mentor, Will, look over some of my progress and he explained what I was doing well, and what I should avoid doing.
I was writing test codes that, as he put it, was "too much like a recipe", versus just telling someone to cook or bake something and letting them decide the way to do it.
For example, I wrote that my board would display spaces in an array, which reads "like a recipe", what if down the road I no longer want to display spaces in array, now that test will fail. However,
if I just wrote that the board displays spaces, then no matter what I change to the production file, it will still pass the test, as long as it's displaying the spaces somehow.
I showed Will the way I was planning my board, and I was just saving all the steps I would take in a word document. This is verging on Big Design Up Front (BDUF\), which is to have a big detailed design that is created before coding and testing takes place.
I was getting stuck on an absolute certain way before I ever wrote a line of code. Instead of listening to my tests and letting the code take me where it may, I was now trying to write code for a design that doesn't exist anywhere except for in my head, or in my word document.
Will then told me I might want to try out Cucumber for my tests, which I could use alongside rSpec. The way my *pseudocode* was set up nearly mimicked Cucumber, so I might as well turn my familiar way into planning into code that could be tested.

## About Cucumber
Cucumber is a framework for testing using plain-text functional descriptions. The language Cucumber uses is called Gherkin, the advantage of using a plain-text language is that the code can be understood,
or even written, by people who are involved in the project that are non-technical. Cucumber is a BDD, or Behavior Driven Development.
From [The RSpec Book](http://pragprog.com/book/achbd/the-rspec-book):
### The Three Principles of BDD
1. **Enough is Enough**: do only as much planning, analysis, and design are you need, no more.
2. **Deliver stakeholder value**: everything you do should deliver value or increase your ability to do so.
3. **It's a behavior**: everyone involved should be able to talk about the system and what it does.
However, even though Cucumber is a testing tool, its purpose is to support BDD. The Cucumber tests are written before anything else
and verified by everyone involved in the project, including non-technical stakeholders. The production code is written from the outside-in so the stories pass.

## Getting Started with Cucumber
First and foremost, install Cucumber using Rubygems. <code>gem install cucumber</code>, to make sure it's installed run <code>cucumber --help</code>,
if a help section appears in your terminal you've successfully installed Cucumber, poke around a bit in the help section to see what's in store.
### Features
A feature is what your software does, or should do, and corresponds to a user story. The way a feature looks is:
{% highlight ruby %}
Feature: <description here>
    <story>

    <scenario 1>
    ...
    <scenario n>
{% endhighlight %}

The description section is where there is a plain text description of the story. While you can use any format to do this, it's best to be consistent,
this is one of the more common templates.
{% highlight ruby %}
As a <role>
I want <feature>
so that <business value>
{% endhighlight %}

This format focuses on three important questions:

1. Who's using the system?
2. What are they doing?
3. Why do they care?

An example of a story, something I used for my Cucumber file is
{% highlight ruby %}
As a player
I want to see the board
So I can make moves
{% endhighlight %}

This says that the user is a player, what they want to do is see the board, and why is so they can make moves.

### Scenarios
A feature is defined by one or more scenarios. A scenario is a sequence of steps through the feature that exercises one path. Cucumber uses the format of given-when-then.
A scenario is made up of three sections related to three types of steps:

1. **Given**: Sets up preconditions for the scenario, if you're familiar with rSpec it works like the <code>before</code> blocks.
2. **When**: This is the action that the feature is talking about, the behavior we're focused on.
3. **Then**: This checks your postconditions, it verifies that the right thing happened in the When step.

The scenario looks like this:
{% highlight ruby %}
Scenario: <description>
    <step 1>
    ...
    <step n>
{% endhighlight %}

Here's an example of a scenario from my Cucumber tests for my board.
{% highlight ruby %}
Scenario: Create a board
    Given game started
    When the board is created
    Then the board is empty

# Here's a scenario example
Scenario: start game
    Given I am not playing
    When I start a new game
    Then they game should say "Welcome to CodeBreaker"
    And the game should say "Enter guess:"
{% endhighlight %}

Notice the <code>And</code>, it can used in any of the three sections, it is shorthand for repeating any of the three steps (<code>Given</code>, <code>When</code> or <code>Then</code>).
In the above code you could've used <code>Then</code> in place of the <code>And</code>, however that doesn't read quite as naturally.
If you want to state a negative <code>Then</code> step, you can use <code>But</code> in place of <code>Then</code>. There are no limits on the number or even the order of steps.
It is best to keep scenarios as simple as possible, and to have multiple simple scenarios. A good rule of thumb seems to be the closer to the given-when-then format you are, the better.

### Adding Steps
Take the feature we used earlier.
{% highlight ruby %}
Feature: Create a board
    As a player
    I want to see the board
    So I can make moves

Scenario: Printing the board
    Given the game is being played
    When the game is in progressGiven
    Then print the board
{% endhighlight %}

Save your cucumber test in /features/create_board.feature and run the command <code>cucumber</code> in the directory.
You will see your feature echoed followed by
{% highlight ruby %}
1 scenario (1 undefined)
3 steps (3 undefined)
0m0.004s

You can implement step definitions for undefined steps with these snippets:

Given(/^the game is being played$/) do
  pending # express the regexp above with the code you wish you had
end

When(/^the game is in progress$/) do
  pending # express the regexp above with the code you wish you had
end

Then(/^print the board$/) do
  pending # express the regexp above with the code you wish you had
end
{% endhighlight %}

Next you need to define what each of the steps mean. When you executed the feature with no steps defined Cucumber gives back
skeletons for the undefined steps. Put those steps in the file features/step_definitions/board.rb and run Cucumber again. When you run that
you will get
{% highlight ruby %}
1 scenario (1 pending)
3 steps (3 skipped, 1 pending)
{% endhighlight %}

The basic structure of a step definition is either <code>Given</code>, <code>When</code> or <code>Then</code>, followed by a regular expression and a block of Ruby code.
The way step definitions work is that the regular expression identifies the step implementation. The step
text in scenarios is compared against the regular expression of the appropriate step definitions to find the one to use.
When the match is found, the substrings matching any groups in the regular expression are used as arguments to the block that is then evaluated.
Keep in mind that the arguments are **always strings**.

Here are my step definitions filled in:
{% highlight ruby %}
Given(/^the game is being played$/) do
end

When(/^the game is in progress$/) do
  game = Game.new
  game.is_over?
end

Then(/^print the board$/) do
  game = Game.new
  game.print_board
end
{% endhighlight %}
I'm going to assume that you have Ruby knowledge, so I'm not going to explain the Ruby. Feel free to use whatever method of verification you want in the <code>Then</code> steps. I
personally use rSpec.

Well, that covers an introduction to Cucumber, which I am actually really starting to enjoy using for testing. It's made the jump from BDUF to BDD, and then on to TDD very smooth and painless.


As always, if you find anything that's not fully correct in this, please let me know via [twitter](http://www.twitter.com).

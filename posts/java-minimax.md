---
title: "Java Minimax"
date: 2013-12-15
categories: ["Java", "Code"]
---

I've now written the Minimax algorithm in Ruby and Java. The first few times I wrote the algorithm I really didn't understand it too much, but after learning more about recursion, and literally writing out the method and going through a tic tac toe game by hand really helped me to understand the algorithm more.

When it came time to write my algorithm in Java, the most difficult part was not trying to figure out how to write a Minimax algorithm, but rather how to do it in Java. Since I had an understanding of the algorithm, was familiar with recursion, and had written it a few times it didn't take too long to get it down. I want to share my Java Minimax algorithm.

````
// Minimax algorithm
	private int miniMax(Board board, char marker, int depth, int color, boolean max, int alpha, int beta) {
		char opponentMarker = getOpponentMarker(marker);
		boardRules = new BoardRules(board);
		if(boardRules.isOver() || depth == 7) return getBoardScore(board, depth, marker) * color;
		else if(max) {
			ArrayList<Integer> emptySpaces = board.getEmptySpaces();
			for(Integer emptySpace : emptySpaces) {
				board.setMove(marker, emptySpace);
				alpha = Math.max(alpha, miniMax(board, opponentMarker, depth + 1, -color, !max, alpha, beta));
				board.undoMove(emptySpace);
				if(alpha >= beta) break;
			}
			return alpha;
		} else {
			ArrayList<Integer> emptySpaces = board.getEmptySpaces();
			for(Integer emptySpace : emptySpaces) {
				board.setMove(marker, emptySpace);
				beta = Math.min(beta, miniMax(board, opponentMarker, depth + 1, -color, !max, alpha, beta));
				board.undoMove(emptySpace);
				if(alpha >= beta) break;
			}
			return beta;
		}
	}
````
If you're not familiar with the minimax algorithm, [I wrote a blog post about it](http://blog.meaganwaller.com/introduction-to-minimax), but I'll also briefly go over it now.

In the minimax algorithm you have two players who have competing goals. What's the best for first player is going to the worst for the second player. Player 1 wants to win, and in order to do that Player 2 cannot win. So this means that the AI player needs to look at all the possible game states and rank them somehow to determine what the next best move is.

So, in the above algorithm the `miniMax` method takes a few parameters. It takes a `board`, a `marker`, a `depth`, a `color`, `max`, `alpha`, and `beta`. What does it all mean though?

## The Parameters
**Board**: The `board` is a `Board` object. We use the `board` to find out what the empty spaces on the board are so the computer can place moves on the board to look at all the game states.

**Marker**: The `marker` is a `char`, it represents the players `marker`. We use the `marker` to find out who the current player is and who the opponent is.

**Depth**: The `depth` is an `int`, it represents how many game states it's gone through while it's searching for the best move

**Color**: The `color` is also an `int`, it represents if we're looking for the maximum or minimum we represent it with (-1 or 1)

**Max**: Returns true or false if the player is the max player

**Alpha**: Is also an `int`, it represents the the `MIN_SCORE`, which is -100, it's an arbitrary score, and could also be -1 or -1000 as long as it's less than the maximum score

**Beta**: Is an `int`, it's the opposite of `Alpha` and represents the `MAX_SCORE`, which is 100, but is also arbitrary and could be 1 or 1000.

## Code Walk Through
I'm going to walk through the rest of the code now. We first initialize the the `opponentMarker` and the `boardRules`.
````
char opponentMarker = getOpponentMarker(marker);
boardRules = new BoardRules(board);
````
The`getOpponentMarker(marker)` is a method that returns `X` if the current player is `O` and `O` if the current player is `X`. The `boardRules` creates a new `BoardRules` object passing in the `board`, the `BoardRules` object is an object that contains all the board logic.

Next we enter into the first if loop.
````
if(boardRules.isOver() || depth == 7) return getBoardScore(board, depth, marker) * color;`
````
This sets the stage, if the `boardRules` returns true for the game being over, or if the `depth` is at 7, we need to return with the score we get in the `getBoardScore` method. The `getBoardScore` method returns an `int` with a score for a particular board at a particular state.

````
else if(max) {
			ArrayList<Integer> emptySpaces = board.getEmptySpaces();
			for(Integer emptySpace : emptySpaces) {
				board.setMove(marker, emptySpace);
				alpha = Math.max(alpha, miniMax(board, opponentMarker, depth + 1, -color, !max, alpha, beta));
				board.undoMove(emptySpace);
				if(alpha >= beta) break;
			}
                        return alpha;
````
Now if the game isn't over, or if the `depth` isn't 7, we enter our first else statement. We set an `ArrayList` with the unoccupied spaces in them. After we set `emptySpaces` to that, we go into a for loop. We say for every `emptySpace` in `emptySpaces` we will set a move on it. Next is where things start to get a little tricky this is where we introduce recursion.

We're going to set `alpha` to the max value of either `alpha` or the `miniMax(board, opponentMarker, depth + 1, -color, !max, alpha, beta));`
This is the recursion portion, because it's going to re-enter the `miniMax` method. So in order to actually find out what `alpha` is we need to hit our base case of the game either being over, or the depth being 7. After that we undo the move, and if `alpha` is greater than or equal to `beta` the method breaks, and we return `alpha`.

If it's not `max` we enter the else statement. We create the `emptySpaces` `ArrayList`. Once again for every `emptySpace` in `emptySpaces` we are going to set a move, and then do the same type of recursion we had before, but instead of finding the `max` we're finding the `min`. We undo the move, and if `alpha >= beta` we break, if not we return `beta`.

## Wrap Up
Recursion is tricky to get around, and it can be helpful to actually write it out to get a good grasp on it, which is what I had to do. Start off with a simple board state like this:
````
X | X | O
O | X | O
X | O |
````
And simply walk through all of the steps as if you're trying to solve for either 'X' or 'O', as you start to understand it you might want to try it with more and more complicated board states.




---
title: "Creating Objects via CLI in Java"
date: 2013-12-08
categories: ["Java", "Tutorials", "Code"]
---

A feature of my Mastermind game is to have the user be able to pick who is the encoder, and who is the decoder, the AI or the human. The user can also elect to have both be human, or both be AI. I was having difficulty deciding how to go about doing this. Should it happen in my `GameWrapper`, or in my `Game` class? I decided that instead of calling `game.setEncoder(new AI())` and `game.setDecoder(new Decoder(new Player());` I should used input from the user to set them in the `Game` class.

I put a method in my `CommandLineInterface` class that prompted the user to  decide if they wanted to be the codebreaker/codemaker by choosing "y" or "n". In my `Game` class where I call methods to display messages and prompt the user for their input, I this:

````
public void setCodeMaker() {
            String codeMaker = cli.promptPlayers().toLowerCase().trim();

            if(codeMaker.equals("y")) {
                    setEncoder(new Player());
            } else {
                    setEncoder(new AI());
            }
    }

    public void setCodeBreaker() {
            String codeBreaker = cli.promptPlayers().toLowerCase().trim();
            if(codeBreaker.equals("y")) {
                    setDecoder(new Player());
            } else {
                    setDecoder(new AI());
            }
    }
````

And then in my method for `gameSetUp` which calls to display the welcome message now calls to also display for the prompts for setting codebreaker/codemaker, as well as the two setters above.

I ran into some trouble when I tried to compare the `codeMaker` or `codeBreaker` string with `==`. In Java, you can't use `==` to compare `Strings` because `==` tests for reference equality and `.equals()` tests for value equality. So if you want to test or compare if two strings have the same value you need to use `equals()`. Use `==` when testing whether two strings are the same object.



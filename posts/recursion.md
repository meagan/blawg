---
title: "Recursion"
date: 2013-05-06
categories: ["Ruby", "Code", "Tutorials"]
---

I've been reading up on Recursion lately, basically recursion is a method that calls itself. Recursion has a base case, and a process in which the task at hand is reduced towards an end goal.

{% highlight ruby %}

def countdown(n)
	if n == 0
		puts "Blast Off!"
	else
		puts n
		countdown(n-1)
	end
end

countdown(5) #=> 5, 4, 3, 2, 1 Blast Off!
{% endhighlight %}

You could also use recursion to see the construct of Fibonacci's sequence.
{% highlight ruby %}
def fibonacci(n)
	return n if (0..1).include? n
	fibonacci(n-1) + fibonacci(n-2) if n > 1
end

fibonacci(12) #=> 144
{% endhighlight %}

So, how does one get better at Recursion?

<code>
	def practice_recursion
		read_blogs_about_recursion && write_recursive_code

		unless good_at_recursion
			return practice_recursion
		end
	end
</code>

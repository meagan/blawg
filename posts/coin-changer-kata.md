---
title: "Coin Changer Kata"
date: 2014-01-23
categories: ["Ruby", "Code"]
---

I worked on the Coin Changer kata this week. I've done this kata before but have always approached it the same way. In previous versions I always used an array to represent the coins I was getting back. If I was trying to make change for 86 cents I would get back: `[3, 1, 0, 1]` for 3 quarters, 1 dime, 0 nickels, and 1 penny. However that array doesn't tell you much unless you know going in how it's set up. I wanted to create a solution that offered an answer that was more readable without too much knowledge of it upfront.

With my new kata solution the answer it 86 cents looks like this now:
`{:quarter => 3, :dime => 1, :penny => 1}`. This format displays intents to the user and also is much more readable right off the bat.

Dealing with hashes is a little bit different than dealing with arrays, and honestly I haven't worked with hashes too much because they kind of intimidated me. But I decided to try to do something to change that, and working on this solution seemed like a good way to get started.

````
class CoinChanger
  def initialize
    @purse = Hash.new(0)
  end

  def make_change(amount)
    if amount >= 25
      change = amount % 25
      @purse[:quarter] = amount/25
      make_change(change)
    elsif amount >= 10
      change = amount % 10
      @purse[:dime] = amount/10
      make_change(change)
    elsif amount >= 5
      @purse[:nickel] = amount/5
      make_change(amount - 5)
    elsif amount > 0 && amount < 5
      @purse[:penny] = amount
    end
    @purse
  end
end
````
Now this isn't the cleanest solution, there is some repetition in `make_change` method, but I worked on this kata in a timeboxed 20 minutes. I think it's a good first step and my next time working on this I'll be looking into some refactoring. Mike opened an issue on my GitHub page with a way to look into how to remove the repetition in the method so I'll be looking into that this week when I work on the kata.

---
title: "Stacks & Queues"
date: 2013-07-02
categories: ["Tutorials"]
---

## What is a Stack?

A stack is a type of list where insertions and deletions take place at one end, called the top. Stacks are also known as LIFO or Last-In-First-Out. Imagine a stack as a stack of books, a pile of dishes, or one of the children's toys as shown below in Image 1.0, in all of those circumstances if you wanted to remove an item from the stack you'd have to remove the top item, or if you wanted to add an item, you'd have to add it on top of the top item. This is where the term, Last-In-First-Out comes from.

## Array Implementation of Stacks

In Ruby there isn't a formal stack object. However, an Array in Ruby supports all the operations necessary for working with a stack. We can get stack like functionality with methods like push and pop, and a few others.

Before diving in, let's review: pop removes last element from self and returns it, or nil if array is empty.

If number n is given, returns an array of the last n elements

push pushes the given object(s) on to the end of the array. It returns the array itself.

length returns an integer of the number of elements in self


````
class StackExample
  def initialize
    @example = Array.new
  end

  def pop
    @example.pop
  end

  def push(element)
    @example.push(element)
    self
  end

  def length
    @example.length
  end
end

ex = StackExample.new
ex.push("a") #=> ["a"]
ex.push("b") #=> ["a", "b"]
ex.push("c") #=> ["a", "b", "c"]
ex.length #=> 3
ex.pop #=> "c"
ex.length #=> 2
ex.pop #=> "b"
ex.length #=> 1
ex.pop #=> "a"
ex.length #=> 0
ex.pop #=> nil
````

## What is a Queue?

A queue is also a type of data structure. However, queues are first-in-first-out. The two major operations in queues are enqueue, which adds an element to the rear of the queue, and dequeue, which removes an element from the front of the queue.

## Array Implementation of Queues

In Ruby we don't have enqueue and dequeue methods, however, we have something that does the same thing. We're going to use unshift to insert elements into the Array, and pop to remove elements. Once again, we'll see Ruby's dynamic arrays at work. Because of this we don't have to store front state or rear state for the class. Instead, because our array is unbounded, like above, it can grown as we keep adding elements to it.


````
class QueueExample
  def initialize
    @example = Array.new
  end

  def dequeue
    @example.pop
  end

  def enqueue(element)
    @example.unshift(element)
    self
  end

  def length
    @example.length
  end
end

qe = QueueExample.new
qe.enqueue("a") #=> ["a"]
qe.enqueue("b") #=> ["b", "a"]
qe.enqueue("c") #=> ["c", "b", "a"]
qe.length #=> 3
qe.pop #=> "a"
qe.length #=> 2
qe.pop #=> "b"
qe.length #=> 1
qe.pop #=> "c"
qe.length #=> 0
qe.pop #=> nil
````

## Wrap Up

Stacks and queues are both basic data structures. In Ruby we can get the functionality of a stack or a queue with Arrays. Tomorrow I will show how to implement recursion with the application of a stack, as well as how to eliminate tail recursion with the application of a stack.

---
title: "My First Day"
date: 2013-04-08
categories: ["Apprenticeship"]
---

I arrived at the office around 8:30am this morning, eager to get started.
I've been looking forward to this day since before I was even offered the job two weeks ago.
After saying good morning and shaking everyone's hand, Cory went over how my first week would play out.
My first assignment is to rewrite my (awful) Tic Tac Toe that I submitted as part of my application to become an apprentice.
I originally wrote my game in Python, and will now not only have to rewrite it in Ruby but will also need to include tests with RSpec.
Cory told me that he believes that I should have this done by Friday, so I'll be working on that throughout the week.
I was also given a couple different books to borrow and read, the first one is <a href="http://www.amazon.com/Software-Craftsmanship-Imperative-Pete-McBreen/dp/0201733862/ref=sr_1_1?s=books&ie=UTF8&qid=1365451927&sr=1-1&keywords=software+craftsmanship" target="_blank">Software Craftsmanship</a> by Pete McBreen,
I am expected to finish this book by the end of the week, I'm excited to dig into it, I truly enjoy reading and this book looks interesting just from the quick glance I took.
Other books on my reading list include: <a href="http://www.amazon.com/Programming-Ruby-1-9-Pragmatic-Programmers/dp/1934356085" target="_blank">Programming Ruby: The Pragmatic Programmer's Guide</a> for 1.9.3, and <a href="http://www.amazon.com/RSpec-Book-Behaviour-Development-Cucumber/dp/1934356379/ref=sr_1_1?s=books&ie=UTF8&qid=1365452101&sr=1-1&keywords=the+rspec+book" target="_blank">The RSpec Book</a>.
I don't have much experience with Test Driven Development so that will be my biggest challenge during this week.

Besides that today was a fun day in the office, too. Cory received his Oculus Rift in the mail and we all got to try it out.
It is so amazing to me to actually be alive to see this kind of technology, and to imagine what leaps and bounds technology will take in the coming years.
I remember being at Best Buy recently and seeing kids under the age 10 playing on iPads and Tablets and thinking how crazy it is that when I was their age (only 11 years ago)
that kind of stuff just wasn't something that existed, or at least it wasn't readily accessible and affordable for entertainment purposes. I distinctly remember playing Wheel of Fortune on a Floppy Disc and it was quite literally the height of fun. I can't even begin to imagine what kind of technology will be available in the next 10, 20 or 30 years.

I've been writing code since I was about 12 years old, I was an avid Neopets player and wanted to customize my user page so I found a website that offered background images and tutorials. I browsed the site for a bit and found an "About" link, so I clicked the link and found out
that the girl who owned the site was only 13. If she could have a website, why couldn't I? I instantly started searching on how to create a website and came across many websites that boasted that they could teach anyone html. I learned the basics and found out how to get my own domain name.
I saved up my allowance money and purchased my own domain name, but not before I had to convince my mom for days to let me use her credit card online.
The first domain I ever purchased was skyy-blu.com. I didn't know that was the name of a vodka then, I was just a 12 year old girl who liked the color blue, and apparently didn't want to conform to standard spellings. Anyway, the point of that story is just so when I say that 12 year old me is ecstatic about this opportunity it is understood why.
I've come along way from glitter graphics and transparent iFrame layouts (ugh), I still have so far to travel, but at least now I have a map for the journey.

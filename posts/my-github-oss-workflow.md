---
title: "My GitHub OSS Workflow"
date: 2013-11-18
categories: ["GitHub", "OSS", "Tutorial"]
---

I've been contributing some to OSS recently. Mostly because I'm very much interested in contributing, and because it's something that we at 8th Light strive to do, it's the reason why on Friday's we spend half the day on open source contributions, it's our way to give back since we used OSS for nearly all our projects. I heard of Katrina Owen's project, Exercism and wanted to get my feet wet right away. I haven't contributed to much OSS, but I have had 12 pull requests merged into master at Exercism, and it's been a lot of fun, and I encourage everyone to consider contributing to OSS, if you have the means, and the time. It has been a very rewarding experience for me.

This blog post is going to explain my work flow for how I send a pull request, it's fairly basic but I would like to document it because sometimes I tend to forget what I'm supposed to do next and am frantically searching for a blog post or something that explains it, so I'll post it here so I always know where to look.

## Get Started

First things first, find a project you're interested in working on. I suggest that you dive into the source code a bit, try and get familiar with what's going on. The best way to figure out what needs to be solved is to use the product, see if there is some functionality that is missing. Contributing to OSS doesn't have to be writing some integral change to the structure of the code. Lots of OSS needs help checking for typos, writing documentation, and refactoring.

When I want to contribute but I'm not entirely sure of where to go, or what to do, I check out the Issues tab. I see what issues are open, and if anyone has opened an issue that I think I can fix, or if someone has opened an issue for the problem I've run into, or functionality I want to add. This is a very important step because you don't want to spend hours, or days on a major pull request only to find out that the maintainer doesn't want the software to change in that way. This is very true with user interface or design changes, so if you would like to implement a big change to say, the user interface, it's best to get in touch with the maintainer to see what their intentions are before you spend time on something they may or may not like.

If your issue isn't open or doesn't exist, open it up yourself. Standard internet etiquette rules apply here, be friendly, thorough, and let the contributors and maintainer know that you appreciate the product (this is always awesome to hear), and then describe the bug or the feature you want to work on, and offer to help. Hopefully it won't take long to get some input on where to look to solve the problem, or if it's a feature they're interested in adding to the software.



## Setting Up

Now we need to set up so we can get to the fun part. Go to the repo of the project you want to work on, and hit "Fork", and Fork the repo so you have your own version. Go to your forked repo, and grab the Git clone ssh: url from the box, and then on the command line, clone it to your local machine.

`git clone [your forked ssh/git url]`

The next step isn't mandatory, but I think it's useful if you plan on working on the project for anything more than a one-off fix. This step is where we make the fork track the original upstream repo. Use the following command to add the 'upstream' (the original project location), as a remote branch so that you can pull their updates into your branch. Replace the 'upstreamname' and 'projectname' values with the actual user/project name that you want to track:

`git remote add --track master upstream` `git://github.com/upstreamname/projectname.git`

This adds the original project (not the forked one) as a remote branch named "upstream". When you want to pull in changes you simply run this command:

`get fetch upstream`

And to merge it into your own projects, type:

`git merge upstream/master`

## Hacking

Once you've gone through the above steps, now is where that fun part comes into play. You'll want to set up your development branch, so switch off the 'master' branch and onto a different branch for the new feature or bug fix you want to work on. It's important to do this so you have only one Pull Request per branch. So, if you want to submit more than one fix, you'll need to have multiple branches. Make a new branch by doing this:

`git branch newfeature`

Then switch to it like this:

`git checkout newfeature`

Now that you're on your new branch, it's time to Hack. Hack like you would until the code is doing what you want it, and passes all the tests you've written for it. It's time to squash your commits. Before you submit your pull request back upstream, you'll want to squash these commits into a small handful of well-labeled commits. To do this, you want to use the git rebase command. First, take a look at the commit you've made with the git log, and figure out the commits you want to squash. If you want to squash the last 5 commits into one, you'd say

`git rebase -i HEAD~5`

This will bring you into your editor with some text that will show some text that might look something like this:

 ````
pick a5467842 Commit 1
pick b4952959 Commit 2
pick g4958285 Commit 3
pick gjw95939 Commit 4
pick fk949607 Commit 5
````

To squash those commits into one, change it to something like this
````
pick a5467842 Commit 1
squash b4952959 Commit 2
squash g4958285 Commit 3
squash gjw95939 Commit 4
squash fk949607 Commit 5
````

Then save and quit, and you'll be brought into another editor session. Describe the changes as well as you can, and save and quit again. And congratulations, you've squashed all of your commits into a single commit. Now you're ready to submit a pull request

## Submitting A Pull Request
Once you've committed and squashed your changes, push them to your remote like this:

`git push origin newfeature`
Then go to your forked repo on GitHub and change branches to the proper one for your new feature. Click on the button that says "Compare and Pull Request", and this will bring you to a page asking you to describe your change. Describe your change as thoroughly as possible. Then press 'Submit Pull Request', and hooray, you've done it! Now is the time to wait for the maintainer to check out your code and see if they want to pull your changes in or not.

Not every pull request will get accepted, it's the unfortunate truth, however, with a constructive Open Source project the maintainer, and the core contributors are usually helpful to guide you in the right direction. Hang out around the issues, try and answer questions, and ask insightful questions. Lots of maintainers are looking for help, and appreciate every person who submits a pull request to help make the software better.

I hope this guide helped to establish and illustrate my Github workflow when contributing to open source projects.


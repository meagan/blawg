---
title: "Agile Estimating and Planning Part Two"
date: 2013-08-10
categories: ["Technology", "Book Reviews"]
---

How do you determine the category of the feature? Kano proposed doing this by asking the user two questions, how they would feel if the feature were present and how they would feel if it were absent. The user should answer on a 5 point scale:

1. I like it that way
2. I expect it to be that way
3. I am neutral
4. I can live with it that way
5. I dislike it that way

Relative weighting is an approach by Karl Wiegers. It is similar to Kano's in the sense that it considers both the positive benefit of the presence of a feature and the negative impact of its absence, as well as the cost into a single value representing the priority of a feature. But, rather than surveying users, this approach uses expert judgment. The team, led by the product owner, determines and assesses each feature being considered for the next release.



## Splitting User Stories

 "These days we do not program software module by module; we program software feature by feature" - Mary Poppendieck
What happens when you have a really big user story? Here at 8th Light, we have a few rules when it comes to starting iterations, and completing iterations. For starters, since we define our point as being worth 4 hours of work, or half a day, the maximum amount of points you can have in a single day is 2. That leaves us with 10 points a week, but since our Fridays are 8th Light University and Waza days, we are left with 9 points since we work on client work only the first half of the day on Friday. In order for an story in an iteration to be accepted it has to meet some criteria, the story must be:

- Estimable
- Manageable
- Valuable
- Testable

If your story doesn't fit in an iteration it's time to split it, or if you need to provide a more accurate estimate. For example, I might currently have a story like:

> Create an Unbeatable TicTacToe Game

That's pretty vague, and too big to estimate. That's not estimable because it's the end goal. It's time to split that into smaller stories that are more estimable, stories like:

- Allow one user to make a mark over and over on a board until the game is over.
- Two player game that has two separate markers and rotates turns and plays until game over
- Create an Easy Computer play that places moves randomly
- Create a more difficult Computer player that can pick winning spaces and block a winning move from opponent
- Create an unbeatable Computer player that can only win or tie.

All of these are examples of stories that I am currently working through for my Unbeatable Tic Tac Toe game. These are examples of proper stories because they all work towards the end product of an unbeatable game, but they are estimate and manageable, they add value because my client (in this case my mentor, Will) is receiving something deliverable in each iteration, and something that gives the Tic Tac Toe game more value. They are also testable since I am Test Driving my game, so each story has tests to back up that it works as well as a working implementation. This means that refactoring isn't something that you can put down as a story, as much as we developers love to refactor. Refactoring is something that happens while you code. There are some special cases for putting refactoring as a user story, but they are exceptions to the rule, ie; if refactoring can speed up the application by a tangible amount, this provides value to the customer they will likely agree to this.

## Scheduling

Alright, so you have everything down now and you're ready to get going! But, wait, how do we create a schedule? In this section of the post we will be looking at how to plan release and how to plan an iteration. We will be ending off by looking at how to cover complex situations or uncertainty.

Release Planning

For starters, a release plan is a high-level plan that covers a period longer than an iteration. Like the Tic Tac Toe example above, the actual Unbeatable Game would be the release plan since it covers many iterations. A release plan doesn't have to describe exactly what will be worked on during each iteration, and it's not even recommended, that much detail isn't necessary, your user stories will describe that for you.

Release planning is an iterative process that starts at the condition's of the product owner, including: goals, scope and resources. If a project can't be planned that meets the initial set of conditions put in place by the product owner you will repeat the planning process to see if a lesser set of conditions can be met. It also should be noted that a release plan isn't something you do once and never think about again, it should be updated at the start of each iteration.

## Iteration Planning

Alright, so we know what a release plan is, now let's dive deeper into iteration planning. Iteration plans are much more detail orientated than release plans. The iteration plan looks no further than one iteration, makes sense, right? Your iteration time might vary, but like I've said at 8th Light, we develop in one week iterations, some other companies might do two week iterations, or three week iterations, any longer than that and you're really looking at release planning. When you plan your iteration remember how to split up stories as you will more than likely have to do this. It's not often that a client will have specific tiny features that will fit into one iteration, so it's important to know when to split the user story and the criteria for determining how to split it. In an iteration each story is estimated in terms of ideals hours or story points the task will take to complete. While there are two general approaches to planning an iteration, velocity-driven and commitment-driven, they are really quite similar and often led to the same iteration plan. However, I'll briefly go over both.

With velocity-driven iteration planning the team first collaboratively adjusts priorities, next they identify the target velocity for the coming iteration, then they select an iteration goal. After selecting their iteration goal the team selects the top-priority user story that supports this goal. (See, all those terms are coming together and making sense). They select as many stories as it takes to fill the iteration to equal the target velocity. Finally, each story is split into tasks, and each tasks is estimated.

With commitment-driven iteration planning involves many of the same steps as its velocity-driven counterpart. The first steps, adjusting priorities and identifying an iteration goal are the same. The next step of selecting a story to add to the iteration is where we have the difference. The product owner and the team still select the highest-level priority story that supports the iteration goal, but in this iteration planning stories are selected and decomposed into tasks and the tasks estimated one story at a time. Stories are selected one at a time because after each story is split into tasks and the tasks estimated, the team decides whether or not they can commit to delivering that story during the iteration.

## How Do You Select an Iteration Length?

As I said up above, at 8th Light we work in one week iterations. I enjoy this because sometimes it can be daunting to plan out two to four weeks in the future, but there are companies that probably work in iterations of two, three, or four week iterations and it works just fine for them. No two teams or companies are alike, and there are different factors to consider when determining an iteration length. Factors such as:

- The length of the release being worked on
- The amount of uncertainty
- The ease of getting feedback
- How long priorities can remain unchanged
- Willingness to go without feedback
- The overhead of iterating
- How soon a feeling of urgency is established


## Estimating Velocity

> "It's better to be roughly right than precisely wrong," - John Maynard Keynes

You can estimate by using historical values. This is a great option, if you have the historical values. Before using the historical values it's important to ask yourself a few questions to determine if they're still valid, questions like: "Is this technology, domain, team, product owner, tools, working environment the same? And are the people making the estimates the same?" If you can answer yes to all of these, feel free to use historical values to estimate velocity, if any of these aren't true you might want to think twice about using them, or at least put a larger range around them to reflect the inherent uncertainty.

Another way to estimate velocity is by running an iteration, or more than one and then observing the velocity during those iteration(s). The best way to predict velocity is by observing it, this should always be your default approach.

Lastly you can forecast the velocity by breaking a few stories into tasks and seeing how much will fit into an iteration, but this is sort of like iteration planning. This is best to use if you have no historical values and it isn't feasible to run a few iterations to observe velocity. First estimate the available hours, then estimate the time available in each iteration, then expand the stories and see what fits, and finally put your range around it.

#Planning for Uncertainty

 "To be uncertain is to be uncomfortable, but to be certain is to be ridiculous" - Chinese proverb

Let's face it, projects contain a lot of uncertainty. We can't say with 100% confidence that we can perfectly perform a task, especially if you are relying on something outside of your control, maybe specific hardware, maybe the external API you're using has bugs in it, maybe your computer totally fries and you lose your entire development environment and are set back (if you don't have repos for your dotfiles, and don't commit to Github often, you probably really should).  However, this uncertainty isn't always reflected in the schedules and deadlines that are created. I can tell you from experience that I am bad about planning for uncertainty. I almost always estimate my stories too optimistically, as if I'm scared of admitting that it might take me a while to do something. This is a hard habit to break because it can seem like admitting that you don't know something, and it is hard to admit that when it comes to your job, right? Well, for me at least. Sometimes this uncertainty can be so large that we should really be taking extra steps and extra time when estimating the duration of the project.

In these cases, where there is either greater uncertainty or great implication to being wrong about a release schedule, Because of this, we should include a buffer in the determination of the schedule. A buffer is a margin of error around an estimate, and it can be wise to include one. Let's take a look at the two types of buffers, feature buffers and schedule buffers.

Feature buffers are created when a product's requirements are prioritized and it is acknowledged that not every feature can be delivered. Creating a feature buffer should be simple to do on an Agile project. The customer first selects all of the mandatory work, those "must-haves". Then the estimates are summed, this will represent the minimum that can be released. The customer will then select 25% to 40% more work, choosing toward the higher end of the range of the projects with more uncertainty or less tolerance for schedule risk. Add these estimates to the original estimate, and get the total estimate for the project. This feature buffering process is consistent with that used in the agile process of DSDM (Dynamic Systems Development Method). On DSDM projects, requirements are sorted into four categories: Must Have, Should Have, Could Have and Won't Have. No more than 70% of the planned effort for a project can fall in the Must Have category, this means that DSDM projects create feature buffers equivalent to 30% of the duration of the project.

I'm going to use a real world example of something from my life to explain schedule buffers. When I was a senior in high school I had to take the SATs. I didn't want to have to take them twice, or to miss the test by not being prepared and paying for the test and not being able to take it (I actually did end up taking the SATs twice but not from lack of preparedness!). I knew that I had to be in my testing room and in a seat at 7:30am in order to be able to take the test. Whether I'm there or not, the test must go on! This means that I needed to create a plan so that I could get there in time without feeling rushed or stressed, but not get there so early that I'm camping out in the parking lot or something. I had to think about all the steps involved, steps like: waking up and taking a shower, getting ready (blow drying my hair, getting dressed), eating a filling breakfast as recommended by the SAT guidelines, driving to the school, parking my car, getting checked in, finding my testing room. It was also important for me to do this plan with as little stress or worry as possible since the SATs are already pretty stressful. If everything were to go well doing all of those things should take me about an 75 minutes. But, what if there was on accident on the way to the school? What if my sister was hogging the shower, what if I forgot my gas tank was empty and I had to stop and get gas? There's no need to plan for everything to go wrong, but since the SATs were pretty important to me at the time, I didn't want to miss my window to take the test and delay applying for colleges any longer.  If I left planned for it to take 100 minutes, and everything goes perfectly according to plan, I have 25 minutes to waste. I could use that time for studying last minute things, or making sure I am comfortable, have a good seat, and set up my desk in a pleasing way. But, if I get stuck in traffic, or if I can't find parking, or if I just take longer in the shower than I thought I have a buffer that allows me to do this.

That was sort of a long-winded example of schedule buffers, but I think it presents it quite well. In software development we also need to allow for schedule buffers. We would be misguided to think that everything will go perfectly right all of the time. We need a way to quantify the uncertainty that comes along with these projects. If we estimate a single value to a user story we are falsely representing that to reflect our expectations and the amount of time it will take to develop it. A more accurate way to estimate stories is by using a range. At 8th Light we give each story three estimates, an optimistic, realistic, and pessimistic estimate. It's important to acknowledge that sometimes things go wrong, sometimes computers get fried, sometimes a feature we thought would be easy to implement actually has some requirements that we didn't address already.



## Planning for a Multiple-Team Project

Agile teams have no more than seven to ten developers by definition. The agile process allows smaller teams to be able to accomplish quite a bit, since it encourages them to be more productive. Sometimes, though, there are times when we need a larger team for a project, this doesn't mean bringing on 50, or 100 people to a single team, the agile approach is to create multiple smaller teams. Agile projects may have dozens of smaller teams instead of a single 150 person team. When multiple teams are working together on one project there needs to be collaboration and a way to coordinate their work.

Teams should establish a common basis for their estimates for starters. At the beginning of a project the teams should meet and choose between story points or ideal days. It's important to have everyone estimating on the same page. Each user story needs to be estimated by only one team, but the estimates should be equivalent regardless of which team estimated the story. Teams need to also agree on the meaning of units set in place by agreeing upon the estimates for a small set of stories.

An agile team begins an iteration with vaguely defined requirements and turns those vague representations into functioning, tested software by the end of the iteration. Granted, going from vague requirements to functioning tested software is easier for one team than multiple teams, it's still doable. When working on a multiple-team project it's important and necessary to put more thought into the user stories before the start of the iteration. This additional detail allows multiple teams to easier coordinate work.

Multiple teams will also benefit from incorporating a rolling lookahead plan into their release planning process. A rolling lookahead plan just looks forward a small number of iterations, typically one or three, and allows teams to coordinate work by sharing information about what each will be working on in the near future. This does work for most teams, but in situations where the interdependencies between teams are very complex or frequent that it is not enough. In these situations your first step should be to try to find a way to reduce the number of interdependencies so that a rolling lookahead plan does work. If that can't be done, you should consider including a feeding buffer in your iterations. A feeding buffer protects the on-time delivery of a set of new capabilities. Basically, this is the amount of time that prevents the late delivery by one team causing the late start of another. If Team A needs something from Team B at 8am in order to get started on their iteration, Team A can't plan on finishing up at 7:59am. There needs to be a buffer in place to ensure that a team won't prevent another team from getting work done. This is done by including the buffers we talked about above in their story estimates.

## Wrap Up

Alright, so far we've learned about what agile estimating and planning are, how to estimate size, how to plan for value, and how to schedule. We are well on our way to being able to put agile methodologies to work in our software development to ensure the success of our projects, as well as client and developer happiness. The next blog post will finish up this series, and will be about Tracking and Communicating agile projects.

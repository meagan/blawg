---
title: "Solving Problems"
date: 2013-11-21
categories: ["Apprenticeship"]
---

Today I was setting up an 8th Light project called Ledjer. Ledjer is an exercise in Java that goes along with a Java training course. Mike wanted me to set up ledjer locally, run the tests, and follow along with the Java training course slides keeping in mind any questions that I might have.

I was able to get ledjer set up with a few difficulties due to lack of instructions for setting up with IntelliJ. However, I took this as an opportunity to learn something new, and also to leave the code better that I found it. Although I didn't touch any of the code, I reformatted the README to fix some weird markdown formatting that wasn't working properly. I also included instructions for how I was able to set up the exercises with intellij. I like finding ways that I can help, I also like that about blogging, it's a way for me to explain my problem. I think these are important things to blog about and to do, because if you have the question then it's quite likely that someone else does as well.

After I got the maven tests running, and modified the README so I wouldn't forget, I attempted to set up the Fitnesse server to run those test suites. However, I couldn't really figure out how to set it up. Thankfully, I knew who to contact to figure it out and was able to get the solution within minutes. I know that e-mailing someone to ask what felt like a silly question would be something I wouldn't have done just a few months ago. I would've been really afraid of looking stupid, or just would have been stubborn and would've tried to figure it out with no leads, and poor google-fu in those situations. I'm happy that I'm becoming better at asking thorough, thoughtful questions.

All in all, today I solved some problems, and also realized that in these past 7 seven months I've grown a lot as a developer. It's really awesome when I look back and realize that I'm actually making real, tangible progress here. I'm excited to see where the next 7 months take me.

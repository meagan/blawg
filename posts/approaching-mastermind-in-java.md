---
title: "Approaching Mastermind in Java"
date: 2013-11-15
categories: ["Apprenticeship", "Java"]
---

My assignment this week is to write the Mastermind game in Java. Mastermind is a code-breaking game for two players, if you're playing the physical version, you'll play a human vs human game. However, I'm writing a Java version that will be playable in the console, so it will be human vs AI player.



## Game Play and Rules

The physical game and the command line version game follow the same rules, with a few differences in presentation, obviously. In both games, you have a codebreaker and a codemaker. The codemaker in my version will be the AI, and the codebreaker will be the human playing the game. The codemaker chooses a pattern of four pegs, in this case, the AI will randomly generate the four pegs. The codebreaker tries to guess the pattern, both the order and the color of the pegs, within a set amount of turns. Each guess they make is made by picking the pegs in whatever order they believe is correct. After they make their guess the codemaker provides feedback by displaying zero to four hint pegs. A black hint peg is placed for each code peg that is both the correct color and in the correct position. A white hint peg indicates the existence of a correct color peg placed in the wrong position.

Once the feedback is provided, another guess is made, the guesses and the feedback continue to alternate until either the codebreaker guesses correctly, or the set amount of turns occur without guessing the correct code. The codemaker gets one point for each guess a codebreaker makes, and receives an extra point if the codebreaker doesn't guess the pattern exactly by the last guess.

## My Approach

I've never written the Mastermind game before, not all the way through at least. When I was reading the RSpec book, the mastermind game was one of the games that was brought up to explain how testing, especially cucumber testing worked. Writing this game in Java is definitely a challenge. I've been reading the Learning Java, 4th Edition Book and it's definitely been helping me to understand some of the things I was just sort of getting when it came to Java. Java is a way different beast than Ruby, but I'm really liking writing it, even if it's frustrating at times since I'm not as familiar with the syntax. For example, yesterday I was trying to figure out why my tests wasn't passing and it was because I tried to new up a class by saying Game.new instead of new Game, whoops, looks like I've been reading/writing a little too much Ruby lately.

I sat down at my computer yesterday feeling slightly overwhelmed, where to even approach this? I thought maybe I should start with the board, but I find that sometimes I have difficulty going "inside-out", and thought it might be better to start at the Game Rules/Logic and build from there. My co-worker, Justin, suggested I take some time and brain storm on the whiteboard that's behind my desk and sort of decide how the different pieces of the game should interact. I was always nervous to approach something this way because it felt like "Big Design Up Front", and then Justin sort of laughed and said "This game isn't big enough to have the problem of 'Big Design Up Front'". So, I took to the board and wrote out some ideas of how to approach this problem:

I set it up in a few different parts, first I set up a simple scenario, if Alice is a user who wants to play this game:

1. Given Alice starts a new game
2. When she enters her guess
3. Then she should see feedback and be prompted appropriately

This gave me some insight on how to set up my game flow, which I wrote like this:


- User Starts Game
    1. A secret code is generated
    2. The number of turns are initialized
    3. The number of guesses are set to 0
- Prompts user for their guess
    1. User input their guess
    2. Guess is validated to make sure inputs are all valid
- Receives users guess and compares against the secret code
    1. Keeps track of turn number
    2. Stores users guess history
- Displays feedback to the user

After the displays feedback portion, it jumps back up to number two. If the user doesn't guess correctly by the end of the set amount of turns, they lose, and are displayed a losing message as well as prompted to play again. If the user guesses correctly they are displayed a winning message and also prompted to play again.

## Approach to Testing

I'm still having a bit of difficulty getting started with the testing. I think the best approach is to start testing the game rules and go from there. My first test looks like this:

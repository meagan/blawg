---
title: "Preparing to Learn Clojure"
date: 2014-02-16
categories: ["Apprenticeship", "Clojure"]
---

This week I'm learning Clojure. There are some things I need to do first before I can dig into a new language:

   - Set up my development environment. For Clojure this probably will mean getting good syntax highlighting going since there a lot of parens.
   - Setting up a testing framework so that I can practice TDD.

Once I have that ready to go, I'll probably go through the [Clojure Koans](http://clojurekoans.com/). After that I will do a kata or two in Clojure to get used to dealing with a functional language. I've only used Object Oriented languages so dealing with immutability will be an interesting new way to look at problems.

I'll also be working on Footprints this week. Taryn and I have been working on Footprints 12 points a week (that's three days a week for us), but this week we are going to devote 8 points to Footprints, so that leaves 3 days for me to work on Clojure as well. Mike wants me to a have a command line Tic Tac Toe by next Friday. It's been a while since I've worked on a Tic Tac Toe game, so I think I'm ready to give it another go now.  It's also been a while since I've learned a new language. The last new language I learned was Java back in November, and this will be my first time working in a functional language. I'm excited to see how much progress I can make on this.

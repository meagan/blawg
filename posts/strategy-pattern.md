---
title: "Strategy Pattern"
date: 2014-02-01
categories: ["Design Patterns", "Ruby", "Code", "Tutorials"]
---
The basic idea of this design pattern is to delegate tasks to encapsulated
algorithms which are interchangeable at runtime.

In the Strategy pattern we have an object (the **context**) that is trying to
get something done. However, to get that thing done, we need to supply the
context with a second object, called the **strategy**, that helps get the thing
done.

1. Define a family of objects which all do the same thing (example: format
   output, generate graphics, etc)
2. Ensure the family of objects share the same interface so that they are
   interchangeable.

There are two strategies to passing data from the context object to the strategy
object. We can pass the data as parameters when the strategy is called, or we
can pass the context object as the single parameter.

If the strategies are very simple and have only one method, we can even use code
blocks for our algorithms and simply use `block.call`. However, if multiple
methods are needed, the strategies must be structured as separate classes.

**Advantages**

  - Algorithms are interchangeable at runtime
  - Promotes modularity

````
# html_formatter
class HTMLFormatter
  def output_report(context)
    puts '<html>'
    puts '  <head>'
    puts "    <title>#{context.title}</title>"
    puts '  </head>'
    puts '  <body>'
    context.text.each do |line|
      puts "    <p>#{line}</p>"
    end
    puts '  </body>'
    puts '</html>'
  end
end

# plain_text_formatter

class PlainTextFormatter
  def output_report(context)
    puts "***** #{context.title} *****"
    context.text.each do |line|
      puts line
    end
  end
end

# report
class Report
  attr_reader :title, :text
  attr_accessor :formatter

  def initialize(formatter)
    @title = 'Monthly Report'
    @text =  ['Things are going', 'really, really well.']
    @formatter = formatter
  end

  def output_report
    @formatter.output_report(self)
  end
end
````

To see this in action, run this code in `main.rb`:

````
require './report'

require './html_formatter'
require './plain_text_formatter'

report = Report.new(HTMLFormatter.new)
report.output_report

# Change the formatter at runtime
report.formatter = PlainTextFormatter.new
report.output_report
````

It will output this:

````
<html>
  <head>
    <title>Monthly Report</title>
  </head>
  <body>
    <p>Things are going</p>
    <p>really, really well.</p>
  </body>
</html>
***** Monthly Report *****
Things are going
really, really well.
````

But, you can also use procs, check this out:

````
# html_formatter
HTML_FORMATTER = lambda do |context|
  puts '<html>'
  puts '  <head>'
  puts "    <title>#{context.title}</title>"
  puts '  </head>'
  puts '  <body>'
  context.text.each do |line|
    puts "    <p>#{line}</p>"
  end
  puts '  </body>'
  puts '</html>'
end

# plain_text_formatter
PLAIN_TEXT_FORMATTER = lambda do |context|
  puts "***** #{context.title} *****"
  context.text.each do |line|
    puts line
  end
end

# report
class Report
  attr_reader :title, :text
  attr_accessor :formatter

  def initialize(&formatter)
    @title = 'Monthly Report'
    @text =  ['Things are going', 'really, really well.']
    @formatter = formatter
  end

  def output_report
    @formatter.call(self)
  end
end
````

And once again, to see this code in action, run the following code, mine is in a `main.rb` file:

````
require './report'

require './html_formatter'
require './plain_text_formatter'

report = Report.new(&HTML_FORMATTER)
report.output_report

# Change the formatter at runtime
report.formatter = PLAIN_TEXT_FORMATTER
report.output_report


# For added flexibility, we can use an on-the-fly code block as a formatter
report = Report.new do |context|
  puts("==== on-the-fly formatter 1 ===")
  puts("==== #{context.title} ===")
  context.text.each do |line|
    puts(line)
  end
end
report.output_report

report.formatter = lambda do |context|
  puts("==== on-the-fly formatter 2 ===")
  puts("==== #{context.title} ===")
  context.text.each do |line|
    puts(line)
  end
end
report.output_report
````

And you'll see this output:

````
<html>
  <head>
    <title>Monthly Report</title>
  </head>
  <body>
    <p>Things are going</p>
    <p>really, really well.</p>
  </body>
</html>
***** Monthly Report *****
Things are going
really, really well.
==== on-the-fly formatter 1 ===
==== Monthly Report ===
Things are going
really, really well.
==== on-the-fly formatter 2 ===
==== Monthly Report ===
Things are going
really, really well.
````

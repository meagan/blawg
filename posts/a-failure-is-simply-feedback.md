---
title: "A Failure is Simply Feedback"
date: 2014-03-24
categories: ["Technology", "Mentoring", "Learning"]
---

Remember that game where someone hides something and they direct you to where it's
hiding by only using the phrases "you're getting hotter", and "you're getting
colder"? If you were searching for an object and you heard "you're getting
colder!" you wouldn't view it as a failure, you wouldn't freeze in place, you'd
view it as feedback and go in the other direction. Every instance of hearing
"you're getting colder", meant that you knew what direction *not* to go, and
it would lead you to the place where the object was hiding.

Now imagine that you're playing this game and instead of hearing "you're getting
colder" the only feedback you got was "no", or "wrong". That's marginally better than
no feedback at all, but after hearing only "no" or "wrong" a couple of times you would probably
freeze in place, scared to move at all.

## Teaching High School Girls to Code

[Nhu](http://www.8thlight.com/team/nhu-nguyen) and I started teaching some high school girls at the [Chicago Tech Academy](http://www.chitech.org) how to code with the [Girls Who Code](http://www.girlswhocode.com) program. Every Friday we go over to the school and spend time teaching the girls coding basics using the [Khan Academy](http://khanacademy.org) platform and JavaScript. We have mostly freshmen girls in our class, but recently a few juniors, and a sophomore girl joined. Last Friday we had record attendance with twelve girls, up from the week before of seven, and the week before that at one.

To me, our biggest struggle hasn't been keeping the girls entertained, or even keeping them coming back, but rather, getting them in the door in the first place. How do you get high school girls to stay after school on a Friday to learn how to code? It hasn't been easy, but thanks to the power of pizza, we're starting to see our numbers rising. There were a few girls who already were interested in being programmers, they weren't quite sure what it meant to be a programmer, but they knew it sounded cool, and they knew they liked using applications and games and wanted to be the ones doing that. But, how do you get the girls interested that don't already have plans to be programmers, and once they're in the class how do you make it something fun, accessible, and engaging so they don't get bored or frustrated and write off programming forever?

I [keep hearing](http://www.cnet.com/news/obama-endorses-required-high-school-coding-classes/) [stories](http://www.suntimes.com/news/education/24270552-418/cps-to-make-computer-science-a-core-subject.html) of schools making coding mandatory, or making programming part of curriculum, and not just an elective. While I agree that [computer science should not be elective only](http://www.fastcoexist.com/3023167/want-to-get-girls-and-minorities-into-coding-stop-making-computer-science-an-elective), I am scared to get excited about making it mandatory. I only express this fear because currently, [the way that most public schools in the USA are taught](http://www.jsonline.com/news/opinion/a-factory-model-for-schools-no-longer-works-b9943187z1-213602131.html) makes me worry that kids will end up resenting or being turned off by programming, in much the same way that many students resent and are turned off by other STEM courses, most notably (in my experience), math courses.

I see this problem already emerging while I'm teaching the girls, they get frustrated and uncomfortable when they don't *get* something. They look to their neighbors to see what they did and just type what their neighbor typed.

They equate an error message with a failure and they freeze instead of viewing it as feedback. The traditional school model says that failures are to be punished instead of viewed as learning experiences. Failures are viewed as a problem of the student rather than an indicator that a gap in their knowledge exists. Failure isn't bad, hell, sometimes I spend all day working on problem just to get to a failure, because at least a failure provides some feedback, it lets me know what I tried wasn't working and indicates where I should look. A failure, to me, is a signal that I need to head in a different direction.

## Relearning What It Means To Fail

I remember when I first started programming, I would get a failure and I would freeze, I was upset and scared to fail more. I was so conditioned to believe that failures meant I was stupid, that I was incapable, that I should be punished, rather than viewing it as an opportunity to learn, viewing it as vital feedback.

Nearly every class I hear a girl take a heavy sigh and exclaim

> "I hate these error messages."

They tend to look at me like I have two heads when I say that error messages are one of my favorite parts of programming.

To the students, there is one right answer, you bubble it in your Scantron sheet, and that's that. It rarely matters how you got to the solution, just that you got there. With programming I would argue that *almost* the exact opposite is true, especially early on.

Creating simple games like MadLibs, and drawing pictures with JavaScript isn't about getting the final solution. While, I am happy when their happy because they finished their game, or their drawing, I try not to put too much emphasis on them getting to the final solution. I'd much rather spend two weeks going over variables than rush ahead so they can start working on building games if they're not ready to move on.

Making sure that everyone has the building blocks necessary to move on to the next lesson is so important when it comes to programming, everything builds off everything else, I don't want anyone to get left behind. This requires making sure that when students aren't understanding something they know that it's a safe space to indicate that they're lost. I remember being in school and even if I didn't understand something, and the teacher would ask "did everyone get that!" I didn't want to be the person who raised their hand and look *stupid* in front of everyone else.

I try to make the classroom a safe space by making mistakes in front of the kids so I can get their feedback on why it's not working, I've left off semicolons or I've named a variable `lastName` and tried to reference it as `lastname`. I think that letting the students correct me not only lets me know they actually understand why something is failing and how to fix it, but also builds their confidence, and lets them see that even a programmer makes mistakes.

When teaching programming I don't want to put too much value on the end project. The goal, for me as their teacher, isn't to get them to complete a game, but rather for them to understand all the small building blocks that go into building a game so they can build other games, and build on that knowledge to create bigger games, and bigger applications. If we get to the end of this class and none of the girls have completed their final projects, but they all understand the building blocks necessary to build games and applications, and they've learned how to *learn*, I still feel confident marking this endeavor as a success.

If I could make one thing truly stick with the girls as they continue on their
programming paths I hope it'll be the knowledge that a failure is feedback. I
hope that they will not freeze and be scared to move in any direction when
they're presented with an error message, rather they will know that the computer
is simply letting them that *"you're getting colder"*, or *"you're getting hotter"*.

---
title: "Back to the Drawing Board"
date: 2013-12-14
categories: ["Apprenticeship"]
---

Taryn and I were working on a project remotely where we would be taking two different CSV files, extracting the data, transforming it in someway, merging duplication from the two spreadsheets into one spreadsheet, and then uploading it.

Our client is Paul, the CEO at 8th Light, and he originally wanted the data to be in spreadsheet form, so we showed him our finished spreadsheet on Friday. We demoed it as if he were a client, and asked him what he thought of it.

He told us that yes, while this was what he asked for, he's figuring out that it's not exactly what he wants. Instead thinks the data would be better served as an application, and we were inclined to agree. We will be moving into a new phase soon where we will be working on this application.

It's interesting to me to work with someone else on a project with someone who isn't my mentor acting as a client. We have a real project, a client, and an end-goal. We get to experience changing requirements, new ideas, and will be able to get a better feel of how it might feel once we're no longer apprentices and are working on projects with clients.

I'm excited for the application because it's going to change the way we approach our apprenticeship application process. We're going to be more informed about the people who apply at our company for an apprenticeship, we'll be able to see the trends, expedite our reply process, our code review process, and see what demographics we aren't reaching and how we can change that.

I'm sure I'll be writing more about the project and the application as we move into the planning and developing stage. It's exciting to get to work on an internet project that will actually be used be our coworkers. It's a new type of challenge, and will be a nice change of pace.


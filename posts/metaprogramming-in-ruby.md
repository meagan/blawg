---
title: "Metaprogramming in Ruby"
date: 2013-04-30
categories: ["Ruby", "Code", "Tutorial"]
---

While learning about Ruby and trying to find out ways to improve upon my progress in my Tic Tac Toe project, I kept stumbling on "Metaprogramming". It sounded difficult, or at least advanced. I decided, before even learning about it, that I wasn't ready to learn that yet. Well, I found myself reading code on some random stack_overflow example and being intrigued at what was written. What were they trying to accomplish? I found out, by reading the answers, that they were using *metaprogramming*. That was the impetus that got me to reading about metaprogramming and immediately implementing it into some parts of my code that were stumping me.

## What is Metaprogramming

Metaprogramming's definition is: The writing of computer programs that write or manipulate other programs(or themselves) as their data, or that do part of the work at compile time that would otherwise be done at runtime. In many cases, this allows programmers to get more done in the same amount of time as they would take to write all the code manually, or it gives programs greater flexibility to efficiently handle new situations without recompilation. [source](http://en.wikipedia.org/wiki/Metaprogramming).

Basically, metaprogramming is writing code that writes code.

I am going to let my code speak for itself.

(Big shout outs to [Cory](http://twitter.com/cory_foy) for helping me with a pesky deprecation error that I kept running into. If anyone is running into this I am going to show you how it was fixed)

{% highlight ruby %}
class Magical
  def entrance
    puts 'The entrance that a certain magical being would have'
  end
end

class Wizard < Magical
  def entrance
    puts 'The Ground Starts to Shake'
  end
end

class Dragon < Magical
  def entrance
    puts 'Flames Appear All Around'
  end
end

class Person
  attr_accessor :name
  def initialize(name)
    @name = name
    @dead = false
  end

  def kill!(by = nil)
    puts "I am #{name}, the new hero!"
    puts "Looks like I was bested by a measly #{by.class}, avenge me...." if by
    puts
    @dead = true
  end

  def dead?
    @dead
  end
end

module Fight
  def self.apply_to_all_magicals
    magical_classes = Module.constants.reject { |c| c == :Config }.map { |x| Module.const_get(x) }
    .select { |x| x.respond_to?(:superclass) && x.superclass == Magical }
    magical_classes.each do |magical_class|
      magical_class.module_eval do
        include Fight
      end
    end
  end

  def battle(person)
    entrance
    puts "The Sky Goes Grey, Ominious Music is Heard"
    person.kill!(self)
    puts
  end
end

wizard = Wizard.new
dragon = Dragon.new
people = [Person.new('The Great and Powerful Fred'), Person.new('The Not So Great and Way Less Powerful Ted')]
begin
  wizard.battle(people[0])
rescue => e
  puts 'The Last Hero Was Slain By Something Mystical'
end

begin
  dragon.battle(people[0])
rescue => e
  puts "However, As Successor to the Hero Line, I Shall Kill the Creature or Die Trying!"
end
puts

# DESTROY
Fight.apply_to_all_magicals
wizard.battle(people[0])
dragon.battle(people[1])
{% endhighlight %}

That above code will make this happen
	The Last Hero Was Slain By Something Mystical
	However, As Successor to the Hero Line, I Shall Kill the Creature or Die Trying!

	The Ground Starts to Shake
	The Sky Goes Grey, Ominious Music is Heard
	I am The Great and Powerful Fred, the new hero!
	Looks like I was bested by a measly Wizard, avenge me....

	Flames Appear All Around
	The Sky Goes Grey, Ominious Music is Heard
	I am The Not So Great and Way Less Powerful Ted, the new hero!
	Looks like I was bested by a measly Dragon, avenge me....


Pretty cool, right? I'm just getting started with metaprogramming, but I am having a lot of fun with it. I mentioned that I kept getting a deprecation error.When I would run the ruby file it would run, but would print this:
	Use RbConfig instead of obsolete and deprecated Config.

It was saying I was running into this error on Line 40/Line 41/Line 42, so thanks to Cory that problem was fixed by doing this

{% highlight ruby %}
# Before
module Fight
  def self.apply_to_all_magicals
    magical_classes = Module.constants.map { |x| Module.const_get(x) }
    .select { |x| x.respond_to?(:superclass) && x.superclass == Magical }
    magical_classes.each do |magical_class|
      magical_class.module_eval do
        include Fight
      end
    end
  end

 # When you map over the constants, Config is included as a constant, among
 # many, many others, however it will print the deprecated error to let you to
 # know to stop using it and to use RbConfig instead, but it is backwards
 # compatiable, so everything else still looked like it worked. The solution was this

 module Fight
  def self.apply_to_all_magicals
    magical_classes = Module.constants.reject { |c| c == :Config }.map { |x| Module.const_get(x) }
    .select { |x| x.respond_to?(:superclass) && x.superclass == Magical }
    magical_classes.each do |magical_class|
      magical_class.module_eval do
        include Fight
      end
    end
  end

  # By adding the .reject { |c| c == :Config } we rejected :Config as a constant
  # No more error, we don't map over it anymore!

 {% endhighlight %}


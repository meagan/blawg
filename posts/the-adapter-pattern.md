---
title: "The Adapter Pattern"
date: 2014-01-02
categories: ["Design Patterns", "Java", "Code", "Tutorials"]
---

Adapters help incompatible interfaces work together. We use adapters in the real world such as:

[![iphoneadapter.jpg](https://d23f6h5jpj26xu.cloudfront.net/7ddfxp8gtdgow_small.jpg)](http://img.svbtle.com/7ddfxp8gtdgow.jpg)

For our electronic devices, or *"what to do with all those iPhone 4 cords I have laying around"*.

Take the adapter above, if you bought an iDevice that uses a lightning cable to charge/sync you might have a few of the "old style" cables lying around that are now sort of useless, right? You might even lose your lightning cable and have to fork over $20-$30 to buy a new one. Instead of buying a new cable, you could use the above adapter to charge your device.

The older style charging cables & ports are different from the new ones that use the lightning cables, you can't charge an iPhone 5 with an iPhone 4 charger. If you use the adapter in between you can fit an iPhone 4 charger plug into the iPhone 5 socket.

I'm sure you have used, or currently use, an adapter in some capacity. The adapter design pattern is also used for getting incompatible interfaces to work together. Just like with the adapter above, the interfaces may be incompatible but the inner functionality should suit the need.

## Generic Adapter Design Pattern
[![Screen Shot 2014-01-02 at 5.54.08 PM.png](https://d23f6h5jpj26xu.cloudfront.net/535hgogrkzhwow_small.png)](http://img.svbtle.com/535hgogrkzhwow.png)

The above UML diagram shows a generic adapter design pattern implementation.

### Elements
 - **Target**: Defines domain-specific interface client uses
 - **Client**: Collaborates with objects that implement the Target interface
 - **Adapter**: Adapts the Adaptee to the Target interface, it translates the request from the Client to the Adaptee.
 - **Adaptee**: Defines existing interface that needs adapting in order for the client to interact with it.

The client calls operations on the adapter instance, and the adapter calls adaptee operations that carry out the request.

## Our Problem

I'm going to demonstrate the use of the Adapter pattern with the example of the iPhone chargers above. Our problem is that we have an iPhone 5, but only have iPhone 4 cords, we need an adapter to let us charge our iPhone 5.

## Implementing the Adapter Design Pattern

````
package com.iphone;

// Target interface
public interface Charger {
	// Request method
	public void plugIn();
}
````

````
package com.iphone;

// Target Class
public class IPhone4 implements Charger {

	@Override
	public void plugIn() {
		System.out.println("Your iPhone 4 is charging.");

	}

}
````

````
package com.iphone;

// Adaptee Class
public class IPhone5 {
	// Special request method

	public void AdapterPlugIn() {
		System.out.println("Your iPhone 5 is charging.");
	}

}
````

````
package com.iphone;

// Adapter Class

public class IPhone5Adapter extends IPhone5 implements Charger {
	// Adapts special request to a standard request
	@Override
	public void plugIn() {
		AdapterPlugIn();
	}
}
````

````
package com.iphone;


// Client class
public class Adapter {
	public static void main(String[] args) {
		// Standard Device (iPhone 4)
		Charger iPhone4 = new IPhone4();

		// Specific Device (iPhone 5)
		Charger iPhone5 = new IPhone5Adapter();

		// Devices are now using the same method
		iPhone4.plugIn();
		iPhone5.plugIn();

	}

}
````

Now if we were to run our `Adapter` class, the console will display:

````
Your iPhone 4 is charging.
Your iPhone 5 is charging.
````

## Using the Adapter Pattern
The main use case for this pattern is when a class that you need to use doesn't meet the requirements of an interface. This might be handy if you were using a 3rd party API, like my problem with JRuby and Java that I mentioned in a [previous blog post](http://blog.meaganwaller.com/using-jruby-irb).

Keep in mind that there are some cons when it comes to using the Adapter Pattern. Some might say this pattern is a fix for a badly designed system that didn't consider all potential possibilities. It can also add complexity to your code that might make debugging difficult.

---
title: "Mock and Stub in RSpec"
date: 2013-04-22
categories: ["Ruby", "Testing", "Tutorials"]
---

*Note: This blog post is going to be a combination of Wednesday and Thursday since I had some issues yesterday (see my twitter for information)*

In testing we want to progress in small, verifiable steps, but how can we know that an individual object is properly executing its role if an object it delegates work to doesn't exit? In come *test doubles*
A test double is an object that stands in for another object in an example, you might've heard of names like mock objects, test stubs, fakes, and others.

If you want to create a double simply use the <code>double()</code> method:

{% highlight ruby %}
example_double = double('example')

# The string argument is optional, but recommended because it is used in failure messages.
# stub() and mock() also produce the same kind of object.

stub_example = stub('example')
mock_example = mock('example')
{% endhighlight %}

We can use <code>mock()</code> and <code>stub</code> to make the spec clearer when appropriate. A little deeper information, the name of the RSpec library used to generate test doubles is <code>RSpec::Mocks</code>. This means that all three methods(<code>double(), mock(),stub()</code>) all provide an instance of the <code>RSpec::Mocks::Mock</code> class, this provides facilities for generating method stubs and message expectations.

### Method Stubs
A *method stub* is a method that can return predefined responses during the execution of a code example.

{% highlight ruby %}
describe Game do
	it "greets the player by name" do
		player = double('player')
		player.stub(:name).and_return('Alice')
		greeting = Game.new(player)
		greeting.generate.should =~ /^Hello Alice!/
	end
end
{% endhighlight %}

In this example we see that the greeting uses the player's name to generate the greeting. The player double stands in for the real <code>player</code>. Because Ruby is dynamically typed the player can be of any class, as long as it responds to the correct methods.

After Describing Game and what it does (greets player by name), we created a test double using the <code>double()</code> method. Next we created a method stub with the <code>stub()</code> method. This takes a single argument: a symbol representing the name of the method that we want to stub. We follow that with a call to <code>and_return()</code>, this tells the double to return <code>'Alice'</code> in response to the <code>name()</code> method.

{% highlight ruby %}
class Game
	def initialize(player)
		@player = player
	end

	def generate
		"Hello #[@player.name}!"
	end
end
{% endhighlight %}

When this example is executed, the code <code>def generate</code> in the example sends the <code>generate()</code> message to the <code>Game</code> object. This is the object in development and is a real <code>Game</code>.

When the <code>Game</code> executes the <code>generate()</code> method, it asks the <code>@player</code> for its <code>name()</code>. The player is not the focus of this example. It is an immediate collaborator of the <code>Game</code> and we're using a test double to stand in for a real <code>Player</code> in order to control the data in the example. It returns <code>'Alice'</code>, so the result is "Hello Alice!" and the example passes.

We could use the <code>generate()</code> method like this

{% highlight ruby %}
def generate
	"Hello Alice!"
end
{% endhighlight %}

This is the simplest thing we could do to get the example to pass, which is what traditional TDD instructs us to do first. What we do next varies from person to person. Some people will say that the next best thing is to triangulate, which is to add another example that uses a different value. This forces the implementation to generalize the value in order to pass both examples. Other people will make <code>'Alice'</code> in the implementation as a duplication with the <code>'Alice'</code> in the example. This follows the DRY principle -- we remove the duplication by generalizing the implementation.

However, both of these approaches have their downfalls. Triangulation requires an extra example that specifies the same essential behavior, and DRY, while a worthy justification, requires we take that extra step. This approach will occasionally result in hard-coded values remaining in implentation code. Don't fret, because there is another option.

### Message Expectations
A message expection, or a mock expectation, is a method stub that will raise an error if it is never called. In RSpec you can create a message expectation using the <code>should_receive()</code> method, like this:

{% highlight ruby %}
describe Game do
	it 'greets the player by name' do
		player = double('player')
		player.should_receive(:name).and_return('Alice')
		greeting = Game.new(player)
		greeting.generate.should =~ /^Hello Alice!/
	end
end
{% endhighlight %}

When we use <code>should_receive()</code> instead of <code>stub()</code> an expectation that the player double receive the <code>name()</code> message is set. The following <code>and_return()</code> works just like it did previously: it is an instruction to return a specific value in response to <code>name()</code>

In the above example, if the <code>generate()</code> method fails to ask the player double for its name then the example will fail with <code>Double 'player' expected :name with (any args) once, but received it 0 times</code>. If the <code>generate()</code> method calls <code>player.name()</code>, then the player double returns the programmed value, execution continues and the example passes.

This example is highly coupled to the implementation but this is justified. We are specifying that the greeting uses the player's name. If that is the requirement that we are expressing in this example, then setting a message expectation is perfectly reasonable. We do want to avoid setting message expectations that are not meaningful in the context of an example. Generally, speaking we only want to use message expectations to express the intent of the example. To further drive this point home, let's use both method stub and a message expectation.

### Mixing Method Stubs and Message Expectations

Extending the greeting examples, let's add a requirement that any time a greeting is generated a log entry gets created.

{% highlight ruby %}
describe Game do
	it "logs a message on generate()" do
		player = stub('player')
		player.stub(:name).and_return('Alice')
		logger = mock('logger')
		greeting = Game.new(player, logger)

		logger.should_receive(:log).with(/Hello Alice!/)

		greeting.generate
	end
end
{% endhighlight %}

We now have three participants in the example: the greeting, which is the subject of the example, the logger which is the primary collaborator, and the player is the secondary collaborator. The logger is the primary collaborator because the example's docstring states that the <code>Greeting logs a message on generate()</code>.

By using the <code>mock()</code> method to generate the logger double and the <code>stub()</code> method to generate the player double, we're expressing that these objects are playing different roles in the example. This is the technique that the RSpec book recommends, it embeds intent right in the code example.

The <code>logger.should_receive()</code> is the only expectation in the example, and it comes *before* the event, the When. The flow is **Given, Then, When** as opposed to Given, When, Then, that you might be used to if you're familiar with cucumber. *Given* a greeting constructed with a player and a logger, *Then* the logger should receive <code>log()</code> *When* the greeting receives <code>generate()</code>.

So far we've discussed setting method stubs and message expectations on test double objects. This is a very useful technique when the collaborators we need don't exist yet, or maybe they're very expensive to set up or use in a code example.

---
title: "Different Subtypes of List in Java"
date: 2013-11-18
categories: ["Java", "Code", "Tutorails"]
---

A List Interface in Java is an ordered Collection, sometimes called a Sequence. Lists differ from Sets because they can contain duplicate elements. List inherits operations from Collection, and also includes the following operations:

  - Positional access: Manipulates elements based on their numerical position in the list. This includes methods such as: get, set, add, addAll, and remove.
  - Search - searches for a specified object in the list and returns its numerical position. Search methods include indexOf and lastIndexOf.
  - Iteration - extends Iterator semantics to take advantage of the list's sequential nature. The listIterator methods provide this behavior
  - Range-view - The sublist method performs arbitrary range operations on the list.

Java contains two general-purpose List implementations.

- ArrayList - The better performing implementation
- LinkedList - Offers better performance under certain circumstances


## Collection Operations

I mentioned earlier that Lists are ordered Collections, and they inherit from Collection. The operations inherited from Collection perform the way you'd expect them to.

- remove - always removes the first occurrence of the specified element from the list
- add and addAll - always append the new element(s) to the end of the list

### Example

- To concatenate one list to another: list1.addAll(list2);
- Producing a third List consisting of the second list appended to the first:
    - List<Type> list3 = new ArrayList<Type>(list1);
    - list3.addAll(list2);
- Aggregates some names into a List:
    - List<String>list people.stream().map(Person::getName).collect(Collectors.toList());
Like the Set interface, List strengthens the requirements on the equals and hashCode methods so that two List objects can be compared for logical equality without regard to their implementation classes. Two List objects are equal if they contain the same elements in the same order.



## Positional Access and Search Operations

The basic positional access operations are get, set, add and remove. The set and remove operations return the old value that is being overwritten or removed. There are other operations like indexOf and lastIndexOf, which return the first or last index of the specified element in the list. The addAll operation inserts all the elements of the specified Collection started at the specified position. The elements are inserted in the order they are returned by the specified Collection's iterator.

### Example

A polymorphic algorithm to swap two elements in any List, regardless of implementation type:
````
public static <Example> void swap(List<Example> a, int i, int j) {
 Example temp = a.get(i);
 a.set(i, a.get(j));
 a.set(j, tmp);
}
````

An algorithm to randomly permutes the specified list using the specified source of randomness It's a bit subtle: It runs up the list from the bottom, repeatedly swapping a randomly selected element into the current position.
````
import java.util.*;
public class Shuffle {
 public static void main(String[] args) {
 List<String> list = new ArrayList<String>();
 for (String a : args)
  list.add(a);
 Collections.shuffle(list, new Random());
 System.out.println(list);
  }
}
````

We can make this program shorter and faster by using the Arrays class. The Arrays class has a static factory method called asList, which allows an array to be viewed as a List. This method doesn't copy the array, and changes in the List write through to the array and vice versa. The resulting List is not a general-purpose List implementation, because it doesn't implement add and remove operations (which are optional). Arrays are not resizeable.

````
import java.util.*;
public class Shuffle {
public static void main(String[] args) {
 List<String> list = Arrays.asList(args);
 Collections.shuffle(list);
 System.out.println(list);
 }
}
````

## ArrayList

ArrayList is a resizable-array implementation of the List interface. It implements all the optional list operations, and permits all elements, including null. In addition to implementing the List interface, the ArrayList class provides methods to manipulate the size of the array that is used internally to store the list.


## Operations

- Run in Constant Time
   - size
   - isEmpty
   - get
   - set
   - iterator
   - listIterator
- Run in amortized constant time (adding n elements requires O(n) time)
   - add
- All the other operations run in linear time (for the most part)
- The constant factor is low compared to that for the LinkedList implementation

## More about ArrayList

Each ArrayList instance has a capacity. The capacity is the size of the array used to store the elements in the list. It is always at least as large as the list size. As elements are added to an ArrayList, its capacity grows automatically. The details of the growth policy are not specified beyond the fact that adding an element has constant amortized time cost.

## LinkedList

The doubly-linked implementation of the List interface is called the LinkedList. It implements all optional list operations, and permits all elements, including null. All of the operations perform as expected for a doubly-linked list. Operations that index into the list will traverse the list from the beginning or the end, whichever is closer to the specified index.

## AbstractList

The AbstractList class provides a bare-bones implementation of the List interface to minimize the effort required to implement this interface backed by a "random access" data store, like an array. For sequential access data, like a linked list, you should the AbstractSequentialList instead. If you'd like to implement an immutable list, you need to only extend this class and provide implementations for the get(int) and size() methods. To implement a mutable list you must also override the set(int, E) method, if the list is variable-size you must also override the add(int, E) and remove(int) methods. You should provide a void and collection constructor, as recommended in the Collection interface specification. Unlike the other abstract collection implementations, you dob't have to provide an iterator implementation; the iterator and the list iterator are implemented by this class, on top of the "random access" methods: get(int), set(int, E), add(int, E). and remove(int).


## AbstractSequentialList

Similar to the AbstractList class, this class also provides a bare-bones implementation of the Lit interface, but it minimizes the effort required to implement this interface backed by a "sequential access" data store, like a linked list. This class is the opposite of the AbstractList class because it implements the "random access" methods (get(int index), set(int index, E element), add(int index, E element), and remove(int index)) on top of the list's list iterator, instead of the other way around. To implement a list you need to only extend the class and provide implementations for the listIterator and size methods. For an immutable list, you need only implement the list iterator hasNext, next, hasPrevious, previous, and index methods. For a mutable list you should additionally implement the list iterator's set method. For a variable size you should additionally implement the list iterator's remove and add methods. You should also generally provide a void and collection constructor, as the recommendation in the Collection interface specification.

---
title: "Negamax Implementation"
date: 2013-06-17
categories: ["Ruby", "Apprenticeship", "Algorithms"]
---

I've touched on a few different algorithms in this blog so far, Negamax and Minimax. However, looking back at those posts I feel they are pretty immature and don't demonstrate the fact that I understand the algorithm.

The reason for that is because I didn't quite understand the algorithms when writing those posts. While I was in Chicago last week I was able to finally get my negamax implementation to work. My Tic Tac Toe game is unbeatable, and I am finally wrapping my head around how negamax works. I discovered that my lack of understanding the algorithms really stemmed from my lack of understanding recursion.

Sure, I wrote a blog post on it, but again, that was kind of an elementary post with elementary examples. When you get into the recursion that an algorithm like negamax requires it's a little harder to wrap your head around than with the recursion that solving Fibonacci's sequence requires. Let's break down the negamax algorithm now and try to get a better understanding of what is actually happening.

## What is Negamax?

I touched on this in my previous post, but it's always nice to have a refresher. The negamax algorithm is one that is used in two-player, zero-sum games. It's a variant on the minimax algorithm in the sense that what's good for X is bad for O and vice-versa.

However, unlike minimax, negamax is done in a single method by calling the negation of negamax instead of calling min or max depending on the player on move. The value is always calculated from the point of view of the player running the algorithm, and the method passes in a color argument, this is the key difference between minimax and negamax, without the color parameter, the evaluation method would need to return a score for the current player (i.e, min or max).

The negamax algorithm has three parameters, node, depth and color, and we will touch on all of those in this post. Before we touch on those parameters, let's make sure we understand exactly how a game tree works, since it's the foundation of the negamax algorithm.

## Game Trees

A game tree is used to represent positions and moves. The positions are commonly referred to as nodes, the root node is used to represent the current position of the game.

Nodes have branches, the branches represent the legal moves from the position that a specific node is representing. A node is known as a leaf if it doesn't have any more legal moves that can be made from it, an easier way to say that is by saying it doesn't have any successors. When we have a leaf, we evaluate if it's a win, lose, or a draw by assigning value.

For example, if its O's turn, and there are only two moves left, and one of the moves results in a win for O, and other move will block X from winning, O should take the move that results in a win, versus going for a block. In order for O to know to take the win over a block, we evaluate the value, in my implementation this means that if the winner is the player, the value is 1, if winner is the opponent the value is -1, and if a tie is result, the value is 0.

## The Parameters

We learned above that negamax passes in three parameters, node, depth and color. For the sake of continuity, I am going to be referring to node as board, and color as player, because that's the name of the parameters in my negamax implementation.

## Board Parameter

The first parameter that the negamax passes in is the board. The board is the current game state, the board is never permanently altered throughout the method. If you're cloning or duping the board, you would pass in the cloned/duped version. What I did to avoid altering the actual board state as the negamax looked through the moves, is I undid the moves after calling the recursive method.


````
board.available_spaces.each do |space|
  board.place_mark(space, player)
  rank = -negamax(board, opponent, depth + 1)
  board.undo(space)
end
````

What this is doing is iterating through all the unmarked spaces, since the method available_spaces returns an array of all the unmarked spaces. Next, for every unmarked space, a mark is being placed with the place_mark method, a rank is assigned by calling the negation of negamax, and then we undo the move with the undo method, setting it back to its original state, and then move on to the next empty space.

## The Player Parameter

The next parameter is the player parameter. This parameter is the marker of the player, in this case, Tic Tac Toe, the player is either X or O. We set the player when we call the negamax negamax(board, @marker, 1), @marker is an attr_accessor that the Computer class initializes with


````
module TicTacToe
  class Computer
    attr_accessor :marker

    def initialize(marker)
      @marker = marker
    end
  end
end
````

So, if we want the computer to have the marker of X we will do so by ai = Computer.new('X').You might've also noticed in the board parameter section the code: -negamax(board, opponent, depth + 1), opponent is determined by this method:


````
def opponent(marker)
  marker == "X" ? "O" : "X"
end
````

In the negamax algorithm, we set opponent like this: opponent = opponent(player), so the opponent of X is O and the opponent of O is X.

## The Depth Parameter

The final parameter is the depth parameter. The depth is how far down the Game Tree the algorithm searches. In my implementation I didn't limit the depth search, however, in some cases, like if I were to make my Tic Tac Toe a 4x4 or 5x5 grid, I'd want to limit the depth searching. Depth was the part of the algorithm that gave me the most trouble. I didn't quite understand its purpose. Every time the computer looks further down the game tree, it's increasing the depth. In Tic Tac Toe, we start with 9 possible moves, since there are 9 blank spaces. Each of those potential moves will stem 8 potential moves, each of those 8 moves with stem 7 potential moves, and so on. If you limit the depth searching, to say, 4, the computer will look 4 moves into the future taking into account every possible legal move that could happen. If you don't limit the depth searching, the computer will look all the way until it hits a terminal state (winner, loser, draw).

## Negamax Explained

Let's just start by looking at my negamax algorithm, and then breaking it down:


````
 1 def negamax(board, player, depth)
 2   if board.is_game_over?
 3     return value(board, player)
 4   else
 5     best_rank = -9999
 6     opponent = opponent(player)
 7     board.available_spaces.each do |space|
 8       board.place_mark(space, player)
 9       rank = -negamax(board, opponent, depth + 1 )
10       board.undo(space)
11       if rank > best_rank
12         best_rank = rank
13         @best_move = space if depth == 1
14       end
15     end
16     return best_rank
17   end
18 end
````

- Line 1 is the method, and the three parameters, board, player, depth, which we discussed above.
- Line 2 sets the base condition for the algorithm, it's seeing if the game is over by calling a method defined in the board class. The `is_game_over` method returns true if there is a winner, or if there are no more available spaces. If the base condition comes back as true, the value is returned. The value is defined in the computer class:


````
def value(board, player)
  if board.winner == player
    return 1
  elsif board.winner == opponent(player)
    return -1
  else
    return 0
  end
end
````

However, if the base condition isn't met, we move onto the else part of our loop.
- In line 5 we set the `best_rank` to `-9999`. This is just the preset return value and it doesn't have to be `-9999`, it just has to be a preset value that we compare the negamax score to later on in the loop.
- In line 6 we set the `opponent`, I already touched on this above, this just sets the opponent to the opposite marker of the player (if computer is X, opponent is O).
- Line 7 is the start of the real nitty-gritty of the negamax. We start by iterating over all the spaces that are available. For each available space a mark is then placed with the `place_mark` method, the mark is then ranked by calling the negation of negamax, and passing in the board, the opponent (which we defined in line 6), and depth + 1. This is the part of the algorithm that makes negamax different than minimax, this is where the player switch happens. The computer first looks at the moves as itself, then looks at the moves the opponent could do in response to the computer's potential move and it gets ranked accordingly.
- In line 10 we are undoing the move the computer placed so as not to alter the actual board state.
-Next, in line 11, we are saying that if `rank` (defined in line 9) is greater than `best_rank`, defined as -9999 in line 5, then we now redefine `best_rank` as `rank`, which is returned at the end of the method.
- In line 13 you see `@best_move = space if depth == 1`, what this means is that if a terminal node (one where there is a winner), is only 1 step away (if computer can win in the next move, block a win, or tie), we should return that instead. This will make it so that the computer will take a win over a block.

## Test Cases

In order to test if the negamax was working properly I ran a number of tests, starting at the simplest case.

1. Correct opponent returned
2. In a filled board, the value is either 1, -1, or 0, depending on if the computer wins, the human wins, or it's a draw
3. When there's one space available, and it's the computer's move, the computer takes the space
4. When there's two-eight spaces available, and it's the computer's move, it wins instead of blocks, or it blocks instead of losing, or it picks a move if the outcome is a tie regardless.
5. When computer makes the first move, it places a mark on the board (I have my computer taking a random move so it's not just taking the same move every time the computer goes first)
6. Check to make sure the computer prevents forks from the opponent, so if the opponent has a marker on the corners, and the computer is in the center, the computer shouldn't take a corner.

## In Conclusion

I'm really excited that I was able to finally push a working, unbeatable, Tic Tac Toe game. My next apprentice task is to explain to Negamax to Will, to show that I understand how it works. Will has also instructed me to read the book Data Structures and Algorithms, and to do some exercises as I go through the text book. After that, I will be doing a totally TDD Tic Tac Toe game. I really feel like pieces are starting to come together in my head, all the gaps in my knowledge that made me feel like I was just wasn't getting it, are slowly getting smaller, and smaller, and I'm so excited to get going and get on with the rest of my apprenticeship.

---
title: "Reflection on 2013"
date: 2014-01-01
categories: ["Apprenticeship"]
---

2013 has been the year of change for me. I started off the year working retail, as I had been doing for the past four years. On January 1st of 2013 I hadn't ever written a single program, I had built websites with HTML/CSS, but never anything beyond that and definitely not professionally.

It wouldn't be until February of 2013 that I'd hear about 8th Light for the first time and send an email to Cory Foy to get more information about the apprenticeship.

I visited the 8th Light office in Tampa for the first time in the middle of February *(at the time of my visit I still hadn't written anything except HTML/CSS)*.  I paired with Cory, he wrote code I helped think of the next steps to take. I got to meet everyone in the office, and after talking to Cory and learning about the next steps in the apprenticeship application process I went home and started working on my unbeatable Tic Tac Toe.

I had no idea what I was doing, but I bought a Python programming book, and got to work on writing my first program. About a week and a half after I initially visited the office, I submitted my Tic Tac Toe, it wasn't unbeatable, but at this point I was driven to figure it out. I saw a glimpse of how awesome a future outside of retail could be, how awesome it felt to light up pixels on the screen with keystrokes. I hadn't felt driven to really solve problems like I had with this one.

I have picked up lots of "hobbies" throughout the years. Crocheting, figure skating, running, cooking, baking, piano, guitar, crafting, scrapbooking, gardening, just to name a few. I'd get really excited and invest a bunch of time and even sometimes some money, and then drop the hobby a week or two later when I wasn't perfect at it. For some reason, it was different with coding. The failures drove me to try harder, and when something started working, I was instantly reminded of why I liked it so much.

On March 28th, the day before my birthday, I accepted an offer at 8th Light as a resident apprentice. I went from having never written a single line of software code to being accepted as a resident apprentice at 8th Light in a little over a month. I was jobless at this point because I quit my retail job almost immediately after my initial meeting with Cory and the rest of the Tampa office. I knew that this was what I wanted to do, and I couldn't stop thinking about writing software while I was folding t-shirts so I decided to put 100% of my time into teaching myself so I could get accepted into the apprenticeship.

Anyway, I've come along way, I've been coding for less than a year and sometimes it's really easily to see how far I have to go and totally miss how far I've come. In 2014 I want to set goals so I can progress even more, and I want to keep my eyes on my goals while also remembering how far I've come.

## Goals for 2014

- Blog & Read more. As per the apprenticeship I should be blogging & reading everyday. But I'm not going to be an apprentice forever, after my apprenticeship I still want to blog at least weekly, and read at least one technical or otherwise related book a month.

- Work on my own projects: I have a few ideas for some software that I really want to make happen. I'd love to get them started and production ready, and then open source them and try and get some traction going. I'm really excited about a couple of my ideas, but I'm scared of putting my name on something that will not be "perfect". I want to figure out how to get over that fear and put my code out there more.

- Daily exercises: I want to do daily exercises so I can continue to do new things and hone my craft. I want to do more of [exercism exercises](http://exercism.io/meaganewaller), and do a kata every morning before work to get my mind ready to code.

## Wrap Up
I'm really excited for what 2014 has to offer, I'm starting off the year by moving across the country so I can continue my apprenticeship at  the Chicago office. I've come along way since 2013, and I can't wait to see what my reflection on 2014 post will look like next year.

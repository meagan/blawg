---
title: "Template Method Pattern"
date: 2014-01-31
categories: ["Design Patterns", "Ruby", "Code", "Tutorials"]
---

The Template Method design pattern is sort of the building block for a lot of other patterns. It's relatively simple, so this blog post won't be as long as some of the other design pattern blog posts that I've had.

The Template Method pattern is simply a fancy way of saying that if you want to vary an algorithm, one way to do so is to code the invariant part in a base class and to encapsulate the variable parts in methods that are defined by a number of subclasses.

  1. Create a skeletal class with methods that are common between algorithms
  2. Create a subclass for each algorithm and override the common methods from the skeletal class.

The disadvantages are lack of runtime flexibility.

````
#html_report
class HTMLReport < Report
  def output_start
    puts '<html>'
  end

  def output_head
    puts '  <head>'
    puts "    <title>#{@title}</title>"
    puts '  </head>'
  end

  def output_body_start
    puts '<body>'
  end

  def output_line(line)
    puts "  <p>#{line}</p>"
  end

  def output_body_end
    puts '</body>'
  end

  def output_end
    puts '</html>'
  end
end

# plain_text_report
class PlainTextReport < Report
  def output_start
  end

  def output_head
    puts "**** #{@title} ****"
    puts
  end

  def output_body_start
  end

  def output_line(line)
    puts line
  end

  def output_body_end
  end

  def output_end
  end
end

# report
class Report
  def initialize
    @title = 'Monthly Report'
    @text =  ['Things are going', 'really, really well.']
  end

  def output_report
    puts('<html>')
    puts('  <head>')
    puts("    <title>#{@title}</title>")
    puts('  </head>')
    puts('  <body>')
    @text.each do |line|
      puts("    <p>#{line}</p>" )
    end
    puts('  </body>')
    puts('</html>')
  end
end
````

Now to see this in action, simply run this code in a `main.rb` file:

````
require './report'
require './html_report'
require './plain_text_report'

report = HTMLReport.new
report.output_report

report = PlainTextReport.new
report.output_report
````

You will see:

````
<html>
  <head>
    <title>Monthly Report</title>
  </head>
  <body>
    <p>Things are going</p>
    <p>really, really well.</p>
  </body>
</html>
<html>
  <head>
    <title>Monthly Report</title>
  </head>
  <body>
    <p>Things are going</p>
    <p>really, really well.</p>
  </body>
</html>
````

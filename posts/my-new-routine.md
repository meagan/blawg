---
title: "My New Routine"
date: 2014-01-19
categories: ["Apprenticeship"]
---

Part of being successful is having a routine that you stick to. In an effort to hold myself accountable and to try to nip procrastination in the bud I want to outline what my routine is going to look like through the duration of my apprenticeship:

I've been arriving to the office around 8:20-8:30am, it's great getting to the office early because instead of having to walk around to everyone to high five them, everyone comes up to you. It's also nice getting to the office early because I can take time unpacking my stuff, getting situated and grab a drink  before diving into my day. I take time during my commute if the train isn't standing room only (if I miss the 7:50 train and have to get on the 8:00 train forget about having a place to sit until the first stops in the Loop, it ain't happening).

I've also been planning on doing katas in the morning to warm up. That's another reason why I like arriving to the office early, it gives me time to work on a kata first thing in the morning. Some of my day is more structured and I've accounted for that with time frames, but some of it I need to work on. I'm deciding if I should use pomodoros to keep track of time, or if I need to figure out another way to stay productive.

We also have an apprentice stand up every day at 10am. This doesn't take more than a few minutes but its good to see where all the apprentices are at, and now that residents are going to spending half of their time with the novices we can use standup to talk about who's going to be downstairs and who is planning on giving a presentation that day. It's a good way to catch up and to hold each other accountable.


After apprentice stand-up I would like to dig into my work 100% until lunch time. Sometimes I feel like the first half of my day goes by so quickly and I haven't accomplished much. I feel much more productive from 1pm - 7pm, but if I could become more productive in the morning I wouldn't have to scramble at night to get things finished. Again, I'd like to find a way to be more productive in the morning.

I'm going to be working with Taryn at least one day a week on the internal application we're working on. I'd like to be at 100% productivity during that day, that means not just after lunch. I think this upcoming week I'm going to try to utilize the pomodoro technique and see how that goes. I've never really given that an honest shot and would like to see if that's something that might work for me.

From after lunch until the end of the work day (roughly 5:45-6:30pm for me), I'm usually quite productive. But, once I get home from work it seems like without much happening at all it's already 9:45pm and it's nearly time for bed. I'd like to take advantage of my after work time to finish up with all my apprentice tasks. I've been reading on the train with my kindle, and that's been awesome. I'm currently reading Eloquent Ruby by Russ Olsen and am enjoying it quite a bit. But, I'd also like to not have to scramble to get my blog posts in at the end of the week, or during the weekend.

If I take time during the day and think about what I'm going to write about, maybe just take 15 minutes towards the end of my day and open up an Evernote note and jot down some of what I did during the day it'll probably be easier to create a blog post as soon as I get home, or soon after I get home that's more focused on what I did that day. The problem with waiting is that I end up forgetting details of what I did during the day earlier in the week and am left with more general ideas. If I run into a problem during the day and solve it, or even if I don't, I'd like to start writing that down right away so I can be reminded of it for when I sit down to write my blog post. If I solve the problem I'd like to write down the steps I took or take a screenshot or something so I can reference it later in. If I only have general thoughts because it's been a week since I've thought about it, it's going to be a lot more difficult to write a blog post about.

My goals for this week are to finish all my tasks, and to be able to have stress free weekend where I don't have to crank out blog posts. I'd like to finish Eloquent Ruby up, and also blog everyday after work about specific things I did during the day.

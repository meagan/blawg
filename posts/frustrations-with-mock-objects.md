---
title: "Frustration with Mock Objects"
date: 2013-11-27
categories: ["Apprenticeship"]
---

I am having a really difficult time trying to figure out how to roll my own mocks in Java. I just can't wrap my head around it. I've read many blog posts, tons of StackOverflow answers, documentation of Mock libraries, anything to help me to understand what I'm doing. I don't know why I'm having such a difficult time with this concept, but I am. I feel kind of pathetic not being able to understand this.

I've been working on my Mastermind game, and one of my tasks is to mock System.in/System.out, and to roll my own mocks for some of my classes. I get the reason for mocking. A mock object is supposed to mimic the behavior of real objects in certain, controlled ways. You create a mock object to test the behavior of some other object. You mock to simulate the behavior of complex, real objects when it's impractical or impossible to incorporate the real object into a unit test. You should use a mock object in the place of a real object if it
  - Supplies non-deterministic results, like time or temperature
  - Has states that are difficult to create or reproduce (like a network error)
  - Is slow (like if you're using a database you have to initialize before the test)
  - Does not exist yet, or may change behavior
  - Would have to include information and methods exclusively for testing purposes

An example of a mock object is if you're writing a program that displays the current temperature in any given city. The temperature varies from hour to hour, and sometimes even more frequently than that. Your tests could not always pass even if they were technically correct. In order to test this, you have to wait until the temperature is the same as your assertion, which isn't realistic or right. If you used a mock object you could set it to a certain temperature so that your method that gets the temperature can be tested in isolation.

## Information about Mock Objects

Mock objects have the same interface as the real objects they mimic, the client isn't aware if it's using a real object or a mock object. When it comes to Test Driven Development you're supposed to make use of mock objects. Mock objects meet the interface requirements of, and stand in for, more complex real objects. They allow programmers to write and unit-test functionality in one area without actually calling complex underlying or collaborating classes. When you use a mock object as a developer you are able to focus your tests on the behavior of the system under test without worrying about the dependencies.

## Downfall of Mock Objects

I've attempted to use mock objects with my Ruby tic tac toe implementation. I had 100% code coverage, everything was passing, and when I went to run my program, I was getting dozens of invalid argument errors. I overmocked. I overstubbed. I wasn't testing anything real, really. When it comes to using mock objects you can closely couple the unit tests to the actual implementation of the code that is being tested. This is why a unit test should test a method's external behavior rather than its internal implementation.

## Ideas and Pseudocode

I e-mailed Mike the other day to ask him for help because I was feeling stumped. In my specific example he pointed out that I wasn't testing the behavior, I was testing the implementation. I was wondering how to test my game loop, I (incorrectly) thought I needed to test that a secret code was generated when the game started, I set up my test like this

````
@Test
public void generatesASecretCodeAtStart() {
	Game game = new Game();
	FakeEncoder encoder = new FakeEncoder();
	game.startGame(4);
	assertNotNull(secretCode);
}
````
Mike said this is the place where mocks can come in handy, in order to test my game loop, I need to control both the secret code and the guesses I get from the user. He gave me some pseudocode:

````
class MockEncoder extends Encoder {
	@Override
	public List<Color> chooseSecretCode(int size) {
		return /* whatever 4 greens looks like */;
	}
}

class MockDecoder extends Decoder {

	private List<List<Color>> orderedGuesses;

	public setGuesses(List<List<Color>> guesses) {
		// set up a list of guesses
	}

	@Override
	public List<Color> promptGuess(List<Turn> turns, int size) {
		// return the 1st seeded guess, 2nd seeded guess, etc
	}
}

@Test
public void getGuessesUntilGameIsSolved() {
    Encoder encoder = new MockEncoder();
	Decoder decoder = new MockDecoder();
	decoder.setGuesses(/* all yellows, then all greens */);
	Game game = new Game(encoder, decoder); // inject your dependencies
	game.startGame

	// assert that the while loop finishes, possibly by checking for output to System.out

}
````

And went on to explain that MockEncoder and MockDecoder are mock objects that respond to the same methods and messages as Encoder and Decoder, but I can control their behavior because I'm responsible for setting them up before they're used. I control the setup of these mock objects because I want to predict what the game loop needs in order to drive out the method. I will want to guess N number of times, then output that the user solved the puzzle once they guess the secret code. If I mock the behavior of the real classes, I enable testing of the main loop.

His explanation helped a bit to get the gears turning, but when I sat down to write it, I was still confused, and even more frustrated at myself for not being able to *get this*. I feel like this should be easier to understand than it is, and I'm just not getting it.


## Wrap Up
I feel like if I could have someone sit down with me and go over this, and walk me through a few examples I could understand this. I am hitting a brick wall, reading the same posts over and over again. I understand Mock Objects in theory, but I really don't know how to apply them to my Java mastermind game.

---
title: "Ruby on Rack"
date: 2014-05-01
categories: ["Ruby", "Tutorial", "Code"]
---

Last Friday I gave a [smalltalk at 8th Light
University](http://www.meetup.com/8th-light-university/events/178849452/) about
Rack. I've been really interested ever since doing my Java HTTP server to
understand how frameworks, like Rails, deal with requests, responses, and
routing.

The original idea I had for the talk was to track a request through
Rails. I wanted to see what happened from the moment a user clicked on a link,
or submitted a form in the browser, to when the response was rendered. As I dug
more through Rails source code I found out that the part that ended up being the
most interesting to me was all the stuff that Rack provided, the middleware, or
the stuff that happened between sending the request to the server and returning
the response to the client.

## What is Rack?
Rack is the minimal, modular, and adaptable interface that Ruby frameowkrs like
Rails and Sinatra are built on. Christian Neukirchen is the author of Rack and
in a [2007 blog post titled: "Introducing
Rack"](http://chneukirchen.org/blog/archive/2007/02/introducing-rack.html)
Christian talked about how he was exploring different frameworks and found that
there was a bunch of duplication between them. The goal of every framework is
basically the same, to receive a request and return an appropriate response.

He thought there had to be a better way to do this, every framework developer
shouldn't be writing their own handlers for every web server they want to use
hoping that their users are satisified with the choice that they do make.

In comes Rack. Christian outlined his goals of
Rack to be this:
  > "Rack aims to provide a minimal API to develop web applications in Ruby, for
  > connecting web servers supporting Ruby (like WEBrick, Mongrel, etc) and Ruby
  > web frameworks (like Rails, Sinatra, etc)."

## A Minimal API

Informally, a Rack application is something that responds to call:

````
super_simple_rack_proc = lambda { puts 'Super Simple Rack App' }
super_simple_rack_proc.call
````

This, while not using the Rack gem, is a "Rack" application. Since a lambda
responds to call this meets the requirements of being a Rack application.

It takes a single argument, the `environment`:

````
super_simple_rack_proc = lambda { |env| [200, {"Content-Type" => "text/plain"}, ["Hello, world!"]] }
super_simple_rack_proc.call({})

#=> [200, {"Content-Type" => "text/plain"}, ["Hello, world!"]]
````

The `environment` is an instance of `Hash`, it contains all the information
about the request, `#call` returns us our response. A response is made up of
three things:

    1. The status code `200`
    2. The headers: `{"Content-Type" => "text/plain"}`
    3. The body: `["Hello, world!"]`


## Connecting Web Servers

As I said above, before Rack, Ruby framework developers were writing their own
handlers to deal with web servers. This meant frameworks were opinionated about
the web server that they used, users of those frameworks had little to no say in
what web server they could use when using that framework.

Rack helps out a lot in this regard. Rack abstracts away the common web server
functions, like parsing an HTTP request or HTTP response. Rack also provides a
lot of libraries for commonly used things like query parsing, or cooking
handling. This means that Rack isn't opinionated about web servers, there are
handlers for almost every common Ruby web server, including WEBrick, Mongrel,
Thin, and Puma to name a few.

Rack provides a standard way for Ruby applications to talk to web servers.

````
require 'rubygems'
require 'rack'

class HelloWorld
  def call(env)
    [200, {"Content-Type" => "text/plain"}, "Hello, World!"]
  end
end

Rack::Handler::Mongrel.run HelloWorld.new, :Port => 9292
````

However, you could run this with any of the handlers that Rack has already
provided for you.

Rack also abstracts away that server stuff that we don't really want to have to
deal with if we're building a framework, or even just creating a simple
application.

### Abstracts Away Server Stuff
````
# lib/rack/handler/webrick.rb

module Rack
  module Handler
    class WEBrick < ::WEBrick::HTTPServlet::AbstractServlet
      def self.run(app, options={})
        environment  = ENV['RACK_ENV'] || 'development'
        default_host = environment == 'development' ? 'localhost' : '0.0.0.0'

        options[:BindAddress] = options.delete(:Host) || default_host
        options[:Port] ||= 8080
        options[:OutputBufferSize] = 5
        @server = ::WEBrick::HTTPServer.new(options)
        @server.mount "/", Rack::Handler::WEBrick, app
        yield @server  if block_given?
        @server.start
      end
    ...
  end
end
````

This is from the [WEBrick
handler](https://github.com/rack/rack/blob/master/lib/rack/handler/webrick.rb#L5-L19).
We can see that handling ports are taken care of (`options[:Port] ||= 8080`),
the actual starting of the server (`@server = ::WEBrick::HTTPServer.new(options)`), initializing the servlet (`@server.mount "/", Rack::Handler::WEBrick, app`), and running the server (`@server.start`). Rack does all of this for us so we don't have to worry about it. And if we get sick of using WEBrick we can switch over to using Thin, or Mongrel without having to change any of our framework or application code.

### Parsing HTTP Requests

If we take a look at [Rack's Request
class](https://github.com/rack/rack/blob/master/lib/rack/request.rb#L12-L34) we
can see that Rack parses the `env` hash to give us back useful information. We
don't have to worry about parsing out the nasty `env` hash anymore, and yeah,
it's pretty nasty:

<img src="/images/env.png">

### Parsing HTTP Responses

Rack also parses HTTP responses for us, looking at the [Rack Response
class](https://github.com/rack/rack/blob/master/lib/rack/response.rb#L23-L42), we can see that there is a nice interface to create our Rack responses. We can set headers, and cookies, and we even have some useful defaults. (`status=200`, for example).


## Ruby Web Frameworks

Many Ruby frameworks utilize Rack. If you've done any Rails, or Sinatra development, you've used Rack.

### Rails and Rack

In the [RailsGuides](http://guides.rubyonrails.org/v3.2.13/rails_on_rack.html) there is a section that explains how the `rails server` command works and a bit of code:

````
app = Rack::Builder.new {
  use Rails::Rack::LogTailer unless options[:detach]
  use Rails::Rack::Debugger if options[:debugger]
  use ActionDispatch::Static
  run ActionController::Dispatcher.new

}.to_app
````

Basically `rails server` creates a `Rack::Builder` object and starts the webserver for us. This is the Rails version of Rack's rackup script.

### Sinatra and Rack

From
[Sinatra](https://github.com/sinatra/sinatra/blob/master/lib/sinatra/base.rb#L1467-L1475) we have this:

````
def build(app)
  builder = Rack::Builder.new
  setup_default_middleware builder
  setup_middleware builder
  builder.run app
  builder
  end
````

This also creates a `Rack::Builder` instance, it provides all the middleware set up and the given app as the end point.

Of course there are many more ways that Rails and Sinatra use Rack, so it's worth digging into the source code a bit to see. Knowing about Rack can also help to remove some of the "magic" around Rails.


## Rack Middleware & Rack Applications

Rack's simple protocol makes it easy to compose powerful applications by stacking Rack apps and middleware on top of each other. But what exactly is the difference between Rack apps and Rack middleware? They both seem sort of the same, it's a thing that responds to `#call`. The key difference is that Rack middleware takes an application as an argument on initialize, where a Rack application does not. This means that Rack middleware has knowledge of the Rack application and is able to do things to it at call time.

### Middleware
Rack middleware is a way to implement a pipelined development process for web applications. Middleware can do anything from managing user sessions, to authentication, to caching, to just about anything else. Since Rack middleware has access to the Rack application at call time we can do things to the request before it gets sent to the server, and do things to the response before it gets sent back to the client. Rack middleware comes between calling the client and the server, this is where the name "middleware" comes from, it happens in the middle of all that goodness.


````
class CustomRackMiddleWare
  def initialize(app)
    @app = app
  end

  def call(env)
    # Make the app always think the URL is /foobar
    env['PATH_INFO'] = '/foobar'
    @app.call(env)
  end
end
````

In the above example I'm modifying the path info to make the application always
think that `env['PATH_INFO']` is /foobar. This is something that middleware can
do (although, I'm not so sure you'd *want* to do this. :)).

Rack middleware always us to process the request before sending it to the server, and process the
response before returning back to the client. Things like `Rack::Runtime` take
advantage of this.

If you're curious about what middleware your application is using you can run `rake middleware` in your application directory and see a list of all the middleware that is being used.

## Wrap Up
Now that you know a bit more about Rack (hopefully), you might be able to see
some of it's power. We can quickly create web applications using just Rack, when
things like Rails are too big for our needs. Knowing about Rack also gives us
the ability to be able to remove some of the magic surrounding Rails, and can
even allow us to debug easier and with more confidence.

I've [forked a version of Rack and added some useful puts statements](https://github.com/meaganewaller/rack) that helped
me to understand Rack and what role it was playing. It might be useful for you
as well to point your Rack gem at my repository so you can also get some logging
in your console to see what Rack is doing for you.

---
title: "Anonymous Functions in Clojure"
date: 2014-02-25
categories: ["Clojure", "Tutorials", "Code"]
---

In Clojure we can have functions without names, these unnamed functions are
called anonymous functions, or lambda functions.

Anonymous functions can be created in a couple
different ways. We can use `fn`, like so:

````
; the syntax
(fn [param-list]
  function body)

(map (fn [n] (str "hello " n)) [ "alice" "bob" "carl"])
; => ("hello alice", "hello bob", "hello carl")

((fn [x] (* x 3)) 5)
;=> 15
````

When we think of `fn`, we can think of it, and treat it the same way we do with
`defn`. The parameter lists and the body of the function work the same way.

We can also associate our anonymous function with a name like so:

````
(def say-hello (fn [n] (str "hello " n)))
(say-hello "alice")
; => ("hello alice")
````

However, I said above that we can create anonymous functions in a couple
different ways, we saw how to do it with `fn`, now let's see how to do it with
`#()`.

````
(#(str "hello " %) "alice")
;=> "hello alice"

(def square #(* % %))
(square 3)
;=> 9

(map #(str "Hello, " %)
     ["Alice", "Bob", "Carl"])
;=> ("Hello, Alice", "Hello, Bob", "Hello, Carl")
````

This one is a little bit harder to wrap our head around possibly, so I'll try
to explain.

It looks a lot like a function call, except it starts with a `#`.

````
; Function call
(* 3 3)

; Anonymous Function
#(* % 3)
````

When we glance at this we can see what will happen when we apply the anonymous
function. In our example above, it's going to multiply the argument by 3.

The percent sign `%` represents the argument that we pass to the function.

If we take multiple arguments we can differentiate them like so:

````
(#(str %1 " says hello to " % 2) "alice" "bob")
;=> "alice says hello to bob"
````

We can also pass a rest parameter like so:

````
(#(identity %&) 1 "something", :anything)
;=> (1 "something" :anything)
````

It's important to remember that although you *could* use anonymous functions for a
lot, it's not always the best choice. Anonymous functions can easily become
unreadable, so it's best to keep them limited to short functions, or short-term
use functions.



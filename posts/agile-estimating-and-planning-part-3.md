---
title: "Agile Estimating and Planning Part Three"
date: 2013-08-11
categories: ["Technology", "Book Reviews"]
---

In my first post of this blog series, we discussed what agile means and why it's important to practice agile planning, as well as how to estimate. In the second post we went over how to plan and how to schedule. Today we are going to be learning about Tracking and Communicating, this will include how to monitor the release plan, the iteration plan, and how to communicate regarding plans. This blog post will a bit shorter than the most, but we've already touched on the real key parts of what it means to practice agile methodologies, so this will just be wrapping it all up and getting it ready to put it into practice. Let's get started.

## Monitoring the Release Plan

Monitoring the release plan seems easy enough, how do we get from Point A to Point B? You might answer that we do this the same way we get to two points in math: draw a line. However, software is rarely (read: never) that easy. There are other factors that come into play, and there is no simple line that can be deduced as the correct path to take from start to finish. As we've discussed, we know that requirements can change, our progress rate can differ from what we expected, and we also have to account for just good old human error. But, there are ways to get from point A to point B; ways that won't make us curl up in the fetal position, or tear our hair out.

## Tracking the Release

Remember in Part 2 we talked about what a release plan was and how to schedule one? Well, a release plan might look something like:

> "In the next 6 months, with 24 weekly iterations we will complete 216 points."

Keeping in mind that your estimate is likely to change after learning about your users' true needs, the scope and size of the project. But at any point we want to be able to see where we are relative to the goal of completing a certain amount of work in a given amount of time. There are a couple of things that need to be considered: first is the amount of progress made by the team, second is any change in the scope of the project.

Let’s drive this point home with an example. These forces that you’re facing, forces such as: completed work, changing requirements, and estimate revisions, can be thought of as being similar to the forces at play when operating a boat. You have the forces of wind, and of the sea. Because of the wind, the distance we go in the boat will be less than what we would’ve estimated strictly from the speedometer. Also, we might’ve had our compass pointing due west for our entire trek, but the wind caused us to veer north. If we don’t adjust our course, the boat will take longer to get somewhere that we didn’t want to end up in the first place.

Software development is a lot like that boat. We also need to mentally re-adjust for our forces, forces such as: completed work, changing requirements, and revised estimates. Without adjusting we will also take longer to end up somewhere that we don’t want to be. Think of the forces, wind and the sea, on the boat, as requirement changes, and estimate changes, and when we are presented with those forces, we need to take a step back and recalculate and re-adjust so we’re not stuck on an island in the middle of nowhere.

##Velocity

Velocity is the primary measure of a team’s progress, so it’s important to establish some rules on how to calculate it. The most important rule is that a team only counts points toward velocity for stories/features that are 100% completed. 100% completed means exactly that, this doesn’t mean: “the code is done, but I still have to write tests”, or “it’s coded but it’s not integrated yet”. Complete code is clean, refactored, checked-in, and compiles with coding standards and passes all tests. at 8th Light we practice TDD, or Test Driven Development, not only does our code has to pass all tests, we shouldn’t even have any code written without a test, since we write all of our tests first and let that drive our code.

The biggest reasons that we do not count unfinished work is because it’s really hard to measure. How do you decide if this incomplete feature is more complete than that other incomplete feature? The second reason is because it breaks down the trust between the team and the customer. If a story can’t be completed as planned during the iteration, the developers and the customer need to collaborate and resolve the issue as soon as it’s brought up. This usually means that the story gets moved out of the iteration, or it gets split up into smaller stories and moved around. In my opinion, the most important reason that we don’t count unfinished stories at the end of an iteration, is because they lead to a really big backlog. The more work that you throw on top of the backlog, the longer it takes for new features to be transformed from ideas into functioning and complete software. This will lead to stress for the team, frustration for your client, and not only that but the team is now missing out on the learning that would be happening if they were working on new features.

Why do we sometimes have unfinished stories at the end of an iteration? The team might be working with stories that are too large. If this is the reason for your unfinished stories, now is the time to break that large story into smaller, more manageable chunks. This should ideally happen before the iteration starts but sometimes a story can be deceivingly small only to find out it’s way more complex or complicated than was originally thought. If you find that you’re working on a story during an iteration that is larger than expected, bring it to the attention of the product owner, they should collaborate with the team and find a way to split the story or reduce its scope.

Unfinished stories aren’t the end of the world, they happen to all of us at some point or another. But, it’s important to learn from your errors, and to figure out why you underestimated that story so you can prevent it from happening again. If your stories are unfinished because there is simply too much work plan your iterations more carefully in the future, and keep that in mind.

## Monitoring the Iteration Plan
It’s important to not only monitor the high-level release plan, but to also monitor and track the development team’s progress on single iterations. We will be looking at two tools for helping us do just that: the task board and iteration burnout charts.

## The Task Board
Use a task board, it can be a whiteboard, a cork-board, or just any designated space on a wall, to give the team a convenient mechanism for organizing their work and way of seeing at a glance how much work is left in the iteration. The task board allows the team a lot of flexibility in how they organize their work. Individuals on an agile team do not sign up for/get assigned work until they’re ready to work on it. So, except for the last day or two of an iteration, there typically are many tasks that no one has signed up for yet. A task board makes these tasks highly visible so that everyone can see what tasks are being worked on and which are available to sign up for.

## Iteration Burndown Charts
Drawing a release burndown allows users to see whether a project is going astray or not. Depending on the length of your iterations, it can be useful to draw one, if you’re doing only one week iterations it may not be useful to draw one of these because by the time a trend is visible on an iteration burndown chart the iteration will be over. To create an iteration burndown chart simply sum all of the hours on your task board once per day and plot that on the chart.

## Tracking Effort Expended
On a project it’s more useful to know how much remains to be done versus how much is done. For example, we might have finished eight hours of work, but our eight hour estimate of how much we would get done compared to what was actually done may differ. Measuring the amount finished are not helpful if we’re not sure all the progress was in the right direction. Tracking the effort expended and comparing it with estimated effort can lead to “evaluation apprehension” -- when estimators are apprehensive about providing an estimate, the familiar “flight or fight” instinct kicks in, and estimators may rely more on instinct than on analytical thought. Tracking effort expended in an effort to improve estimate accuracy is a very fine line. It can work, however, the person doing the tracking must be very careful to avoid putting significant evaluation pressure on the estimators, as doing so could result in estimates that are worse rather than better. It’s also important to keep in mind that no matter how much effort you put into improving your estimates a team will never be able to estimate perfectly.

## Individual Velocity
Individual velocity refers to the number of story points by an individual team member. Do not track individual velocity, doing so leads to behavior that works against the success of the project. If an individual knows that their individual velocity will be measured and tracked from iteration to iteration how might they respond? Well, they might be forced to chose between finishing a story on their own or helping someone else, there is no incentive to measuring it. They should be given incentives to work as a team, if the team’s velocity is helped by an individual helping another team member they are going to do so. Further, individual velocity shouldn’t even be measurable, stories shouldn’t be written for just one individual to work on. They should be written for multiple people on a team, UX designers, programmers. database engineer, tester, etc. If most of your stories can be done by a single person consider rewriting your stories at a higher level so that work from multiple individuals is included with each.

## Communicating about Plans
Next we’re going to look at some specific ways to communicate about plans, and more importantly how we approach the work of communicating about estimates and plans. We want all communication to be frequent, honest, and two-way.

Frequent communication about plans is important because of how often an agile plan should be updated.

Honest communication is important if the development team and customer team are to trust each other. Without trust there is no honest communication, which makes it harder to work together and to have the type of open and honest relationship that a development team needs with their client.

It’s also important to make the communication two-way because we want to encourage dialogue and discussion about the plan so that we make sure we always have the best possible plan for delivering value to the organization. We want to iterate and refine plans based on feedback and knowledge gained.

## Communicating the Plan
When possible, include with your communication of a target date either your degree of confidence in the estimate, a range of possible dates, or both. For example:

I am X% sure we’ll complete the planned functionality by MM/DD
Based on assumptions about the project size and team performance, the project will take X to Y months.
Communicating Progress

The simple way to predict the number of iterations remaining is to take the number of points remaining to be developed, divide this by the team’s velocity, and then round up to the next whole number. Remember though, that measures of velocity are imprecise, and we expect velocity to fluctuate. Burndown charts can come in handy in this regard. It’s also useful to think of velocity as a range of values rather than one value. Use the velocity of the most recent iteration, the average of the previous eight iterations, and the average of the lowest three of the previous eight iterations. These three values present a good picture of what just happened, a long-term average, and a worst case of what could happen. On some projects, an end-of-iteration summary can be useful for disseminating current information and as a historical document for use in the future.

##Why Does Agile Planning Work?

The purpose of agile planning is to iteratively discover an optimal solution for the product development questions of “which resources with which resources in what timeline”.

Replanning occurs frequently: We know a plan can be revised at the start of the next iteration, so we don’t focus on creating an (impossible) “perfect plan”, and instead create a plan that is useful right now.
We estimate size first, and derive our duration from it
We make plans at different levels: the release, the iteration, and the current day.
Plans are based on features not tasks
We have small stories that keep the work flowing
We eliminate the work in process every iteration, this means teams are more easily able to work efficiently in short iterations.
We track at the team level, more incentive to help each other and work together.
We acknowledge and plan for uncertainty


##Guidelines for Agile Estimating and Planning
> Involve the entire team
> Plan at different levels
> Keep estimates of size and duration separate by using different units
> Express uncertainty in either the functionality or the date
> Replan often
> Track and communicate progress
>  Acknowledge the importance of learning
>  Plan features of the right size
>  Prioritize features
>  Base estimates and plans on facts
>  Leave some slack
>  Coordinate teams through lookahead planning.
>  Wrap Up

This blog post will conclude my Agile Estimating and Planning series. I've had a fantastic time reading this book, and have walked away from it feeling more confident about myself and my ability to use agile methodologies everyday at work. I hope my blog series has helped you to have that same experience, and if you'd like to read this fantastic book, you can buy it here. I obviously really recommend this book, and I'm sure that I will be reading it again in the future.

---
title: "The Problem with Datamapper"
date: 2014-01-26
categories: ["Apprenticeship", "Code", "Ruby"]
---

*This is going to mostly a rant posing as a blog post, this is your warning*


Taryn and I are using DataMapper with our Sinatra application and have found out that there some inherent problems with it.  If you don't know about DataMapper, it's a Ruby ORM library. The reason there are inherent problems with it is because it values booleans over exceptions. Things shouldn't fail silently. I shouldn't need to ask why a record failed to save, but I do. And unless you don't care about knowing why your data doesn't make it into the database, you will have to ask why.

````
applicant = Applicant.new(:name => "Meagan")
if applicant.save
   # applicant is valid & saved
else
   applicant.errors.each do |error|
        puts error
    end
end
````

This is going to end up everywhere in your code if you happen to care about knowing why records aren't being saved in the database. This isn't me being ridiculous, [this is literally in the DataMapper documentation](http://datamapper.org/docs/validations).

Taryn and I were spinning our wheels trying to figure out why something wasn't being saved. The reason we didn't know why is because we didn't ask what the errors were, because apparently that's something you have to do.

DataMapper and I aren't really friends. We don't really get along, and it's because code that fails shouldn't fail silently. It should be noisy, it should tell me why it's failing. I shouldn't have to do extra work to find out when something goes wrong, it should tell me. If DataMapper did something like, I don't know, throw an exception when something failed to validate it would decrease so much work, debugging, and unnecessary code (well not really unnecessary since you need it to know why code isn't saving, but you get my point). This would make it so I could handle the creation of data and deal with the errors in one place instead of having to check literally everything for failures. I could write code without wondering if I forgot to explicitly ask if there were errors on an object.

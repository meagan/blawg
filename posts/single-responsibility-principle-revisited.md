---
title: "Single Responsibility Principle Revisited"
date: 2013-12-02
categories: ["Java", "Code", "Tutorials", "SOLID Principles"]
---


I wrote about Single Responsibility Principle a few posts back, [here](http://blog.meaganwaller.com/single-responsibility-principle). However, now that I'm writing Java, the principles seem to make more sense, especially since most of the blog posts and writing about it have examples in Java. Being able to read Java and write it (a bit) have helped me to further my understanding of it. So, I'd like to revisit SRP in a bit greater depth and with some better code examples.

## What is Single Responsibility Principle?
Simply put, Single Responsibility Principle, or SRP, is the principle in object-orientated programming that
> "Every class should have a single responsibility, and that responsibility should be entirely encapsulated by the class. All its services should be narrowly aligned with that responsibility."

Robert C. Martin, aka Uncle Bob, introduced this term in his article called *Principles of Object Oriented Design*. Uncle Bob defines responsibility as a *reason to change*, and says that a class or module should have only one reason to change.

Utilizing this principle ensures that if a change needs to be implemented in the future in our code base we won't have to worry about the amount of dependency we have in our class. Now if we're asked to make a change to a certain functionality we can be sure it won't impact the other functionalities in the class, because there won't be more than one responsibility. If we find out that our class has more than one responsibility we should split the functionality into separate classes.

## SRP In Action
For our example, we've been asked to implement a new feature into our application. The feature is for a bank site, and we want to allow users to be able to check the balance on their account, however we need to check to make sure the user is authorized to do so.

```
public class UserLedger {
  public void checkBalance(User user) {
     if(userHasAccess(user)  {
          // Allow user to check balance
       }
}
  public boolean userHasAccess(User user) {
        // Check to make sure the user is valid
  }
}
````

This makes sense right? If `userHasAccess` is true the user is allowed to check their balance. However, if we want to reuse `userHasAccess` in the future, or if we want to change what it does, or even change how users check their balance, we're going to run into some messiness and be digging into dependencies.

We can correct this before it's too late, though! If we make `UserLedger` split into two classes, one called `UserLedger` and one called `Security` (yes, I'm amazing at naming things), we can remove the added and unnecessary responsibility from `UserLedger`.

````
public class UserLedger {
    public void checkBalance(User user) {
        if(Security.userHasAccess(User user)) {
            // Allow user to check balance
         }
     }
}

public class Security {
    public static boolean userHasAccess(User user) {
         // Check to make sure the user is valid
    }
}
````

In our second example the `checkBalance` method is now calling `Security.userHasAccess(User user)`, and now any other method in the code base that needs to check if a user has access can do so by calling that as well.

## Wrap Up
When it comes to SRP, it's important to try and stick to it to save ourselves from unnecessary dependancies like we saw above. If we wouldn't have moved `userHasAccess` out of `UserLedger` then every time we wanted to check if a user had access we'd have to do it through `UserLedger`,  or create duplication in our code.

When we create a class that has more than one reason to change, or more than one responsibility we risk things going wrong, and things getting overly complicated. The sooner we extract things into their own classes, and make sure we remove duplication, and follow this principle we make our code easier to read and understand, as well as easier to debug and test.

I'll conclude with this quote from Uncle Bob's article,
> The SRP is one of the simplest of the principles, and one of the hardest to get right.

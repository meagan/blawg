---
title: "Under New Mentorship"
date: 2013-11-25
categories: ["Apprenticeship"]
---

As of the middle of November I've decided to move up to Chicago to finish my apprenticeship. As part of this new apprenticeship agreement, Mike Jansen is my new mentor. The timing isn't exactly the best, so we're doing the first month of the apprenticeship remotely. Most of my apprenticeship I've worked solo, we have a really small office, and most of my apprenticeship I've been the only developer apprentice at or around my level. I hadn't had much opportunity to pair, or work with any other apprentices.

In November I was up in Chicago for the third time, this time for SCNA. I went up early, and stayed longer than Justin or Will. During this time I got to work with the other apprentices. I had a particularly awesome time pairing with Kelly, another resident apprentice, on our Dojo Dashboard, a Clojure and ClojureScript project. The challenge was exciting to me, I felt like I learned a lot in Chicago. I started reflecting on this, it seemed like every time I went to Chicago I learned a lot.

When I first started the apprenticeship, my progress shoot up dramatically. I was so excited, taking in all these new concepts, making huge leaps and bounds. Then I started to plateau. I wasn't able to learn things as quickly, I felt like I was stalling. I saw my review board target date move further and further away. I sort of felt like a failure.

One day while I was in Chicago Mike pulled me aside and we had a talk about my apprenticeship, and the idea of me being moved to Chicago was brought up. I was really excited for it, every time I'd been in Chicago I had a breakthrough, my first visit I was finally able to solve my Negamax algorithm, y second visit, I was able to do some serious refactoring that had left me stumped, and the most recent time, I worked through my Java kata, and had a really successful couple days of pairing. Whenever I'd leave Chicago I felt excited to go and code again.

Then Monday would roll around. Being the only apprentice in the office is tough, it's not really how the apprenticeship is supposed to go. Yes, you will need to figure things out on your own, but the other apprentices act as mini-mentors somewhat, helping you find your way through paths they just treaded. I lack that guide in Tampa.

So, what this means is that I'm moving to Chicago on January 8th. I bought my one-way ticket, and am currently remotely roommate searching. I'm nervous, and excited, and kind of sad to be leaving the place I've lived for my entire life, but also optimistic that I'll be able to see that same growth I saw in the beginning of my apprenticeship through to the end of my apprenticeship.

Mike has promised to be a tough, but effective mentor. I will be doing things differently, and it's not going to be easy, but I will learn a lot, and I will be prepared for my review board when that time comes. I've found out that I'll need to stick to routine. I've adjusted some habits in my day-to-day life recently to accommodate this. I will be responsible for writing a blog post everyday, for example, in order to try and stay on top of this, I've added recurring alerts in my phone to go off to remind me to spend just an hour a day writing a blog post. I've also got to remember to start pushing to GitHub more frequently. I have a bad habit of working on a bunch of things at once, and not making small, frequent commits. I need to remedy this by remembering to work on small changes and to follow things like SRP in my commit messages to keep them small.

This change in my apprenticeship is difficult for me, I find myself falling back into old habits. I need to take this seriously and put in the time everyday to succeed. I know that once I'm in Chicago I'll be able to make those big strides, so it's really going to be important to get on a routine so the transition to Chicago is seamless.

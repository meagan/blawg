---
title: "Open Closed Principle"
date: 2013-10-28
categories: ["Ruby", "Tutorial", "Code", "SOLID Principles"]
---
The SOLID principles are very important principles when it comes to object-orientated programming and design. The principles, when applied correctly and together help to create systems that are easier to maintain and extend over time. The principles of SOLID are guidelines that can be applied while working on software to remove code smells by showing the programmer when and where to refactor code until it is both readable and extensible.

This post is going to be about the O in SOLID. The O stands for Open/Closed Principle, or OCP. OCP states that an object should be open for extension, but closed for modification.

This means that when you introduce a new behavior to an existing system, rather than modifying old objects you should create new objects which inherit from or delegate to the target you wish to extend. The reason for doing this is because it improves the stability of your application by preventing existing objects from changing frequently. This makes dependency a bit less fragile because there are less moving parts to worry about.

````
class Animal
  def breathe(animal)
    if animal == "cat"
      puts "inhale and exhale"
    elsif animal == "dog"
      puts "inhale and exhale"
    elsif animal == "cow"
      puts "inhale and exhale"
    end
  end

  def speak(animal)
    if animal == "cat"
      puts "meow"
    elsif animal == "dog"
      puts "bark"
    elsif animal == "cow"
      puts "moo"
    end
  end
end

cat = Animal.new
cat.breathe("cat") #=> "inhale and exhale"
cat.speak("cat") #=> "meow"


dog = Animal.new
dog.breathe("dog") #=> "inhale and exhale"
dog.speak("dog") #=> "bark"


cow = Animal.new
cow.breathe("cow") #=> "inhale and exhale"
cow.speak("cow") #=> "moo"
````

In this example, we have an Animal class, with a few methods on it, breathe, and speak. The breathe and speak method both take an animal argument and then we can find out how that animal breathes or what it says. This violates the open/closed principle because if we want to find out what a horse says, or how it breathes, we have to modify the Animal class.

Instead, we can allow different animals to inherit from a class, like a Mammal, class. See the example below:

````
class Mammal
  def breathe
    puts "inhale and exhale"
  end
end

class Cat < Mammal
  def speak
    puts "meow"
  end
end

class Dog < Mammal
  def speak
    puts "bark"
  end
end

class Cow < Mammal
  def speak
    puts "moo"
  end
end

class Horse < Mammale
  def speak
    puts "neigh"
  end
end

cat = Cat.new
cat.speak #=> "meow"
cat.breathe #=> "inhale and exhale"

dog = Dog.new
dog.speak #=> "bark"
dog.breathe #=> "inhale and exhale"

cow = Cow.new
cow.speak #=> "moo"
cow.breathe #=> "inhale and exhale"

horse = Horse.new
horse.speak #=> "neigh"
horse.breathe #=> "inhale and exhale"
````
Now, adding new animals is as easy as letting an Animal inherit from the Mammal class.

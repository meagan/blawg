---
title: "Making the Move"
date: 2014-01-08
categories: ["Apprenticeship"]
---

This week has been super hectic. On Wednesday I moved from Tampa, FL to Chicago to join the 8th Light team at the Chicago office. Thankfully I had no flight delays and everything went very smoothly.

This week I've been very focused on getting settled in and have adapted pretty well this far, except for one day when it was raining and freezing and there were ankle deep puddles of ice water at every crosswalk that I wasn't sure how to traverse.

Working from the Chicago office is completely different than working from the Tampa office. In the Tampa office I could work all day without interruptions, Chicago is a bit different. There is a lot more going on, especially on Friday when everyone is in the office. Interruptions aren't necessarily bad, just something that I'm going to have to get used to.

There are lots of awesome things about working in the Chicago office. I get to work alongside with the other apprentices, this means more people to ask questions, and for one of the first times, people asking me questions, and I actually have answers for them. I also get the opportunity to work with the craftsmen, and in Chicago there are way more tech meetup groups that I will be able to attend.

I'm really excited about the upcoming things I get to do as I approach my one year anniversary since I started to program ( I started in February 2013 ).
I've come along way so far and can't wait to see how far I'll come in the next year, two years, five years, and ten.



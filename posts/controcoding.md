---
title: "Controcoding"
date: 2014-01-22
categories: ["Apprenticeship"]
---

Yesterday, Rachel, our sales apprentice, put on a really great event: a panel discussion about Controversial Coding topics. There was a great turn out despite how cold it was outside. I did the video for the event, and wanted to share it.

<iframe src="//player.vimeo.com/video/84793305" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="http://vimeo.com/84793305">Panel Discussion: Exploring Controversial Coding Topics</a> from <a href="http://vimeo.com/eighthlight">8th Light</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

---
title: "Goals for Next Week"
date: 2014-01-26
categories: ["Apprenticeship"]
---

Next week I'd like to accomplish a few things.

1. Finish reading Eloquent Ruby. I'm really enjoying the book so far, and I'd like to finish it up and write a blog post about it.

2. Continue to work on Footprints and finish up some of those stories. We're knocking out our stories and I'm really proud of the work that we've accomplished thus far. I'd like to keep that drive and spend 2 days this week working with Taryn on Footprints.

3. Redo the Coin Changer kata with the refactorings in mind.

4. Continue to spend time with the novices downstairs and be sure to be open if they have questions.

5. Get sleep, last week I got a grand total of 6.5 hours combined during a 3 day period. The lack of sun in Chicago is really throwing me off and making my insomnia come out in full swing. I'd like to hone that in somehow, I think I'll be buying a SAD lamp which is supposed to help with the manufacturing of vitamin D, something that the sun provides and that I'm obviously lacking.

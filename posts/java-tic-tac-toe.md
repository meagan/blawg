---
title: "Java Tic Tac Toe"
date: 2013-12-11
categories: ["Java","Code" ]
---

I've started on my Java Tic Tac Toe this week. I have started over on this a few times because I kept trying to rewrite my Ruby version in Java. Rewriting Ruby code in Java is problematic because Ruby isn't Java, and so my approach should be different for each language.

I started out with my `Board` class. I like starting with the board because I know exactly what the board needs. It needs spaces, it needs the ability to set a move on the board, it can get the empty spaces on the board, it can undo a move, it has a size, it knows if the board is empty.

When it comes to the other classes, like `Game` or `Player` or even `UI` it's easy to start seeing things get coupled. Should the `Game` draw the board, or should the `UI`? Should the `Game` care about the `Player` or should it only call something like `make_move` on a `current_player` and not care if it's an `AI` or a `Human`.

Starting with the `Board`, for me, is a good way to get in the TDD groove. I know what to expect, probably because I've written Tic Tac Toe so many times, especially the Board class since I usually start with it. I also know that the `Board` doesn't have to be aware of certain things, like validations to check if it can set a move in a space, or checking for a winner. The board method `setMove` literally just sets a move, however maybe in the future in my `Game` class I might have something that will only actually call the `setMove` method if the user's move input is valid. My board, however, isn't interested in that, it's only interested in setting a move.

I'm still really enjoying Java, and doing Tic Tac Toe in Java is changing the way I approached the problem in the past. With Ruby you don't have to worry about types, since everything is duck-typed (if it quacks like a duck, and walks like a duck, it's a duck), however in Java everything has a type.

Here's the way I wrote my Ruby method that placed moves on the board:

````
   def place_move(piece, *indices)
      indices.each do |space|
        @spaces[(space.to_i)-1] = piece
      end
    end
````

This method takes the parameters `piece` and `indices` with a splat, so I could place multiple moves if I wanted to. Next it iterates over the `spaces`, and for each `space` at the designated index (after calling `to_i` on it, and subtracting 1 since arrays are zero-indexed), it sets that `space` equal to the `piece`. So I would call it like this `board.place_move("X", 3)` it would go to `@spaces[2]` and set it equal to `X` and now my `@spaces` would look like this: `__X______` if there were no other moves already on the board.

In Java my method to place moves is a bit different.

````
 public void setMove(char marker, int move) {
                StringBuilder newSpaces = new StringBuilder(spaces);
                newSpaces.setCharAt(move - 1, marker);
                spaces = newSpaces.toString();
        }
````

This method is expecting 2 parameters, the first is a `char` called `marker`, and the second an `int` called `move`. Next it's going to build a new `String` via `StringBuilder` named `newSpaces` initialized to the contents of `spaces`, a `String` I defined in my class already (`private String spaces = "";`) After that, it `setCharAt(move - 1, marker)` on `newSpaces`. Like Ruby, arrays in Java are zeroindexed. Afterwards, it sets `spaces` equal to `newSpaces.toString();`

So, in order to call this method, I have to say something like:
`board.setMove('X', 5)` in Java, `char` is represented with single quotes ('') and `Strings` with double quotes (""). If you try to pass in a `String` it will not work.
It will then create a new `String` called `newSpaces`, and on `newSpaces` it's going to `setCharAt(4, 'X');`. This will give us something like "----X----" as `spaces`.

Java and Ruby have both been really fun to learn, Ruby was easier to pick up and easier to get something working right away. Java challenges me, which I also enjoy. I am enjoying having to think about the types I am expecting and what types I'm passing in, as well as learning a lot about OO programming, and good OO habits, like the SOLID principles.

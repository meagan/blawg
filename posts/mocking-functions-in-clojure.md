---
title: "Mocking Functions in Clojure"
date: 2014-03-26
categories: ["Clojure", "Code", "Tutorial", "Testing"]
---

I'm relatively new to Clojure, I've built a command line tic-tac-toe in Clojure,
and the client project I joined last week is a Clojure project, so I've had
about 3 weeks of worth of exposure to Clojure so far. I [wrote a blog post about
getting started with
Clojure](http://meaganwaller.com/posts/getting-started-with-clojure) where I
detailed my vim plugins, my testing framework, and some general environment
setup. Going forward with this blog post, we'll assume that everything I
outlined in that blog post is still applicable to the examples going forward in
this one.

## What is Mocking?
Let's take a few steps back and consider what mocking means as well as when and
why we'd want to use a mock. In object-oriented programming, a mock object is a
simulated object that mimics the behavior of a real object in certain controlled
ways. We'd use a mock object in a unit test to simulate the behavior of a real
object because actually testing the real object is either impossible or
impractical. Some guidelines, according to [the Wikipedia
page](https://en.wikipedia.org/wiki/Mock_object) to know when you might want to
use a mock object are if your real object:

  - Supples non-deterministic results (like the current time)
  - Has states that are difficult to create or reproduce
  - Is slow (for example, a database)
  - Would have to include information and methods exclusively for testing
    purposes (and not for its actual behavior)

You might want to test a method that displays some output on the command line,
but you don't want output to clutter your tests. The solution might be to use a
mock output stream that captures the input somehow (possibly by pushing it
inside of an Array) so you can assert against that.

Now, maybe you caught that I referred to mock objects and said they were a part
of object-oriented programming. If you're not aware, Clojure is a functional
language, so we aren't going to create a mock object because the notion of
objects or classes as we think of them in object-oriented languages doesn't
transfer over to functional languages. We want to focus more on mocking out
functions during testing. That's where the handy `with-redefs` will come in.

## With Redefs

In Clojure core exists a function called `with-redefs`. The [actual
documentation](http://clojuredocs.org/clojure_core/clojure.core/with-redefs)
says that it's useful for mocking out functions during testing.

This function allows you to temporarily redefine Vars while executing the body.
I think of it as controlling what a function returns, this isn't actually what
it's doing, but it helped me wrap my head around what was going on.

If we wanted to check that a function can validate that input is an Integer we
can use `with-redefs` to do this.

````
(it "gets valid input"
  (with-redefs [prompt-for-input (fn [] "1")]
    (should= 1 (get-input)))
  (with-redefs [prompt-for-input (fn [] "abc")]
    (should-throw Error "Invalid Input"
    (get-input))))
````

In reality, our `get-input` function is expecting to prompt a user via
the command line to enter input using the `prompt-for-input` function, we don't want to run our tests and have to enter input though.

We're using `with-redefs` to mock-out `prompt-for-input` and telling it that it's getting `"1"`, or it's getting `"abc"` instead. So now when we assert against `get-input`, it's using our mocked out `prompt-for-input` instead of the real one that would expect to get user input from the command line. `get-input` is just using what we said `prompt-for-input` is returning so we can assert against that and actually test that our function can throw an `Error`, or that it can get valid input.

## Wishes: The Wishlist Application

Let's pretend we have an application for creating and curating
wishlists. When a wishlist is created it's associated with a generated token so
we can retrieve it by the token. We don't actually want to generate a new random
token everytime we run the tests, we want to mock out the function so we can
assert against it and eliminate randomness from the equation.

<script src="https://gist.github.com/meaganewaller/9796171.js"></script>

Alright, let's go through this.

 - Lines 1-4 are where I'm doing my namespace configuration type stuff. This
   file has the namespace `wishes.wishlist-spec`, it requires `wishes.wishlist`,
and the `speclj.core` (the testing framework I'm using)
 - Line 6 is where the testing starts, everything in this particular suite will
   be describing `"Wishlist"`.
 - Lines 7-9 are where I'm creating a `fake-wishlist`, for my example I'm
   assuming a wishlist has a `name` and `description` field. I'm just creating
this so I can use this `fake-wishlist` through the suite, you'll see in a bit.
 - Line 11 I have a comment about setting up your test database. This really
   depends on what you're using for your database.
 - Lines 13-17 are where the actual test is happening.
    - 14: This is the first instance of `with-redefs` that we're coming across.
      What I'm doing here is mocking out my `generate-token` function and
setting it to return `"footoken"` instead. I'm controlling the behavior of
`generate-token` in a way that makes it easy for me to assert on it, eliminating
randomness.
    - 15: I'm creating a `let`, saving the `fake-wishlist` I created in lines
      7-9 and binding it to `wishlist` so I can refer to `wishlist` later on.
    - 16: I'm binding `wishlist-key` to the `:key` of `wishlist` (bound in the
      line above)
   - 17: My assertion, I am finding the wishlist I just created, assuming I
     have a function in place to do this (`find-by-key`), and getting the `:token`
from it. This should equal `"footoken"`, the value that I set `generate-token`
to return for this test.

## Wrap Up & Further Reading
Having the ability to create mocks is important when it comes to unit testing
our programs. We need them to control behavior that otherwise would be
impossible to test. Object-Oriented languages, like Ruby or Java, allow us to
create entire mock objects and inject those while we're testing. Clojure, being
a functional language, approaches this a bit differently, it allows us to
temporarily redefine Vars so we can essentially control what a function returns
so we can assert against things that would be impossible or impractical to
assert against.

  - [Mike Ebert's blog post on mocking input in Clojure](http://mikeebert.tumblr.com/post/32243344470/mocking-input-in-clojure-thanks-colin) - includes some thoughts and practices for how to better mock input in Clojure.

  - [Joseph Wilk's blog post on Isolating External Dependencies](http://blog.josephwilk.net/clojure/isolating-external-dependencies-in-clojure.html) -  while not all about `with-redefs`, it does mention some of the dangers of what it means to redefine root `Var`.

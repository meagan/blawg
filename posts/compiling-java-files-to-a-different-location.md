---
title: "Compiling Java Files to a Different Location"
date: 2014-01-02
categories: ["Java", "Tutorials"]
---

I kept re-Googling how to compile Java files to a different location. I  was really sick of compiling my Java files only to have the directory they were stored in be accompanied by the `.class` files as well. It's really messy looking and I knew there had to be a better way to do it.

I've said in a [past blog post](http://blog.meaganwaller.com/using-every-opportunity-to-learn) that although using Google isn't a bad way to supplement my knowledge, I shouldn't be searching the same things over and over again, it's duplication. I decided that the [DRY principle](http://en.wikipedia.org/wiki/Don't_repeat_yourself) isn't solely applicable to software, but should be applied to my life as well.

If I searched for the same thing multiple times, (three is a good number to remove duplication at), I should refactor. When it comes to refactoring outside of the software definition, to me, it means learning something so I can avoid the Google step altogether.

## How To Compile to Different Directory
Back to Java now. By default the `javac` command used to compile Java files produces `.class` files in the same directory where the source code files are located. However, you can change the location of the generated `.class` files by using the `-d` switch.

If I have directory structure like so:

````
- bin
- src
 |_com
     |_ttt
         |_ AI.java
            Board.java
            BoardRules.java
            CommandLine.java
            ComputerPlayer.java
            Game.java
            GameRunner.java
            HumanPlayer.java
            Player.java
            UnbeatableAI.java
            UserInterface.java
            Validations.java
- test
  |_com
      |_ttt
          |_ BoardRulesTest.java
             BoardTest.java
             CommandLine.java
             ComputerPlayerTest.java
             GameTest.java
             HumanPlayerTest.java
             UnbeatableAITest.java
             ValidationsTest.java
- manifest.txt
- README.md
- Play.jar

````

Running `$ javac src/com/ttt/*.java` will indeed produce my `.class` files but they will be produced alongside my `.java` files in my `src/com/ttt` directory. This is something I want to avoid, because as I said earlier, it's messy and unnecessary. I want my `.class` files to be generated in my `bin` directory. I can do so like this:

````
$ javac -d bin src/com/ttt/*.java
````

After compiling, we can see that all my `.class` files have been created in the proper bin directory.

[![Screen Shot 2014-01-02 at 2.30.29 PM.png](https://d23f6h5jpj26xu.cloudfront.net/dzzuyi5bnu4jca_small.png)](http://img.svbtle.com/dzzuyi5bnu4jca.png)


## Running the Jar File

In the [old README of my java tic tac toe repo](https://github.com/meaganewaller/java-ttt/blob/5db1adc01e4e6450cfb35ba22be2fa6b7fec42fa/README.md) I had these instructions

1. clone the repo
2. cd into the directory
3. compile the java files: `javac src/com/ttt*.java`
4. cd to the src directory
5. jar the files: `jar cfm ../Play.jar ../manifest.txt com/ttt/*.class`
6. cd to the main directory
7. play the game: `java -jar Play.jar`

Since I'll be compiling the `.class` files into a different directory, I need to account for that. The [new README](https://github.com/meaganewaller/java-ttt/blob/9aa52a847c1b964c4fa21cac43689a15b846cc71/README.md) looks like this:

1. clone the repo
2. cd into the directory
3. play the game: `java -jar Play.jar`

Because I'm now compiling all my `.class` files to a bin directory, I can push those to my GitHub repo so the user doesn't need to compile and jar the files themselves.

I didn't add the `.class` files before because they looked very messy in the directory with the `.java` files. Since the `.class` files are in a separate bin directory, the user just needs to clone the repo, cd into it, and play the game since it's already been jarred for them. This approach is much more user friendly, much neater, and more easier.


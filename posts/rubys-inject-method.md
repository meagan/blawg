---
title: "Ruby's Inject Method"
date: 2013-05-30
categories: ["Ruby", "Code", "Tutorials"]
---

I spent some time today at work going over the Ruby Koans. I'm currently at exercise 170 out of 280. I'm having a really great time, and actually learning a lot. The first time I attempted to do the koans I didn't really understand what was going on. I also did them in the browser, and not on the command line. Doing them on the command line has actually made them a lot simpler to understand.

I got to the 'about iteration' koans. The method name the koans uses to show you the inject method is


````
def test_inject_will_blow_your_mind
````

I'd never really seen the inject method (also known as the reduce method) before, so I plugged it into pry to see if I could understand what was going on.

The koan looked like this:


````
def test_inject_will_blow_your_mind
    result = [2, 3, 4].inject(0) { |sum, item| sum + item}
    assert_equal __, result #=> 9

    result2 = [2, 3, 4].inject(1) { |sum, item| sum * item }
    assert_equal __, result2 #=> 24

    # Extra Credit:
    # Describe in your own words what inject does
end
````

The commented answers (9, 24) were the answers pry gave me when I tested it out. I wasn't quite sure why those were the answers so I tried out different numbers in the array, passed in a different number for the inject method, and eventually started seeing a pattern.

Let's try and break down the first part of the koan, where it's getting a sum of the numbers.


````
result = [2, 3, 4].inject(0) { | sum, item| sum + item }
#=> 9
````

## Using Inject to Sum Numbers

For starters, we can see that the inject method takes a parameter and a block. The block is executed once for each item contained in the Array. In this case, we have three items in our Array, so the block is executed three times. Our block takes two parameters in this example, sum and item. The parameter passed to inject (the number 0 in this example) will be the initial value of sum, and item will be the first item of the Array that we're calling inject on.

For the sake of this example: The block is executing three times. The first time our block executes sum is going to equal the number we passed into inject which is 0, and our item parameter will have a value of 2 (the first item in our Array).

Next we have the code within the block, sum + item. In our case, this will be 0 + 2 which will return the number 2. The return number is very important with the inject method. Whatever the return value of the block is now becomes the parameter that is passed into our sum parameter. Also, don't forget that what we pass into the item parameter changes with each run as well. The next time time the block is executed sum will be 2 and item will be the next item in the Array: 3.

Take a look at the example below:


````
result = [2, 3, 4].inject(0) { |sum, item| sum + item}
#=> the first run: sum = 0 , item = 2 (0 + 2) = 2
#=> the second run: sum = 2, item = 3 (2 + 3) = 5
#=> the third run: sum = 5, item = 4 (5 + 4) = 9
````

The return from the last execution of the block is the return value of the inject method, which does in fact equal 9.

However, passing in a parameter to inject is optional. If you don't set a default value to be passed in as a parameter the first time the block runs the first parameter (sum in our example) will be set to the first item in the object (in our case, 2, from the Array [2,3,4]), and the second argument (item from our example), is set to the second item of the object (3 in our example). Notice how the block only needed to run twice instead of three times.


````
[2, 3, 4].inject { |sum, item| sum + item }
#=> first run: sum = 2, item = 3 (2 + 3) = 5
#=> second run: sum = 5, item = 4 (5 + 4) = 9

#=> 9
````

However, the example above is very basic and contrived, and probably not something that you'd actually use. Let's look at some of the other things you can do with inject.

We can use inject to convert an Array into a Hash.


````
our_array = [['carrots', 'Vegetables'], ['strawberries', 'Fruit'], ['chicken', 'Meat']]
our_hash = our_array.inject({}) do |hash, item|
    hash[item[0]] = item[1]
    hash
end
#=> {"carrots" => "Vegetables", "strawberries"=>"Fruit", "chicken"=>"Meat"}
````

Let's now turn our Hash back into its original Array.


````
our_hash = {"carrots" => "Vegetables", "strawberries"=>"Fruit", "chicken"=>"Meat"}
our_array = our_hash.inject([]) do |array, item|
    array << [item[1], item[0]]
end
#=> [["Vegetables", "carrots"], ["Fruit", "strawberries"], ["Meat", "chicken"]]
````

## Wrap Up

The real power of inject is its ability to build new objects while looking at every element in the object individually. It can get values from any Enumerable object. Use inject when you need to build something, you can use it for filtering, summing and grouping.

---
title: "Relearning TDD"
date: 2013-11-18
categories: ["Apprenticeship", "Testing"]
---


This weeks iteration has been nothing short of challenging. Learning how to do a problem in a new language is tough, learning how to TDD a new problem in a new language seems impossible. This week, when doing the Mastermind game in Java, I tried my hardest to TDD. However, I feel like since I'm still learning the language, it's hard for me to know how to test it. I'm not only learning the language, but learning the Junit framework as well.

When approaching testing in Ruby, I'm fairly confident with Rspec. Junit, however is a different story. I'm having a difficult time testing because I'm not quite sure how to debug, and in some cases I'm not even sure what I'm supposed to be returning, and how to compare it for an assertion. Also, when testing I found myself writing larger chunks of code to get simpler things to pass. I'm still having a hard time thinking in small pieces in Java, the atomic elements of the language I have are still kind of big since I'm not sure how every single part works. I remember dealing with this specific problem in Ruby when I first started learning it, I'm confident with more writing, and more katas I'll be over this hump soon.

I do have a working implementation of my Mastermind game, with one small flaw, the randomness isn't quite so random. Since I'm having a hard time testing, I'm sort of brute forcing this, trying to figure out how to make this more random. I will work on that tomorrow morning before my IPM. I'm sure it's something simple, but I've been messing with it for the past 30-45 minutes with no avail, so I think I should be able to figure it out tomorrow with fresh eyes.

This exercise, while super challenging has been really rewarding. I feel like I've learned a lot about Java and a lot about learning new languages in just this short amount of time. I am starting to miss Ruby a little bit though, it definitely makes me happier when I'm writing Ruby versus writing Java.

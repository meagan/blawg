---
title: "Negamax"
date: 2013-05-08
categories: ["Ruby", "Apprenticeship", "Algorithms"]
---

In my [previous blog post](http://meaganwaller.com/blog/2013/05/01/MiniMax.html) I talked about the algorithm Minimax. I discussed how you can use Minimax as an algorithm for a Tic Tac Toe game, like what I'm working on right now. We learned that minimax is used for *mini*mizing the possible loss for a worst case (*max*imum loss) scenario. I explained briefly about **Game Trees** and how to **Rank Game States**.

However, upon talking to some people in the office, as well as, researching a bit online, I think I am going to try to implement **negamax** as my algorithm for my Tic Tac Toe game.

## What is Negamax?

Negamax is a variant form of minimax, however, negamax relies on the fact that max(a,b) = -min(-a,-b). This simplifies the minimax algorithm quite a bit. What that means is the value of a position to player A in the game is the negation of the value to player B. So, whichever player is on move looks for a move that **maximizes** the negation of the value of the position resulting from their move, which means that the next position must have been valued by the opponent. This works regardless if player A or player B is on move, this means that a single procedure can be used to value both positions. Where as in minimax it is required that player A selects the move with the maximum-valued successor while player B selects the move with the minimum-valued successor, so negamax is a simplification. The minimal requirements for running negamax are the node(board) and color(player marker).

## Node (Board)

As we just learned, the minimal requirements for negamax are the node(board) and color(opponent).

{% highlight ruby %}
  board.available_spaces.each do |space|
    board.place_mark(mark, space)
    score = -negamax(board, opponent, depth + 1)
    board.undo_move(space)
  end
{% endhighlight %}

The method above uses recursion, which I talked about in a [previous post](http://meaganwaller.com/blog/2013/05/06/recursion.html). In this method we are iterating through the <code>available_spaces</code> in the board, placing a mark in each <code>space</code> that was available and finding the score, however, we don't want to alter the board, so we undo it at the end with <code>undo_move(space)</code>.

## Player's Marker

Both players have a their own marks, which means we need to make sure both players are assigned a different mark. In order to make sure of this, we have the opponent method. This method reads like this: If marker equals "X" it returns "O", else return "X", this will also act as our "turn switcher" when we call

<code>-negamax(board, opponent, depth + 1)</code>

because now we are calling -opponent, or the opposite marker. You'll see it all come together down at the bottom.

{% highlight ruby %}
    marker == 'X' ? 'O' : 'X'
{% endhighlight %}

## Base Condition
Maybe now my recursion post from the other day is making sense. In order to better understand any of these algorithms, I needed to better understand recursion. If you don't know recursion, this next step will be tricky. Sure, you could just implement it, but isn't it really awesome to actually *understand* how it works? In this case, the base condition is:

  - Return -1 if the result is a loss for the computer
  - Return 0 if the result is a tie
  - Return 1 if the result is a win for the computer

{% highlight ruby %}
     if board.winner == mark
    return 1
  elsif board.winner == opponent(mark)
    return -1
  else
    return 0
  end
{% endhighlight %}

So, the method <code>game_result</code> will return 0, -1, or 1 depending on the way the board looks.


## Putting it All Together

My next step is to put this all together. I know that the first method needs to be recursive and search through every possible move. (<code>board.available_spaces.each { |space| }</code>) that I used above does this. Inside of that iteration I need to make a move and then call the recursive method. The recursive method will act as if the move made previously is filled and will continue adding moves to every available space until the base condition is true (winner or tie). After a space has gone through the entire thing the board isn't altered because we called the <code>undo_move</code> method on it at the end. After the move is undone the whole thing starts again with the next available space, this will continue until there is a winner or the game is tied.

My code will probably look *something* like this:
{% highlight ruby %}

def negamax(board, mark, depth)
  if board.is_board_full?
    return winner(board, mark)
  else
    best_score = -9999
    opponent = opponent(mark)

    board.available_moves.each do |space|
      board.place_mark(mark, space)
      score = -negamax(board, opponent, depth + 1)
      if score > best_score
        best_score = score
        best_move = space if depth == 1
      end
    end
    return best_score
  end
end
{% endhighlight %}

This is not the full implementation of the AI, this is simply my work in progress for the negamax. I might not even be getting this correct. I sort of meshed together the few articles on negamax I could find and came up with this. If you notice anything I could be doing *better*, let me know on [twitter](http://www.twitter.com/meaganewaller).




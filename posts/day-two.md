---
title: "Day Two"
date: 2013-04-09
categories: ["Apprenticeship"]
---

I left the office yesterday a little before 5pm not really taking into account that everyone would be on the road, my 24 mile commute
took over an hour yesterday, but my mom was happy to let me talk her ear off on the phone about how excited I am to be a part of this company.
I didn't realize just how tired I was until I was struggling to keep my eyes open while watching Buffy the Vampire Slayer's season 1 finale when I finally got home. I intended to sleep for only 20 minutes
so naturally I woke up a little over an hour later. Yesterday night I spent some time looking into Test Driven Development, but after my nap the night was mostly shot so I watched an episode of the Simpsons with
my roommates and went to bed. I'm still not quite used to going to bed before midnight and waking up around 6am but today I didn't even snooze my alarm, so that's something.

I arrived this morning around 8am and got to work right away. I finished faxing the rest of my paperwork to the Libertyville office, and cracked open the RSpec book and started reading.
When Cory arrived he informed me that my blog wasn't formatted, I admit, I didn't do any type of cross-browser testing on this blog, which obviously isn't a good idea, but after starting from scratch on this blog
literally more than 10 times (that includes totally deleting the GitHub repo this blog is stored in multiple times), I just was done, and never went back to see if my code was broken on other browsers. Well, my code was broken on Firefox specifically.
It was just a simple error in the way that I called my stylesheet, apparently including type=all in the stylesheet href does not work in Firefox. However, Cory helped me to fix the error and now my blog should look the same in Safari, Firefox, and Chrome. I'm not sure about
Internet Explorer since I'm running OSX.

Today was a quiet day in the office, I got sushi for lunch with Alejandra, Justin and Jamie, and it was pretty delicious. It's so nice being able to go out and get food at other places besides
what's available in the food court (I do not miss working retail in a mall whatsoever). I really want a good foundation of Ruby instead of just figuring out how to translate my Tic Tac Toe into Ruby so I've been reading this book that Cory suggested: (Why's (Poignant) Guide to Ruby)["http://mislav.uniqpath.com/poignant-guide/book/"]
and it's honestly the most fun I've had reading a programming book while also feeling like I'm getting a really good understanding of the language. I decided to borrow a few tricks from high school and have actually been taking notes on the book
in an outline format. I am surprised how much it is actually helping me to retain knowledge. I'm even taking notes on things that seem to be a given just because it never hurts to write it out in your own words, at least for me. I find that when I can actually write something in my own words
as well as being able to simply explain it to someone that I have a firm knowledge of whatever I'm explaining. Even Einstein said: "If you can't explain it simply, you don't understand it well enough", I've always loved that quote since the first time I heard it in my AP Psychology class in high school. I'm sensing a theme here, I keep pulling things from my brain that I learned in high school and applying them to my life today. Looks like my high school teachers were right after all, now I'm just waiting for someone to ask me how to graph a logarithm.

My plan for the rest of the week is to keep pushing through this book so I have a firm understanding of the Ruby language. The more I think about my assignment, the more I want to rebuild my Tic Tac Toe from the ground up. I think with a better understanding of the language I'm writing in I could write a much cleaner and efficient code. When I wrote my first Tic Tac Toe in Python I had no Python knowledge whatsoever. I went from zero to learning just enough to do what I needed to do to make the code run.
Like I said yesterday, Test Driven Development is going to be my biggest hurdle this week, but I will try my hardest to get through it, I'll see how well I can balance learning not only a new language, but also RSpec and TDD.

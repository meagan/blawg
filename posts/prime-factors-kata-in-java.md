---
title: "Prime Factors Kata in Java"
date: 2013-11-06
categories: ["Java", "Code", "Tutorials"]
---

<p>I flew into Chicago Tuesday night, and am spending the next week in the Chicago office. Yesterday, I met with <a href="http://www.8thlight.com/our-team/mike-jansen">Mike Jansen</a> and we discussed some of the bad and good paths I had taken with my Java server. I was feeling very lost and overwhelmed with it, and a lot of that had to do with not having a strong Java foundation. Mike has asked me to practice the prime factors kata in Java, and then present it, with no mistakes or typing errors at my IPM next Tuesday for him and my mentor in Tampa, <a href="http://www.8thlight.com/our-team/will-warner">Will</a>.</p>

<p>I've never done the prime factors kata in any language, so my first step was to look up what the prime factors kata was. The prime factors kata is fairly simple, the idea is that you'll have a method that will break down any number into a list of its prime factors, so it'll return a list of one or more prime numbers that when multiplied together give you the initial number you passed in.</p>

<p>When doing a kata, a big part of it is practicing TDD (Test Driven Development), this means writing a test before you write any code, and following the "Red/Green/Refactor" cycle. With Java, the unit testing framework is jUnit. While working with my Java server I felt unsure about my jUnit tests. I wasn't quite sure if I was testing things correctly. However, after working through the kata twice yesterday, I'm feeling better about jUnit, especially jUnit with IntelliJ; the IDE I am using for writing Java. I use vim as my only editor, so making the jump to an IDE was difficult for me at first, my code was littered with :w and :q and dd and every vim shortcut I'm used to using.</p>

<p>Following true TDD fashion, I set up my first test with the simplest case, in this instance, I wanted to test that passing in 1 would generate an empty list.</p>

````
package com.kata.junit;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorsTest {
  private PrimeFactors primeFactors;

  @Before
  pubic void setUp() throws Exception {
    primeFactors = new PrimeFactors();
  }

  @Test
  public void oneGeneratesEmptyList() throws Exception {
    List actualResult = primeFactors.generate(1)
    ArrayList expectedResult = new ArrayList<Integer>();
    assertEquals(actualResult, expectedResult);
  }
````

<p>In order to get this to pass, following TDD, I need to write the simplest code that passes the test:</p>

````
package com.kata.junit;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorsTest {
  private PrimeFactors primeFactors;

  @Before
  pubic void setUp() throws Exception {
    primeFactors = new PrimeFactors();
  }

  @Test
  public void oneGeneratesEmptyList() throws Exception {
    List actualResult = primeFactors.generate(1)
    ArrayList expectedResult = new ArrayList<Integer>();
    assertEquals(actualResult, expectedResult);
  }
````

<p>I follow along this line of thinking, writing the simplest test and making it pass with the simplest code. My next test cases were:</p>

````
<ul>
  <li>2 generates <code>[2]</code></li>
  <li>3 generates <code>[3]</code></li>
</ul>
````

<p>Those passed by implementing this code:</p>

````
package com.kata.junit;
import java.util.*;

public class PrimeFactors {
    public static List<Integer> generate(int number) {
        ArrayList<Integer> primes = new ArrayList<Integer>();
        if(number == 1) return primes;
        primes.add(number);
        return primes;
    }
}
````

<p>What this code is doing is creating an <code>ArrayList</code> named primes. If the <code>number</code> that is passed into <code>generate</code> is 1 it will just return the new, empty <code>ArrayList</code> named <code>primes</code>. If <code>number</code> isn't 1 it'll add  <code>number</code> to the <code>ArrayList</code> (adds 2 for <code>number = 2</code>, 3 for <code>number = 3</code>), and then it will return the <code>ArrayList</code> at the end. Java, unlike Ruby, doesn't have implicit return values.</p>

<p>The next test case is when things started to get a little more interesting.  </p>

<ul>
  <li>4 generates <code>[2,2]</code></li>
</ul>

<p>The way my code is currently written, <code>number = 4</code> would generate <code>[4]</code>. The way that I made this pass is by doing this:</p>

````
package com.kata.junit;
import java.util.*;

public class PrimeFactors{
    public static List<Integer> generate(int number) {
        ArrayList<Integer> primes = new ArrayList<Integer>();
        if(number == 1) return primes;
        if(number % 2 == 0) {
            primes.add(2);
            number = number/2;
        }
        if(number != 1) {
            primes.add(number);
        }
        return primes;
    }
}
````



<p>Now the code is looking a bit more interesting. What this is doing differently than before is:</p>

<ol>
  <li>Checking if <code>number</code> is divisible by 2</li>
  <li>If so, it's adding 2 to the <code>primes ArrayList</code></li>
  <li>Next, we are setting <code>number</code> equal to <code>number</code> divided by 2</li>
  <li>If <code>number</code> doesn't equal 1, we are adding <code>number</code> to the <code>primes ArrayList</code></li>
  <li>Returning <code>primes</code></li>
</ol>

<p>For example, when we pass in 4 to <code>generate</code> it goes through these steps:</p>
<ol>
  <li>Create new <code>ArrayList</code> named <code>primes</code></li>
  <li>Does 4 equal 1? <i>(No, moving on to the next step)</i></li>
  <li>Is 4 divisible by 2? <i>(Yes, now we enter the <code>if statement</code>)</i></li>
  <li>Add 2 to the <code>primes</code>, so now <code>primes</code> looks like this: <code>[2]</code></li>
  <li>Now, <code>number</code>, which originally was 4, will be divided by 2, and that is <code>number</code> now, so 4/2 is 2, so now <code>number</code> is 2</li>
  <li>Since 2 does not equal 1, we move into this next <code>if statement</code> on <code>line 12</code></li>
  <li>We will add the second 2 to our <code>primes ArrayList</code>, so now it looks like <code>[2,2]</code></li>
  <li>We return the <code>primes ArrayList</code> = <code>[2,2]</code></li>
</ol>

<p>My next couple of tests, such as 5 generating to <code>[5]</code> passed because 5 is a prime and isn't divisible by 2, so it didn't hit our first <code>if</code> statement since 5 doesn't equal 1. 5 isn't divisible by 2 so it didn't enter that <code>if</code> statement, and since 5 isn't equal to 1, we add 5 to our <code>primes ArrayList</code> and end up with <code>primes = [5]</code>, ending off with, of course, returning <code>primes</code>.</p>

<p>6 generating [2,3] also passed without any modification to our code because it's divisible by 2, so it entered that if statement, we add 2 to the primes. When we divide 6 by 2, we end up with 3, and since 3 doesn't equal 1, we add 3 to the primes ArrayList and get [2,3]. </p>

<p><i>* TDD Note: If you write a test, and it passes without modification to the code, this isn't the TDD way of Red/Green/Refactor, the way I remedy this is: I change the assert to something that I know should fail, for example -- 6 generating <code>[2,3]</code> passed without failing first, so what I did was change the expected result to <code>[10]</code>, or to something that is obviously wrong, then running the tests, since now it fails I can be sure that it passing wasn't a fluke, and it was in fact the intended behavior. I now will change the test code back to the passing state, and move on to my next test.</i></p>

<p>My next failing test was 8 generates <code>[2,2,2]</code>. The way I made this passed was a very simple change. All we needed to do was change the <code>if</code> statement on <code>line 8</code> to a <code>while</code> statement, so it will stay in that loop as long as number is divisible by 2.</p>

<p>After that test, 9 was when the algorithm changed the most. 9 needs to generate <code>[3,3]</code>, but all we're set up for is checking if <code>number</code> is divisible by 2. This was a bigger leap, and sometimes when it comes to writing algorithms you have to make a bigger leap to get where you need to be. I'll show how I made it pass, and then walk you through the code for clarity. (Plus, it helps me to understand Java better, so win/win).</p>

<p>Alright, I'm going to start at the top and work my way down as if we are <code>number</code> 9.</p>
````
package com.kata.junit;

import java.util.*;

public class PrimeFactors {
    public static List<Integer> generate(int number) {
        ArrayList<Integer> primes = new ArrayList<Integer>();
        if(number == 1) return primes;
        int i = 1;
        while(i <= number) {
            i++;
            while(number % i == 0) {
                primes.add(i);
                number = number/i;
            }
        }
        if(number != 1) {
            primes.add(number);
        }
        return primes;
    }
}
````


<ol>
  <li>Setting the <code>primes ArrayList</code></li>
  <li>Checking if 9 is 1 (<i>it's not so we move on</i>)</li>
  <li>Set an <code>int i</code> equal to 1</li>
  <li>Our first <code>while</code> loop executes as long as <code>i</code> is less than or equal to <code>number</code> (9)</li>
  <li>Since, 9 is less than 1, we enter the <code>while</code></li>
  <li>We increment <code>i</code>, now <code>i</code> is equal to 2</li>
  <li>We enter into our second <code>while</code> loop, while 9 is divisible by <code>i</code> (2), since 9 isn't we jump back up to our first <code>while</code> loop on <code>line 10</code></li>
  <li>We increment <code>i</code>, now <code>i</code> equals 3</li>
  <li>Enter back into our second <code>while</code> loop, while 9 is divisible by 3 (<i>it is, so we enter this loop</i>)</li>
  <li>We add <code>i</code> (3) to <code>primes</code>, (<code>primes</code> now = <code>[3]</code>)</li>
  <li>Divide 9 by 3, now <code>number</code> = 3</li>
  <li>Go back up into our <code>while</code> loop, is 3 divisble by 3> (<i>Yes, enter our <code>while</code> loop</i>)</li>
  <li>Add <code>i</code>(3) to <code>primes</code> (<code>primes</code> now = <code>[3,3]</code>)</li>
  <li>Set <code>number</code> equal to 3/3, which is 1</li>
  <li>Now, since 1 isn't divisible by 3, we move back up to our first <code>while</code> loop, but 3 is greater than 1, so it doesn't execute</li>
  <li>We exit the <code>while</code> loops, and down to our <code>if</code> statement on <code>line 17</code>, and check if 1 doesn't equal 1, and since 1 does equal 1, we don't add it to our <code>primes ArrayList</code></li>
  <li>Return <code>primes</code>, which is <code>[3,3]</code></li>
</ol>

<p>And that is our algorithm! Next, I did a sanity check test just to make sure it worked for a random number, I choose 1,300. The prime factorization of 1,300 should be <code>[2,2,5,5,13]</code>  I set up my unit test as:</p>

````
package com.kata.junit;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PrimeFactorsTest {
    private PrimeFactors primeFactors;
    private ArrayList expectedResult = new ArrayList<Integer>();

    @Before
    public void setUp() throws Exception {
        primeFactors = new PrimeFactors();
    }
...

 @Test
    public void thirteenHundredGeneratesTwoTwiceFiveTwiceAndThirteen() throws Exception {
        List actualResult = primeFactors.generate(1300);
        expectedResult.add(2);
        expectedResult.add(2);
        expectedResult.add(5);
        expectedResult.add(5);
        expectedResult.add(13);
        assertEquals(expectedResult, actualResult);
    }
}
````

<p>And my test passed, and that is the prime factors kata in Java</p>

<p>
  I find myself really liking Java a lot once I've gotten more familiar with it. I really like how you have to declare types, and the syntax is starting to come easier to me. I actually found myself adding semicolons to my Ruby code today when I wrote some. I will be writing the prime factors kata in Java probably 15 more times or so, I need it to be perfect without errors, typing, or otherwise, when I do it live for Mike and Will at my IPM next week. </p>

<p>To see this code in it's entirety, I've posted it to <a href="https://github.com/meaganewaller/java_katas/tree/5eb313f891170c1266ebd099ea517d71f4671f2b">Github</a>. I've also committed after every single step if you <a href="https://github.com/meaganewaller/java_katas/commits/5eb313f891170c1266ebd099ea517d71f4671f2b">view the history</a>.  What that means is that if you view the history, the first commit for this kata implementation is called: <a href="https://github.com/meaganewaller/java_katas/commit/99c80e681d4e28e31e5943ef32d00ef123937edd">Adds Second Kata Implementation</a>. After that you can view every single commit, I committed after I wrote a test, even a failing test, and then committed after I made the previous failing test pass. This is a good way for me, and for others, to see what TDD really looks like. TDD is all about writing one test at a time, the most simple test, and making it pass quickly and simply. </p>

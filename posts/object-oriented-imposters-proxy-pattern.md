---
title: "OO Imposters Proxy Pattern"
date: 2014-01-29
categories: ["Design Patterns", "Ruby", "Code", "Tutorials"]
---

The Proxy Pattern is another one of the OO Impostors in my design pattern series.

Let's say we have a class called `BankAccount` that keeps track of bank accounts (go figure):

````
class BankAccount
  attr_reader :balance

  def initialize(starting_balance=0)
    @balance = starting_balance
  end

  def deposit(amount)
    @balance += amount
  end

  def withdraw(amount)
    @balance -= amount
  end
end
````
Instances of `BankAccount` will be our real objects. Now, we have a proxy for `BankAccount` called (get this...) `BankAccountProxy`:

````
class BankAccountProxy
  def initialize(real_object)
    @real_object = real_object
  end

  def balance
    @real_object.balance
  end

  def deposit(amount)
    @real_object.deposit(amount)
  end

  def withdraw(amount)
    @real_object.withdraw(amount)
  end
end
````

Now we can create a bank account and a proxy for the bank account and use them more or less interchangeably.

````
account = BankAccount.new(100)
account.deposit(50)
account.withdraw(10)

proxy = BankAccountProxy.new(account)
proxy.deposit(50)
proxy.withdraw(10)
````

But that's kind of boring, right? It's using the same interface as the subject, `BankAccount`. The proxy doesn't know anything about high finance. Whenever someone calls a method on it, the `BankAccountProxy` turns to the real `BankAccount` object, delegating the method call to the subject.

This is where we might want to look into what type of proxies we can use and there are a few different types of proxies when it comes to the proxy pattern.

## Protection Proxy
A protection proxy protects an object from unauthorized access. To ensure methods can only be run by authorized users. We can verify this by running an authorization check before messages are passed to the underlying object.

````
require 'etc'

class AccountProtectionProxy

  def initialize(real_account, owner_name)
    @subject = real_account
    @owner_name = owner_name
  end

  def deposit(amount)
    check_access
    return @subject.deposit(amount)
  end

  def withdraw(amount)
    check_access
    return @subject.withdraw(amount)
  end

  def check_access
    if Etc.getlogin != @owner_name
      raise "Illegal access: #{Etc.getlogin} cannot access account."
    end
  end
end
````

````
# Protection Proxy
require './account_protection_proxy'
protection_proxy = AccountProtectionProxy.new(account)
protection_proxy.deposit(100)
protection_proxy.withdraw(25)
puts protection_proxy.balance
````

## Remote Proxy
Remote proxies provide access to objects that are running on remote machines. An example of a remote proxy is [Distributed Ruby (DRb)](http://www.ruby-doc.org/stdlib-1.9.3/libdoc/drb/rdoc/DRb.html), which allows Ruby programs to communicate over a network. With DRb, the client machines runs a proxy which handles all of the network communications behind the scenes.

## Virtual Proxy
Virtual proxies allows us to delay the creation of an until it's absolutely necessary. This is useful when the creation of an object is computationally expensive.

````
class VirtualAccountProxy
  def initialize(starting_balance=0)
    @starting_balance = starting_balance
  end

  def deposit(amount)
    s = subject
    return s.deposit(amount)
  end

  def withdraw(amount)
    s = subject
    return s.withdraw(amount)
  end

  def balance
    s = subject
    return s.balance
  end

  def subject
    @subject || (@subject = BankAccount.new(@starting_balance))
  end
end
````

````
# Virtual Proxy
require './virtual_account_proxy'
virtual_proxy = VirtualAccountProxy.new { BankAccount.new(25) }
virtual_proxy.deposit(10)
virtual_proxy.withdraw(20)
puts virtual_proxy.balance
````

## Using Message Passing to Simplify Proxies
When we build a proxy, we could implement a method for each method in the underlying object. But this leads to a lot of code repetition and tightly couples the proxy with the underlying object. A better solution is to pass method calls directly to the underlying object. Ruby includes a method that is perfect for this situation called `method_missing`.

In Ruby, when you call a method on an object it looks for a method in the initial object and its modules and then works it way up the stack to that object's superclass and then its superclass and so on. If the method isn't found, Ruby looks for the method `method_missing` in the initial object, then its parent, and its parent, etc.

Instead of implementing each of the underlying objects method in the proxy, we can use `method_missing` to simply pass method calls to the underlying object.

````
# account_protection_proxy.rb

require 'etc'

class AccountProtectionProxy
  def initialize(real_account, owner_name)
    @subject = real_account
    @owner_name = owner_name
  end

  def method_mssing(name, *args)
    check_access
    @subject.send(name, *args)
  end

  def check_access
    if Etc.getlogin != @owner_name
      raise "Illegal access: #{Etc.getlogin} cannot access account"
    end
  end
end

# account_proxy
class AccountProxy
  def initialize(real_account)
    @subject = real_account
  end

  def method_missing(name, *args)
    puts "Delegating #{name} message to subject."
    @subject.send(name, *args)
  end
end

#virtual_proxy

class VirtualProxy
  def initialize(&creation_block)
    @creation_block = creation_block
  end

  def method_missing(name, *args)
    subject.send(name, *args)
  end

  def to_s
    subject.to_s
  end

  def subject
    @subject ||= @creation_block.call
  end
end

# main

require '../bank_account'
require './account_proxy'
require './account_protection_proxy'

proxy = AccountProxy.new(BankAccount.new(100))
proxy.deposit(25)
proxy.withdraw(50)
puts "account balance is now: #{proxy.balance}"

# Generic virtual proxy
require './virtual_proxy'
array = VirtualProxy.new { Array.new }
array.push('hello')
array.push('out')
array.push('there')

puts array.to_s
````

This has a couple advantages:

 1.  The proxy is super simplified by having far fewer methods.
 2. The proxy isn't coupled to the underlying object and can be reused for multiple object classes.

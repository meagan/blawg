---
title: "Giving a Code Review"
date: 2013-12-17
categories: ["Apprenticeship"]
---

Giving code reviews are great ways to hone your craft, while also helping someone else hone their craft. This past week Taryn and I are gave each other code reviews on our Sinatra tic tac toe projects. This was my first time really giving a code review and I enjoyed it.

Having someone look over your code is a great way to identify confusing code. When you're the only one looking at a code base, you know what's going on. When you have a new set of eyes look over your code, especially if they have no context, they can tell you which parts of it are confusing. If you're the person giving the code review, you also benefit because it's easier to see when you're *breaking the rules* if you observe someone following them.

Realistically, we don't always the opportunity to have our code reviewed by our peers, but we can benefit from self-assessing our own code. While it won't quite be the same thing as having a fresh set of eyes on our code, there are things you can do to give yourself a proper code review. Something I like to do is to read my code out loud. I'll explain what it's doing out loud, method by method, class by class. If I find myself saying "or"/"and" a lot it's probably a safe bet that my method or my class needs to be broken up.

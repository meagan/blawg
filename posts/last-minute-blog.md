---
title: "Last Minute"
date: 2013-04-29
categories: ["Apprenticeship"]
---

Today's post is going to be rather short, I'm making some progress on my Tic Tac Toe. I actually spent the weekend working on it, and I can say with confidence that it will be done soon. I have *basically* everything planned out, just have to start reading up on the Minmax/Negmax algorithms and figuring that out. I actually spent my weekend being productive,
and I think it paid off. I usually spend my weekends doing a whole lot of nothing, and then on Monday my head is a little cloudy, I'm a little spacey, and I'm finding myself struggling to do things that I seemed to have a firm knowledge of just that past week.

Today I implemented some <code>stubs</code> into my testing and I'm getting the hang out of it. I might do another more in depth, specific blog post about it that sometime this week. I also want to write a post about Minmax/Negmax to help me to learn it at as well.

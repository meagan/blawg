---
title: "The Test Turned Green"
date: 2013-12-04
categories: ["Ruby", "Testing", "Apprenticeship"]
---

In yesterday's post I outlined the paths I had taken when trying to solve my Java mocking problem. I felt like I was running around in circles, totally frustrated and completely lost. I had been trying to mock System.Out classes for over a week now to not much avail. That's why yesterday was so frustrating, I seemingly had all the pieces in order, it should work in theory, so why wasn't it?

The code I was dealing with, for a refresher, looked like this:

````
// My unit test
public void itDisplaysAWelcomeMessage() {
        MockPrintStream printStream = new MockPrintStream(outputStream);
        cli.setOutput(printStream);
        cli.displayWelcomeMessage();
        assertEquals("", printStream.getStringHistory());
    }

// My MockOutputStream
public class MockOutputStream extends OutputStream {
    ArrayList<String> stringHistory = new ArrayList<String>();

    @Override
    public void write(int b) throws IOException {
    }

    public void write(String message) throws IOException {
        stringHistory.add(message);
    }

    public ArrayList<String> getStringHistory() {
        System.out.println("hi");
        System.out.println(PrintStream.class);
        return stringHistory;

    }
}

// My MockPrintStream

public class MockPrintStream extends PrintStream {

    public ArrayList<String> stringHistory;

    public MockPrintStream(OutputStream out) {
        super(out);
    }

    @Override
    public void println(String message) {
        stringHistory.add(message);
    }

    public ArrayList<String> getStringHistory() {
        return stringHistory;

    }
}

// Relevant part of my CommandLineInterface
public class CommandLineInterface {
PrintStream output = System.out;

    public void setOutput(PrintStream output) {
        this.output = output;
    }

    public List<Color> promptCode() {
        output.println("Enter Guess: ");
        String userInput = null;
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            userInput = in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return convertInput(userInput);
    }

    public void displayWelcomeMessage() {
        output.println("Welcome to Mastermind!");
        output.println("The code is made up from 4 colors, the initials of the colors are r, g, y, b, p, o (red, green, yellow, blue, purple, orange)");
        output.println("Good luck!\n");
    }
}
````
Whenever I would run this test, I was getting a `NullPointerException` and a stack trace indicating it was coming from `println` in my `MockPrintStream` class. I felt like I had exhausted all of my options, and I had gone down many paths that only became more twisted and more convoluted the further down I went. Finally I reached out to Mike, since I'm really trying to be someone who seeks help and isn't afraid to ask for it in lieu of staring at my computer screen waiting for a breakthrough that more than likely, isn't going to happen.

After talking with Mike he asked me a simple question,
> "Where are you setting your stringHistory ArrayList?"

I immediately realized I hadn't set one, and promptly set one and re-ran the test. After rerunning the tests, and seeing it fail with what I was expected I laughed out of pure relief and realized that tearing my hair out would have to wait for another day, fortunately.

## What's the Lesson Learned?
I had to learn what a `NullPointerException` was, and why I was getting one to even attempt to debug and reason this. A `NullPointerException` is thrown when an application attempts to use `null` in a case where an object is required, this includes:

- Calling the instance method of a `null` object
- Accessing or modifying the field of a `null` object
- Taking the length of `null` as if were an array
- Accessing or modifying the slots of `null` as if it were an array
- Throwing `null` as if it were a `Throwable` value

As you can see, I was trying to modify the field of a `null` object since my `stringHistory` was never actually set.

So, basically the reason that I was getting a `NullPointerException` is because when `MockPrintStream` called `println` and tried to `add message` to `stringHistory`, it couldn't, because `stringHistory`, a field that hadn't been set, was trying to be modified with the `add` method.

The lesson to be learned is to be more familiar with the exceptions and errors that you get from your tests. If I had known more about what a `NullPointerException` was the answer might've come quicker to me. However, another valuable lesson to be learned is that when you have someone who is more experienced, or someone in a mentor position, look over your work if you're struggling, well, you know the saying of "two heads are greater than one". A second person looking over your work can be a great way to find those silly things like typos, forgetting a `setter`, or even help fill in gaps in your knowledge, and as an apprentice I need to remember to always be seeking feedback, and ways to improve my code.



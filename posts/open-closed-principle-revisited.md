---
title: "Open Closed Principle Revisited"
date: 2013-12-17
categories: ["Java", "Tutorial", "Code", "SOLID Principles"]
---

I originally wrote about the Open Closed Principle [a few posts back](http://blog.meaganwaller.com/open-closed-principle). At the time of my posting I hadn't had any experience with Java, so trying to make sense of some of the examples and translate to Ruby made that difficult. However, now that I'm much more familiar with (and actually enjoy) Java, I wanted to revisit it.

## What is the Principle?
The Open/Closed principle states that software entities, this includes classes, modules, functions, etc, should be open for extension, but closed for modification.

Okay, what does that mean? What this means is that we shouldn't have to rewrite or change code every time our requirements change. We can solve this with inheritance.

## Area Finder

We have a `Rectangle` class, and it has a `width` and a `height` as rectangles do.

````
public class Rectangle {
	private int width;
	private int height;

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

}
````

However, we want to be able to calculate the areas of a bunch of different rectangles. And, as we know the area for a rectangle is width multiplied by height. We can accomplish that with a for loop.

````
public class AreaCalculator {
	public int area(Rectangle[] shapes) {
		int area = 0;
		for(Rectangle shape : shapes) {
			area += shape.getWidth() * shape.getHeight();
		}
		return area;
	}

}
````

However, what if we want to know the area of a different shape, like a square, or a circle in the future? We want to extend it so that it can calculate the area of rectangles, and squares, and circles too.

We need a square and circle class first

````
public class Square {
	private int side;

	public void setSide(int side) {
		this.side = side;
	}

	public int getSide() {
		return side;
	}

}

public class Circle {
	private int radius;

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public int getRadius() {
		return radius;
	}

}

````
Now let's check out our `AreaCalculator`:

````
public class AreaCalculator {
	public int area(Object[] shapes) {
		int area = 0;
		for(Object shape: shapes) {
			if(shape instanceof Rectangle) {
				Rectangle rectangle = new Rectangle();
				area += rectangle.getWidth() * rectangle.getHeight();

			}
			else if(shape instanceof Square) {
				Square square = new Square();
				area += square.getSide() * square.getSide();
			}

			else {
				Circle circle = new Circle();
				area += circle.getRadius() * circle.getRadius() * Math.PI;
			}

		}

		return area;
	}

}

````

This does solve our problem, yes, if we only want to check the area for circles, squares, and rectangles. However, what if down the road we want to do it for triangles, or ellipses? That wouldn't be difficult, we could just add another `else if` statement. But that modifies our code, our code is not **closed for modification**, we need to change something about it in order to extend it, it's not **open for extension**.

This is a pretty simple example, but if you're dealing with a big code base something where you're dealing with deployment servers, different packages, and a code base thats hundreds of times larger, this is a much bigger problem.

## Area Calculator using Open/Closed Principle

You could solve this with open/closed principle by creating a base class for all shapes to inherit from.

````
public interface Shape {
	public double area();
}
````

Now our shapes can inherit from the `Shape` interface.  They might look a bit like this now:

````
public class Circle implements Shape {
	private int radius;

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public int getRadius() {
		return radius;
	}

	@Override
	public double area() {
		return radius*radius*Math.PI;
	}

}

````

````
public class Square implements Shape {
	private int side;

	public void setSide(int side) {
		this.side = side;
	}

	public int getSide() {
		return side;
	}

	@Override
	public double area() {
		return side * side;
	}

}
````
````
public class Rectangle implements Shape{
	private int width;
	private int height;

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	@Override
	public double area() {
		return width * height;
	}
}
````

We've moved the responsibility of calculating the area out of `AreaCalculator`, and now the method is much simpler and reads better and is extensible since it can handle any type of `Shape` we might think of.


````
public class AreaCalculator {
	public double area(Shape[] shapes) {
		double area = 0;
		for(Shape shape : shapes) {
			area += shape.area();
		}

		return area;
	}
}
````

This means that we've closed it for modifications, and opened it to extension. Now in the future if we want more shapes, we just have to add a new class and implement the `Shape` interface.

## Wrap Up
Knowing when to implement the SOLID principles, like Open/Closed principle take time, and practice. You don't want to write more code than you have too, and you don't want to write something that your tests or requirements aren't telling you need yet. If we only ever needed to calculate the area of a rectangle, implementing from an interface to do so would be kind of overkill.

Once the requirements change once, it's probably likely they'll change again, you might want to start planning for making sure your code is open to extension and closed for modifications.

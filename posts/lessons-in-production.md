---
title: "Lessons in Production"
date: 2014-02-11
categories: ["Apprenticeship"]
---

This week I learned first hand how important it is to be better organized and better prepared. Taryn and I have weekly IPMs where we deliver demos and stories for Footprints to our client (a craftsman at 8th Light). This week we learned first hand how important it is to be prepared. We pushed code to production about 20 minutes before our IPM, and it was unfortunately broken code. We were in a rush, and we were messy, and our IPM suffered because of it. I know I was unconfident in our demo, and unconfident in myself, and that showed throughout our meeting and throughout our demo.

This week, in order to avoid ever having to deal with something like this again, we are going to implement better practices. We will have a code freeze on Thursday. Our IPMs are on Friday, so we shouldn't be writing production code on this day, we should spend Friday preparing for our demo, and have a retroactive between ourselves about what did and didn't go well during our iteration that week. Another problem we had was that our code wasn't 100% under tests, we were careless and it showed. We came into the office on Saturday, and spent a large part of the day putting our entire code base under tests. We are now at 98% test coverage according to SimpleCov, and before we push to production we will run our test suite to ensure that everything is passing.

We also learned that we have to be sure to check in on our stories while we're doing them. We sort of just went by memory on our stories, and ended up forgetting to include some details our client wanted so some of our stories didn't technically get finished, and that was definitely a disappoint. This week our goal is to be more prepared, to test first, and to test often while keeping a short feedback loop. We won't push to production unless everything is 100% passing and working in our staging environment first. Careless mistakes like these can easily happen, but they can also easily be avoided by putting agile practices in place.

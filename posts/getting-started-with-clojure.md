---
title: "Getting Started with Clojure"
date: 2014-02-24
categories: ["Clojure"]
---

I started learning Clojure last Monday. I wrote [this post](/posts/preparing-to-learn-clojure) about how I would prepare to learn Clojure. I said there were a few things I'd have to do first to get prepared to learn Clojure. I did both of those things. The first was to set up a development environment, and the second was to set up a testing framework.

## Development Environment
I use vim (well actually, MacVim) for my development for nearly every language I've developed
in with the exception of Java (I use Eclipse for that). I even write my blog
posts in vim. In order to get ready to write Clojure I installed a few Clojure
plugins.

  - [Rainbow Parens](https://github.com/kien/rainbow_parentheses.vim): In
     Clojure there are *a lot* of parentheses, being able to easily tell which
     parens are closing the opening ones is very important. This plugin, to me,
     is a must have.
  - [Vim ClassPath](https://github.com/tpope/vim-classpath): This plugin sets
    the `path` for JVM languages to match the class of your current Java
    project. (Clojure runs on the JVM).
  - [Vim Clojure Static](https://github.com/guns/vim-clojure-static): This
    plugin is a **must have**. It provides syntax highlighting, proper
    indentation, basic insert mode completion for parts of `clojure.core`.
  - [Vim Fireplace](https://github.com/tpope/vim-fireplace): Fireplace lets you
    evaulate from the buffer, I haven't used this plugin too much yet, but it
    seems really promising.

## Testing Framework

The testing framework I'm using is [Speclj](http://speclj.com/). Speclj was
created by [Micah Martin](http://www.8thlight.com/team/micah-martin), so I might
be slightly biased towards using it.

Speclj was a breeze to setup as long as you have [Leiningen](https://github.com/technomancy/leiningen) installed just run the command `lein new speclj YOUR-PROJECT-HERE` to generate a project using Speclj.

The [Speclj tutorial](http://speclj.com/tutorial) is a good place to start, it'll [walk you through your first tests](http://speclj.com/tutorial/step2), and even show you some of the best conventions for testing, including [how to test input and output](http://speclj.com/tutorial/step5), and [exceptions](http://speclj.com/tutorial/step6#ch9). I'm not going to rehash everything here, but the tutorial will be very helpful for first-timers. I refer to it a lot still, as well.

## Gotchas

I did have one annoying problem that I was able to fix with relative ease. My
indention for my `.clj` files was perfect, but my indention for my Speclj spec
files were less than stellar, it was actually awful. The problem was that in the
Vim Clojure Static indention file there was no indention for Speclj, I modified
the indention file to allow for better indention. In `vim-clojure-static/indent/clojure.vim` I added the following:

````
" Vim indent file
....

"Speclj Indentation Support
setlocal
lispwords+=describe,it,context,around,should=,should-not=,should-not,should,should-be,with,run-specs
...
````

*(The above file can be found in your* `~/.vim/bundle` *directory if you
  installed with [Pathogen](https://github.com/tpope/vim-pathogen)).*

This just adds those words (`describe,it..` etc) to the list of `lispwords`
locally so it knows to apply indention for those words.

Now my indention in my Speclj files is not making me pull my hair out anymore. I think I am going to submit a pull request to the repository so that future Speclj users won't have to worry about it anymore.

*edit on Feb 25, 2014:
[I submitted a pull request to the vim-clojure-static plugin](https://github.com/guns/vim-clojure-static/pull/45), if it doesn't get merged in, at least it'll be a reference point for anyone struggling with this.*


## Wrap Up
These tools have been really helpful to my Clojure development process. Having a
good development environment and honing your tools means you'll spend more time
coding and less time messing with your tools.

This is important when learning a new language, it's important to spend as much of your time working on solving your problems, not on worrying about syntax, or indention, or fighting your
tools.







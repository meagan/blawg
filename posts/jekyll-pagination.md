---
title: "Jekyll Pagination"
date: 2013-05-20
categories: ["Ruby", "Code", "Tutorial"]
---

I've been wanting to add pagination to my blog for a while. It's annoying to have to scroll to the bottom once there's more than 5 blog posts on a page. It's especially annoying when all of those blogs posts are 800+ words long like mine tend to be. I will show you how I got pagination on my blog. Let's get started.

The first thing you need to do is to modify your `_config.yml` file. You need to include

`paginate: 5`


, feel free to use whatever number you want instead of 5. This tells your blog how many posts should be displayed per page. By adding `paginate` to our `_config.yml` file we are given access to the paginator object.

## What is the Paginator?

Basically, when Jekyll sees that we have added pagination to our `_config.yml` file we have access to the paginator object. There are many methods that you can call on this object, however, we are just going to worry about `#previous_page` and `#next_page`

You might be thinking that `#previous_page` returns the previous page number, and that `#next_page` returns the next page number of paginated posts. If you were thinking that you would be absolutely correctly. Also, it's handy to note that if there is no previous or next result `nil` is returned. We can use this knowledge when we create our navigation.

Next you need to know that there is a certain naming convention when it comes to our paginated pages. Jekyll uses the labels "page2", "page3", "page4", and it goes on until you have no more paginated posts. I didn't leave out "page1" because I wanted to confuse you, Jekyll doesn't generate a "page1". Keep this in mind as you read through this post.

Now I am going to show you how to get a simple pagination navigation that shows a Next and Previous link, as well as displays how many pages there are and what page the user is currently on.

## Pagination in Action

We know that we want our pagination to act as navigation, so it would make sense to wrap the next page and previous page links in a container. And since it is navigation, we can go ahead and use the element.
````
 <nav id="pagination">
        ...
    </nav>
````
So, we have the container for the pagination, now we need to add in the links themselves.

The quickest and simplest way to do so would be to just put the two links into the container and be done with it:
````
  <nav id="pagination">
    <a href="/page{{ paginator.previous_page }}/" title="Previous Page">&laquo; Previous</a>
    <a href="/page{{ paginator.next_page }}/" title="Next Page">Next &raquo;</a>
  </nav>
````

However, we're going to run into problems if we do it this way. For starters, the 'next link': what if there isn't a next page? We probably shouldn't display it by default. We can solve this problem by taking advantage of the fact that the `#next_page` method returns nil if there is no next page. We can use an if statement to remedy that problem:

````
  <nav id="pagination">
    <a href="/page{{ paginator.previous_page }}/" title="Previous Page">&laquo; Previous</a>
    {% if paginator.next_page %}
    <a href="/page{{ paginator.next_page }}/" title="Next Page">Next &raquo;</a>
    {% endif %}
  </nav>
````

This will now only display the "next page" link if there is actually a next page. But what about the "previous page" link, we're stuck with the same problem. When a user is on the index page with the 5 (or however many you decided for your pagination) most recent posts there wouldn't be a previous page. There are a couple problems to be solved with previous page, let's solve the first one similar to how we solved the "next page" problem:
````
  <nav id="pagination">
    {% if paginator.previous_page %}
    <a href="/page{{ paginator.previous_page }}/" title="Previous Page">&laquo; Previous</a>
    {% endif %}

    {% if paginator.next_page %}
    <a href="/page{{ paginator.next_page }}/" title="Next Page">Next &raquo;</a>
    {% endif %}
  </nav>
 ````
Now the "Previous Page" link will only be displayed if there is a previous page of posts. However, there is problem with this, you might recall from earlier I mentioned how Jekyll doesn't make a page1 folder. The paginator labels each page as '/pageN'. That means the first page would be a URL that looks like 'http://yoursite.com/page1'. This will obviously return in a 404 error since there is no '/page1'. The first page of results needs to point to your room 'http://yoursite.com/'. We can fix this easily with an if-else statement and bringing in the `#previous_page` method.
````
  <nav id="pagination">
    {% if paginator.previous_page %}
      {% if paginator.previous_page == 1 %}
      <a href="/" title="Previous Page">&laquo; Previous</a>
      {% else %}
      <a href="/page{{ paginator.previous_page }}/" title="Previous Page">&laquo; Previous</a>
      {% endif %}
    {% endif %}

    {% if paginator.next_page %}
    <a href="/page{{ paginator.next_page }}/" title="Next Page">Next &raquo;</a>
    {% endif %}
  </nav>
 ````
I promised y'all that I would show you how to do a simple pagination menu (which, the code above would take care of, no problem), however I also said I would include a way to show how many total pages there as well as what page the user is currently on. You can do this by using the code below:

````
<nav id="pagination">
    {% if paginator.previous_page %}
        {% if paginator.previous_page == 1 %}
        <a href="/" title="Previous Page">&laquo; Previous</a>
        {% else %}
        <a href="/page{{paginator.previous_page }}/" title="Previous Page">&laquo; Previous</a>
        {% endif %}
    {% endif %}
        <span>Page: {{ paginator.page }} of {{paginator.total_pages }}</span>
        {% if paginator.next_page %}
    <a href="/page{{ paginator.next_page }}/" title="Next Page">Next &raquo;</a>
    {% endif %}
  </nav>
  ````
The paginator object can also call the methods `page` and `total_pages`, they're pretty self-explanatory! If you're interested in anymore of the methods paginator can call, check out the Jekyll docs.

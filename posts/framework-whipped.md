---
title: "Framework Whipped"
date: 2014-05-11
categories: ["Technology", "Diversity"]
---

*This is a response to Robert Martin's disgustingly misogynistic blog post, called
"Framework Whipped", it's since been taken down, but my criticism still stands*

*Robert Martin has rewritten his post, [Framework
Bound[2]](http://blog.8thlight.com/uncle-bob/2014/05/11/FrameworkBound.html). He
has also [issued this
apology.](https://gist.github.com/unclebob/2abcce451bafeab421f2)*


Frameworks are powerful tools, I'd say people can't be tools, but if you write a blog post comparing women to frameworks complete with sexist tropes you *might* be a tool.

The relationship between a programmer and a framework is similiar to the
relationship between a programmer and a framework, or a programmer and a hammer,
or a programmer and a power drill, or a programmer and a *tool*.

The relationship between an executive and an administrative assistant is similar
to the relationship between someone in power and someone who answers to them.
These are real people, and power dynamics come into play. But let's be real,
when you say executive you mean man, and when you say administrative assistant
you mean woman.

Finishing off the first section with the short sentence, *we
marry our secretary*. As if that's comparable to using a framework? Ah, women,
they're just replaceable tools, and in a few months, or years, you can just
upgrade to the new model, the new version, just like you can with frameworks
right?

Your metaphor is spot on, *if you believe that women aren't people*.

I don't even want to touch on your use of the word "harem" as your
header for the next section. But let's dig into that, shall we? Since I don't
think you're talking about the part of a Muslim residence reserved for women,
I'm going to assume you mean the "tongue in cheek" version, the version where a
group of woman is associated with one man, I guess collecting women as if
they're objects, or *frameworks* does fit that definition, gross. Let's
move on.

Using a framework requires a significant commitment, yes. You allow the
framework to handle some, if not most, of the heavy lifting for you, and you
have to compromise on some of your conventions possibly. You might sacrifice
some speed for conveience, for example.

Is this a trap? I suppose if you went into it thinking that there wouldn't be
compromise, that you wouldn't have to adjust your way of doing things. But is that the
fault of the framework?

I've never had a framework smile prettily at me, or entice me to move in closer,
if you have you might want to step away from your computer for a while.

I guess if you view a heterosexual marriage as one where the man delegates tasks
to the woman and she is obendient and subservient, than your metaphor sticks. I
guess if the thought of allowing the woman to do any of the heavy lifting is
just preposterous your metaphor is fantastic. I guess if you view women
as objects, as not quite people, as tools to use for clickbait, for getting a
point across, as literary devices, your metaphor is fucking fantastic.

Calling it framework whipped is funny though right, because if a woman gets to
dictate to a man or gets to call the shots than he's whipped. This is grade A
humor over here folks. When the roles are reversed it's not so desirable anymore
is it? Hmmm, wonder why that is?

And can we please talk about how fucking disgusting the use of the word
concubine is? Comparing frameworks and code to concubines undermines the real
pain and suffering that women who were, and are, concubines face. Women in pain,
women suffering, women are not your literary metaphors. If you need to rely on
sexist languages, and sexist tropes to get your point across, your point fucking
sucks.

And I know, you were trying to be *edgy*, and *hyperbolic*. It's just so
original. No one has ever used woman as a punchline, no one has ever taken away
women's autonomy and compared them to objects. It's so cutting edge, so bold of
you to aim so high.

As someone who works at 8th Light, someone who is a feminist to their core, who
works to break down the systematic barriers in technology, posts like this on
the blog of the company I work for undermine what I do.

Robert Martin has a platform bigger than what I could ever hope for. Even if I
wanted to I couldn't silence him, I couldn't hope to silence him, his platform
is huge. All I'm asking is that he considers who is affected when he posts
things like this on a company's blog.

Is this what you want 8th Light to
represent? Posts like this one, and [this
one](http://blog.8thlight.com/uncle-bob/2014/04/25/MonogamousTDD.html), and
[this
one](http://blog.8thlight.com/uncle-bob/2014/02/21/WhereIsTheForeman.html), and
[this one](http://blog.8thlight.com/uncle-bob/2013/11/19/HoardsOfNovices.html),
and especially [this fucking
one](http://blog.8thlight.com/uncle-bob/2013/03/22/There-are-ladies-present.html) all make me as an employee feel uncomfortable, undermined, and confused. Posts like these belong on a personal blog. They don't represent how I feel, or how I hope the company at large feels.

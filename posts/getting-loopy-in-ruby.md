---
title: "Getting Loopy in Ruby"
date: 2013-05-22
categories: ["Ruby", "Code", "Tutorial"]
---

When writing programs we often time need to repeat a task many times quickly and efficiently. The way to do this is by using a loop. A loop is a construct that allows a program to execute thousands of commands in less than a second with a little amount of code. With a loop a program can quickly and easily output much more (try millions of times over) than what you input.

## The For Loop

It is tedious and unnecessary to type the same code over and over again. This also goes against an important programming principle of DRY -- Don't Repeat Anything.


````
puts "My favorite number is 1"
puts "My favorite number is 2"
puts "My favorite number is 3"
puts "My favorite number is 4"
puts "My favorite number is 5"
````

When you use a for loop you can repeat an action as many times as you want. You can also vary output depending on the amount of times the loop has run.


````
the_numbers = [1, 2, 3, 4, 5]
for number in the_numbers
  puts "My favorite number is #{number}"
end

#=> My favorite number is 1
#=> My favorite number is 2
#=> My favorite number is 3
#=> My favorite number is 4
#=> My favorite number is 5
````

However, the for loop isn't used very much by Ruby developers. It's still good to know because the syntax is similar in many languages.

## While Loops

A while loop executes the code block inside of the loop as long as the boolean expression is True. A while loop will execute similarly to an if statement but instead of running the code block once, it will return to the top where you defined while and repeat until the boolean expression is False. We have to be careful when writing while loops because sometimes they can infinitely run which is not a desired effect; unless you never want your loop to end, ever. It's important to review your while statement and make sure that what you're testing will become False at some point.


````
i = 0
while i < 10
  i += 1
  puts i
end

puts i
#=> 1
#=> 2
#=> 3
#=> ...
#=> 10


x = 50
while x > 0
    x -= 2
    puts "#{x} more times"
end

#=> 48 more times
#=> 46 more times
#=> ....
#=> 0 more times
````

## Until Loops

The until loop does basically the opposite of the while. An until loop will continue executing the code in the loop until the boolean expression is True. So it runs while the boolean expression is False.


````
i = 0
until i == 10
    i += 1
    puts i
end

#=> 1
#=> 2
#=>...
#=> 10

x = 50
until x == 0
    x -= 2
    puts "#{x} more times"
end
#=> 48 more times
#=> 46 more times
#=> ....
#=> 0 more times
````

## Using While and Until to Simulate Do..While

Ruby allows us to use the while and until keywords as modifiers. This means you can put them at the end of an expression just like you can do with an if statement.


````
i = 0
puts "#{i += 1}" while i < 10

x = 50
puts "#{x -= 2} more times" until x == 0
````

Both will put the same output as the examples above it. This property of the while and until keyword lets us simulate the do..while loop in Ruby. Ruby doesn't have a looping construct that is guaranteed to always execute at least once (which the do..while does), but we can do this:


````
i = 15
begin
    puts "#{i}"
    i += 1
end while i < 14
````

This will output the number 15, even though we've set the loop to terminate when the i < 14, the loop is guaranteed to run at least once. That means we get the value of i printed out once.

We can use a similar concept with until
````
i = 15
begin
    puts "#{i}"
    i -= 1
end until i == 14
````

This outputs the number 15 and then exits the loop since the exit condition of i == 14 will be reached.

There are more ways to iterate in Ruby, but these are great starting points since the syntax is found in many other languages. Once you tackle loops you're well on your way to solving more complex problems!

---
title: "Introduction to Minimax"
date: 2013-05-01
categories: ["Ruby", "Algorithms", "Apprenticeship"]
---

This blog post is going to be more for me to sort of put out all my thoughts on Minimax, an algorithm, specifically the algorithm I'm going to use for my Tic Tac Toe game. This post may be disjointed, just a warning.

## What is Minimax?

Minimax is a decision rule, I'm going to be implementing minimax into my game, but it's also used in decision theory, statistics, and philosophy. Minimax is used for *mini*mizing the possible loss for a worst case (*max*imum loss) scenario. Minimax was originally formulated for two-player zero-sum games (like Tic Tac Toe), it covers both the cases where players take alternating moves, and games where players take simultaneous moves. As stated earlier, Minimax isn't just for game theory in zero-sum games, it's been used in more complex games and in general decision making when faced with uncertainty.

## What is the Minimax Theorem?

The minimax theorem states:
> For every two-person, zero-sum game with finitely many strategies, there
> exists a value V and a mixed strategy for each player, such that
>>	1. Given player 2's strategy, the best payoff possible for player 1 is V and
>>	2. Given player 1's strategy, the best payoff possible for player 2 is -V

So, Player 1's strategy guarantees him a payoff of V regardless of Player's 2 strategy, and Player 2 can guarantee himself a payoff of -V. The name is minimax because each player minimizes the maximum payoff possible for the other, and since the game is zero-sum, the player also minimizes his own maximum loss.

## Game Trees

A game tree has thousands of possible game states. There are potientally a total of 9 game states that could occur from this first move. From each of those game states, there are 8 more, from each of those 7 more, and so on, this means that in a TicTacToe game there are 9! game states. (That's 9 factorial, not me being excited about the number 9, 9! = 362,880) However, for various reasons, while this is *technically* how many combinations there are, most intelligent players won't play anything except a corner, or the center on their first move. That means we're only starting with 5! possible game states, and then there are other exceptions but that's not really what this is about, so I digress.

In a game tree, the first Game State would show nine moves coming from it, one for each potential move the player could make with the 9 empty spaces on the board.

The next level of Game States would show eight moves, because there would be only eight empty spaces after the first player made their move.

This continues until you reach one.

When you represent the game as a game tree the computer can evaulate each of its potential moves by determining whether it will ultimately end in a win or a loss.

Basically, the main concepts of defining a Game Tree for Tic Tac Toe:

1. The board state, where the X's and O's are
2. The player who is currently making a move
3. The next available moves, this is where the difficulty might be for some, as a human you don't think like a computer, you physically place your marker onto the board. To better understand the algorithm, just remember that the computer is *thinking* "I've selected the next game state", but this basically signifies when you would switch turns.
4. The actual game state, which are the 3 previous concepts.

**TLDR?** A game tree organizes all possible (legal) game states by moves that allow you to go from one game state to the next (make a move (select a game state) -> switch turns -> make a move(select a game state) -> repeat). This is ideal for an AI because the computer can evaluate what moves to make because by going through EVERY possible game state the computer will know the outcome of a certain move and can decide whether or not to take it.

## Ranking the Game States
When ranking game states, the most common approach is to assign numerical value to a move based on whether it will result in a win, draw, or loss.

For example, if it was the computer's turn, and the computer was X, and if, for example, X could win by moving into the upper right corner, then X should take that spot. But, how does the computer *know* that's a winning spot. The computer knows because we gave it a numerical value based on its board state.

The common way to rank it is like this

 - Win: 1
 - Draw(Tie): 0
 - Lose: -1

 You could choose whatever numbers you want for the ranking, as long as the highest is the winner, the lowest is the loser, and a draw is in the middle.

 The highest-ranked moves corresponds with the best outcomes, and the lowest with the worst, we should obviously choose the move with the highest value. That's where "max" comes from in Minimax.

 However, the AI would pretty useless if the only thing it could measure was "winning move" or "losing move", what kind of game would start at the final stage?

 When you're dealing with intermediate game states the choices aren't always "win immediately" or "lose immediately". Remember, a move that results in an immediate win for O (or whoever the opponent is) is a loss for X. That means, from X's perspective, the game state that leads to an immediate win for the opponent should be marked as a -1, meaning a loss. This is where "mini" in Minimax comes from, the rank of intermediate Game State where O is the current player should be set to the *minimum* rank of the available moves.

**TLDR?** The final Game States are ranked by if they're a win, draw, or loss. While Intermediate Game States (in progress Game States) are ranked according to whose turn it is and the available moves. For example

- If it's X's turn, set the rank to that of the **maximum** available move. (If the move results in a win, take it.)
- If it's O's turn, set the rank to that of the **minimum** available move. (If a move results in a loss, avoid it.)


I don't have an example of it in action yet, but I should have something that I've worked out by tomorrow's blog post.

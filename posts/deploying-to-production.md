---
title: "Deploying to Production"
date: 2014-02-06
categories: ["Apprenticeship"]
---

This week Taryn and I deployed Footprints to production. Footprints is an internal 8th Light application that we've been working on for a little over a month now. The purpose of Footprints is to track our apprentice applicants through the application process. Currently the way we've been doing that isn't probably the most ideal, we've been using a spreadsheet to do the tracking, but this leads to a lot of manual updating, and with the sheer amount of applications we get on a weekly basis this can be quite cumbersome. Footprints is going to help to automate this process and make everything a bit more transparent and hopefully get more of our applicants through the entire application process.

This was the first application that I've ever pushed into production, and I'm really happy to be able to work on internal application for 8th Light, especially an application that will help the apprenticeship program since I really believe in it.

This time last year I had never written a single line of software. I had written some static HTML websites, but even then I didn't have much idea of what I was doing. I'm kind of amazed that in just a year how far I've come. I've gone from not even knowing what testing is, to deploying a tested rack application to production, an application that will be used and utilized at 8th Light. I'm excited to continue working on this and hopefully be able to see it through to the end.

This has also helped us become more familiar with what it might be like to work with a client. We have a "client", our apprentice steward, Ryan, a craftsman at 8th Light. We have our weekly IPMs, and we get to demo our application and our stories to him every Friday. We've learned a lot about how IPMs work, and how to demo applications, and I'm really thankful for this opportunity, and it definitely has helped me to feel more confident as I near the end of my apprenticeship.

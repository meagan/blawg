---
title: "Test Driven Development Part Two"
date: 2013-07-18
categories: ["Apprenticeship", "Book Reviews"]
---

If you read my previous post on Test Driven Development, you might have some questions. Questions like:

> What does testing mean? When should you test? How do you choose what logic to test? What data?

Not to worry, these questions will all be answered when you learn about Test-Driven Development Patterns. I'm going to go over my favorite parts of the book, the things that I thought were the most useful, and great places to start off with Test Driven Development. I'm going to be going over, Test Driven Development Patterns, Red Bar Patterns and Green Bar Patterns in this blog post.

## What Does Testing Mean?

Testing is an important part of being a software craftsman, more importantly, Test Driven Development. To have tests, and to test are two different things, however. Test, as a verb, means "to evaluate", whereas test as a noun means "a procedure leading to acceptance or rejection". A software craftsman should have tests, procedures that lead to acceptance or rejection, for all the code that they write. It's important to write tests for every piece of code to avoid a terrible infinite loop. The infinite loop isn't a particular piece of code, but rather a positive feedback loop that deals with stress. For example, the more stress you feel, the less testing you might do, the less testing you do the more errors you will make, the more errors you make the more stress you feel, and repeat forever. You can break that loop, but how? The way we're going to break the loop is by changing Testing to Automated Testing. When you introduce automated testing and you start to feel stressed, simply run the tests. Think of tests as your "Programmer's Stone", it changes fears into boredom. You can simply say, "Oh, great, all my tests are still green, no need to be stressed," the more stress you feel, the more you run the tests.Running the tests immediately, at the first sign of stress reduces the numbers of errors you'll make, which will help to reduce your stress levels.

## Isolated Test

When you run your tests, they shouldn't affect each other, at all. Your tests need to be able to ignore one another, completely. For example, if you have one broken tests this should tell you there is only one problem, and if you have 15 broken tests there should be 15 problems. Your tests need to be order independent, so if you want to just grab a subset of tests and run them, you can, without worrying that a test will break because a prerequisite test is gone. However, there are some reasons why people might have tests share data, one is for performance, this is the usual reason that people will give. Another is that you have to work, sometimes you have to work pretty dang hard to break up your problems so that setting up the environment for each test is quick and easy. Isolating tests encourages you to compose solutions out of many highly cohesive, loosely coupled objects.

## Test List

Alright, so you have an idea of what testing means, you also now know that your tests should be independent of each other. Now, what exactly are we supposed to test? The TDD book suggests that you write a list of all the tests you know you will have to write, they want you to do this to "never take a step forward unless we know where our foot is going to land". However, I find that when I write out a list of everything I think I'm going to have to test, I don't really let the code reveal itself. However, this technique might work for some, so I'll briefly go over it:

1. Put on the list examples of every operation that you know you need to implement
2. For those operations that don't already exist, put the null version of that operation on the list
3. List all of the refactorings that you think you will have to do in order to have clean code at the end of this session.
4. Items left on the list when the session is done need to be taken care of
5. If you can think of a test that might not work, getting it to work is more important than releasing code.
One thing that the book, and I, agree on is not to implement a bunch of tests at once. There are a few reasons for this:

1. Every test you implement is a bit of inertia when you have to refactor
2. If you have 15 broken tests, you are 15 steps away from green. With TDD, it's important to follow the red/green/refactor, you should never be more than one step away from a green test at all times.
The way that I approach discovering what to test is by just thinking of the simplest case, for example, if I'm writing the FizzBuzz kata, the simplest thing to test would be


````
describe FizzBuzz do
  it "should convert 1 to 1" do
    fizzbuzz = FizzBuzz.new
    fizzbuzz.convert(1).should == "1"
  end
end
````

Then, make the test pass


````
class FizzBuzz
  def convert(input)
    "1"
  end
end
````

However, when I was first starting out with TDD, I thought it'd be helpful to write down everything I thought I was going to need to have, and then going from there. What ended up happening was that my tests weren't guiding my code, rather my to-do was guiding my tests.

## Test First & Assert First

When should you write tests? If you answered, "before you write the code that you're testing", you'd be absolutely correct. This is the cornerstone of TDD, if you'll remember the mantra of red/green/refactor, the first step is red, this means that we should have a failing test before a passing test. Testing first reduces stress, like we discovered earlier, and it provides a design and scope control tool. To test first is to suggest that we will be able to start doing it, and keep doing it even under stress.

When should you write the asserts? Write them first!

1. Where should you start building a system? - With stories you want to be able to tell about the finished system
2. Where should you start writing a bit of functionality? - With the tests you want to pass with the finished code
3. Where should you start writing a test? - With the asserts that will pass when it is done.
Testing assert-first, it can have a powerful effect. When you're writing a test you are solving several problems at once, even if you no longer have to think about the implementation. I'm going to do some of the FizzBuzz kata assert first so you can see what I mean:


````
require 'minitest/autorun'

class TestFizzBuzz < MiniTest::Unit::TestCase
  def setup
    @fizzbuzz = FizzBuzz.new
  end

  def test_one_is_one
    assert_equal "1", @fizzbuzz.convert(1)
  end
end
````

Even though I have no production code yet, I want convert(1) to equal "1".


````
require 'minitest/autorun'
class TestFizzBuzz < MiniTest::Unit::TestCase
  def setup
    @fizzbuzz = FizzBuzz.new
  end

  def test_one_is_one
    assert_equal "1", @fizzbuzz.convert(1)
  end
end

class FizzBuzz
  def convert(input)
    "1"
  end
end
````

Now my test will pass, and my assertion will be correct. Let's see what will happen when I try to run convert(2)


````
require 'minitest/autorun'
class TestFizzBuzz << MiniTest::Unit::TestCase
  def setup
    @fizzbuzz = FizzBuzz.new
  end

  def test_one_is_one
    assert_equal "1", @fizzbuzz.convert(1)
  end

  def test_two_is_two
    assert_equal "2", @fizzbuzz.convert(2)
  end
end

class FizzBuzz
  def convert(input)
    "1"
  end
end
````

Now, my test is going to fail for test_two_is_two because we have convert(input) returning "1". To make this pass, I'll set up a guard clause


````
require 'minitest/autorun'
class TestFizzBuzz << MiniTest::Unit::TestCase
  def setup
    @fizzbuzz = FizzBuzz.new
  end

  def test_one_is_one
    assert_equal "1", @fizzbuzz.convert(1)
  end

  def test_two_is_two
    assert_equal "2", @fizzbuzz.convert(2)
  end
end

class FizzBuzz
  def convert(input)
   return "1" if input == 1
   "2"
  end
end
````

My tests are now both passing, and I'm just taking small incremental steps to do so. Let's see if passing in 3 will return "fizz".


````
require 'minitest/autorun'
class TestFizzBuzz << MiniTest::Unit::TestCase
  def setup
    @fizzbuzz = FizzBuzz.new
  end

  def test_one_is_one
    assert_equal "1", @fizzbuzz.convert(1)
  end

  def test_two_is_two
    assert_equal "2", @fizzbuzz.convert(2)
  end

  def test_three_is_fizz
    assert_equal "fizz", @fizzbuzz.convert(3)
  end
end

class FizzBuzz
  def convert(input)
   return "1" if input == 1
   "2"
  end
end
````

This is going to return "2", because we don't have any code to return fizz for 3 yet, let's go ahead and write some.


````
require 'minitest/autorun'
class TestFizzBuzz << MiniTest::Unit::TestCase
  def setup
    @fizzbuzz = FizzBuzz.new
  end

  def test_one_is_one
    assert_equal "1", @fizzbuzz.convert(1)
  end

  def test_two_is_two
    assert_equal "2", @fizzbuzz.convert(2)
  end

  def test_three_is_fizz
    assert_equal "fizz", @fizzbuzz.convert(3)
  end
end

class FizzBuzz
  def convert(input)
   return "1" if input == 1
   return "2" if input == 2
   "fizz"
  end
end
````

I made this pass by putting in another guard clause, but now it's time to refactor, because of red/green/refactor, the mantra of TDD.


````
require 'minitest/autorun'
class TestFizzBuzz << MiniTest::Unit::TestCase
  def setup
    @fizzbuzz = FizzBuzz.new
  end

  def test_one_is_one
    assert_equal "1", @fizzbuzz.convert(1)
  end

  def test_two_is_two
    assert_equal "2", @fizzbuzz.convert(2)
  end

  def test_three_is_fizz
    assert_equal "fizz", @fizzbuzz.convert(3)
  end
end

class FizzBuzz
  def convert(input)
    if input == 3
      "fizz"
    else
      input.to_s
    end
  end
end
````

This will make all our tests pass, and now we've eliminated the guard clauses. Try out the rest of the kata and see what you get, the rules are,

1. Numbers divisible by three return FIZZ
2. Numbers divisible by five return BUZZ
3. Numbers divisible by three and five return FIZZBUZZ
Test Data

What data should you use for test-first tests? You should use data that makes the tests easy to read and follow along with. You're writing tests for an audience, so to speak, they're for whoever else you're working on the project with, if it's open source, they're for whoever might want to make adjustments to your code, or they're for future you when you come back six months later and look at the code again. You don't want to scatter data values around for the sake of doing so. If there's a difference in your data it should be meaningful. However, keep in mind, Test Data isn't a license to stop short of full confidence. If your system requires multiple inputs, your tests better reflect that. However, if a list of 3 items as input data leads to the same design and implementation decisions as writing a list of 30 items, just use 3. A trick of test data is to try to never use the same constant to mean more than one thing. The alternative to Test Data is Realistic Data, this is where you'd use data from the real world. This is useful when:

1. You're testing real-time systems using traces of external events gathered from the actual execution
2. You are matching the output of the current system with the output of a previous system (*parallel testing*)
3. You are refactoring simulation and expect precisely the same answers when you are finished, particularly if floating point accuracy may be a problem.
Evident Data

How do you represent the intent of the data? You should include expected and actual results in the test itself, and try to make their relationship apparent. Remember, you're not writing tests for the computer, you're writing tests for the reader. Someone, or like previously said, even future you, might come back two years later, and you don't want them (or you!) to wonder, "What was I doing here?!". Leave as many clues as possible, especially if that frustrated reader might be you. A benefit of evident data is that it makes programming easier. Once you're written the expression in the assertion, you know what you need to program.

## Red Bar Patterns

These patterns are about when you write tests, where you write tests, and when you should stop writing those tests.

## One Step Test

A program written from tests that are "one-step", that is to say, a test that will teach you something, and that you're confident you can implement, can appear to be written top-down, or bottom-up. Top-down because you can begin with a test that represents a simple case of the entire computation. It can appear bottom-up because you start with small pieces and aggregate them larger and larger. However, neither really describes the process. A vertical metaphor is more fitting, it implies growth, a self-similar feedback loop in which the environment affects the program and the program affects the environment. These programs can be described as growing from known-to-unknown, meaning that we have some knowledge and experience on which to draw, and that we expect to learn in the course of development.

## Starter Test

The test that you start with should be a variant of an operation that doesn't do anything. This first question to ask is "Where does this test belong?" Until you can answer that you won't know what to put for the test. Write a realistic test first, and you'll see that you're solving a few problems at once:

  1. Where does the operation belong?
  2. What are the correct inputs?
  3. What is the correct output given for those inputs?
When you begin with a realistic test you might be too long without feedback. In case it's not drilled in your head yet, red/green/refactor! You want that to just take minutes. Shorten the time between feedback by testing for inputs and outputs that are trivially easy to discover. Your starter tests should provide an answer:

  - The output should be the same as the input.
  - The input should be as small as possible
Remember the Fizzbuzz tests up above? We tested an input of 1 should give an output of 1.

## Regression Test

Write the smallest possible test that fails, and that, once run, will be repaired, when a defect is reported. A regression test is a test that, with perfect knowledge, you would have written when coding originally. Whenever you have to write a regression test think about how you could have known to write the test in the first place. You'll also gain value by testing at the level of the whole application. If you have to refactor the code before you can easily isolate the defect, the defect may be the code telling you, "You aren't quite done with me yet!"

## Break

It's important to not get burnt out when testing. When you're tired or stuck take a break. Taking a break is also a good way of solving problems, you might even stand up to take your break and realize how to solve your problem. It's still important to go on that walk, to get some water, or even to take a nap. Your solution won't disappear out of your head, and if you're scared it might jot it down on a sheet of paper. If, after a break, you still don't find a solution, review your goals for the session. Maybe they are no longer realistic, maybe you need to pick new goals. Dave Ungar calls this his Shower Methodology. If you know what to type, then type it. If you don't know what to type, take a shower, and stay in the shower until you know what to type. If you know what to type, type the Obvious Implementation, if you don't know what to type then Fake It. If the right design still isn't clear, then Triangulate. If you still don't know what to type, then you can take that shower.

Remember that positive feedback loop about the stress? Another way to break out of that loop is by introducing a new element

1. When it comes to hours -- Keep a water bottle by your keyboard so that you'll have to take regular breaks
2. When it comes to days -- Commitments after regular work hours can help you to stop when you need to sleep before progress
3. When it comes to weeks -- Weekend commitments help get your conscious thoughts off work (ever taken the weekend off, and immediately your head was swarming with ideas?)
4. When it comes to a year -- Mandatory vacation policies can help you to refresh yourself. The French apparently do this correctly -- two contiguous weeks of vacation aren't enough. You'll spend the first decompressing, and the second week ready to go back to work. Three works, or better yet, four, are necessary for you to be your most effective the rest of the year. However, this isn't always possible or realistic.
Do Over

When you're feeling lost, sometimes the best thing to do is to throw away the code and start over. If you're lost, you've taken the break, taken the shower, taken the vacation, etc, and still are lost, and that code that you wrote that was going so well is now a terrible mess, and to top it off, you've thought of a dozen more tests that you really should implement. It might be time to wash your hands clean of the code, start over, and to set up a goal of what you want to accomplish for this session. Don't forget to take regular breaks, and to practice red/green/refactor. If you're pair programming, switching partners might be a good way to motivate productive do-overs. If you try to explain your complicate mess with your new partner, who isn't invested in the code and the mistakes, might take the keyboard and have a better way to solve the problem that you didn't even think of before.

## Green Bar Patterns

Once you have your broken, red, test, you need to fix it. Treat the failing test as something that needs to be quickly fixed, and you'll see that you can get to passing, green, tests quickly. Use the following patterns to make the code pass, even if it's something you know you won't keep for any length of time.

## Fake It ('Til You Make It)

What is your first implementation once you have a broken test? Return a constant. Once you have your test running, gradually transform the constant into an expression using variables. Fake it is powerful for a psychological reason: having a green test feels different than a red one. When you're passing you know where you stand with the code, and you can refactor in confidence. It also is powerful for its scope control: you can solve the immediate problem, not future ones that may occur. When you implement the next test you can focus on that one, knowing the previous test is guaranteed to work.

## Triangulation

Triangulation is the idea of abstracting only when you have two or more examples, to more conservatively drive abstraction with tests. An example might be if we want to write a method that will return the sum of two integers


````
def test_sum
  assert_equal 4, plus(3,1)
end

def plus(first, second)
  4
end
````

With triangulation to the right design, we write


``
def test_sum
  assert_equal 4, plus(3,1)
  assert_equal 7, plus(3,4)
end

# With this example we can abstract the implemention of plus
def plus(first, second)
  first + second
end
````

Triangulation is attractive because the rules seem very clear. Whereas, the rules for Fake It rely on our sense of duplication between the test case and the fake implementation to drive abstraction might seem a bit vague and subject to interpretation. Although they seem simple, the rules for triangulation cause an infinite loop. Once we have two assertions and we have abstracted the correct implementation for plus we can delete one of the assertions on the grounds that it is redundant, however, if we do that we can simplify the implementation of plus to return just a constant, which requires us to add an assertion. Consider using triangulation when you're really unsure about the correct abstraction.

## Obvious Implementation

The way to implement simple operations is to just do it. Fake It and Triangulate are really tiny steps. Sometimes, though, you are sure you know how to implement an operation, just do it. If you know what to type, and can do it quickly, do it. However, by only using Obvious Implementation, you are demanding perfect of yourself. What if what you write isn't really the simplest change that passes your test? What if your pair shows you an even simpler one? Solving "clean code" at the same time that you solve "that works" can be too much to do at once. As soon as it becomes too much go back to solving and then clean it up at leisure. Keep track of how often you're surprised by the failing test using Obvious Implementation. You want to maintain the same red/green/refactor cycle.

## Wrap Up

I hope these helped give you some guidance with Test Driven Development. If you want to learn more about it, check out the book, Test Driven Development by Kent Beck.

---
title: "Test Driven Development Part 1"
date: 2013-07-15
categories: ["Apprenticeship", "Book Reviews"]
---

The goal of Test Driven Development is to write clean code that works. The reason we want to have clean code that works is because it's a predictable way to develop. With TDD you know when you are finished and you don't have to worry about leaving behind a long bug trail.

Test Driven Development's mantra is red/green/refactor. Red: Write a little test that fails, maybe it doesn't even compile Green: Make the test work quickly, committing whatever sins necessary in the process Refactor: Eliminate all the duplication create in merely getting the test to work

I've been reading the book Test Driven Development: By Example by Kent Beck this week. The first part of the book uses the Money Example to demonstrate TDD. Today, I am going to be doing part of the Coin Changer Kata in Ruby to demonstrate TDD. Let's get started!

First, we need to write our first test before we write any code, remember in the red stage we said the test doesn't even have to compile.


````
describe CoinChanger do
    it "converts 1 cent to 1 penny"
        changer = CoinChanger.new
        changer.convert(1).should == [1]
    end
end
````

This test is going to fail for a few reasons, the first reason is because we don't have a CoinChanger class.

So, we will create our CoinChanger class now


````
class CoinChanger

end

````
We are now getting an error that we don't have a method called convert


````
class CoinChanger
    def convert
    end
end
````

We are now getting an error that we have the wrong number of arguments (1 for 0). Let's add in the parameter now


````
class CoinChanger
    def convert(amount)
    end
end
````

Now the error that we're getting is that it's expecting [1] and it got nil. Remember the second step of TDD, Make tests work quickly committing whatever sins necessary. This means we are going to just have the method return [1]


````
class CoinChanger
    def convert(amount)
        [1]
    end
end
````

Alright, now our first test is passing. We went through the red and then green stage and now we're at the refactor stage. The refactor stage is where we remove the duplication. However, there isn't really duplication here yet, so let's write our next simplest test and then we can get rid of the duplication.


````
..
it "converts 2 cents to 2 pennies" do
    changer = CoinChanger.new
    changer.convert(2).should == [1, 1]
end
````

The red message we're getting says that it's expecting to get [1, 1], but it's getting [1]. Now, let's make this test pass, quickly!


````
class CoinChanger
    def convert(amount)
        return [1] if amount == 1
        [1,1]
    end
end
````

This makes the test pass, however, if we were to keep going down this path we'd have to set up guard clauses for every single number, that'd be ridiculously unclean code. So, we're going to do a little refactoring, making sure to run our tests after to be sure it's still passing. Alright, so first we know that we want to return an array of values. The easiest way to do this would be to define an empty array and then push our amount into it, 1 penny for every cent.


````
class CoinChanger
    def convert(amount)
        coins = []
        amount.times do
            coins << 1
        end
        coins
    end
end
````

So, we run our tests again and we are still passing so we can move onto the next test. We're going to test for 5 cents to a nickel. I've also refactored the test down.


````
describe CoinChanger do
    let(:changer) { CoinChanger.new }
..

    it "converts 5 to 1 nickel" do
        changer = CoinChanger.new
        changer.convert(5).should == [5]
    end
end
````

It is expecting [5] and got [1, 1, 1, 1, 1] Let's fix that now.


````
def convert(amount)
    coins = []
    if amount == 5
        coins << 5
    else
        amount.times do
            coins << 1
        end
    end
    coins
end
````

Now our tests are passing! Let's test the next simple test.


````
it "converts 6 cents to 1 nickel and 1 penny"
    changer.convert(6).should == [5, 1]
end
````

Expected [5, 1], and it got [1, 1, 1, 1, 1, 1]. Let's make this test pass.


````
def convert(amount)
    coins = []
    if amount >= 5
        coins << 5
        amount -= 5
    end
    else
        amount.times do
            coins << 1
        end
    end
    coins
end
````

Our tests are now passing.

The rest of the code is pretty straight forward, and the purpose of this post is not to complete the Kata, but rather to demonstrate TDD. Post your solutions in the comments, I'd love to see how you solved this problem, it doesn't have to be in Ruby, either.

## To Review

The mantra of TDD is red, green, refactor. Write the simplest test, and then write the absolute simplest code to get it to pass, just do it quickly, the test will tell you very quickly if it's right or wrong, so don't think about it too hard. We want to take little steps so we are confident in the code. After we have the passing tests then we go back and remove the duplication, little by little, testing as we go along to make sure everything is still passing. Code written with TDD, that has near 100% statement coverage is code that a developer can have confidence in.

---
title: "Array Manipulation"
date: 2013-04-17
categories: ["Ruby", "Code", "Tutorials"]
---

Today I've been working on my Tic Tac Toe with rSpec and Cucumber. It's my second day attempting to implement BDD alongside the TDD.
I'm finding that I'm having a much easier time figuring out which step to take next, which was my biggest hurdle last week. Not only was I
having trouble figuring out which step to take next, I wasn't even sure how to search for what I wanted to do because I simply just didn't know.
My thought process seems to have taken a 180 degree turn, or at least, it's starting to make the 180 degree turn. Today I learned about how to manipulate arrays and
even though I've read about it before, or even seen it in practice and implemented it, it didn't truly click until today. So, today's post is going to be about how to manipulate arrays in Ruby.

### Using the Each Method

Ruby allows you to iterate over an array without the use of a loop. The usual way to iterate over an array was you'd need to get the length so you could find out how many times to iterate over the array. The <code>for loop</code> gives an index, you'd use that to access the next element in the array.
{% highlight ruby %}
a = ["1", "2", "3", "4"]

for i in 0..(a.size - 1)
    puts a[i]
end
{% endhighlight %}

However, you can use the <code>.each</code> method and write this instead
{% highlight ruby %}
a = [1, 2, 3, 4,]
a.each {|num| puts num}
{% endhighlight %}
This is much more expressive, and easier to read than the for loop.

### Using the Collect Method

The <code>.collect</code> method iterates over the array, collects the result of the block, and stores the result in a new array.
It's less to type and it's more expressive than using <code>.each</code>. If you're new to Ruby and stumbled on this page, you might be thinking what I thought when I'd read blog posts about Ruby when I was just getting started: "Yeah, that's great and all, but *how* do I implement it in my code?!"

Let's start with an example that might be a little bit more familiar and easier to read/understand.
If you want to transform each element of an array, your first thought is probably to use the <code>.each</code> method, makes sense, right?

If you wanted to change an array of words and make them all uppercase, it *is* possible with <code>.each</code>:

{% highlight ruby %}

lowercase = ["yes", "this", "is", "an", "example"]
uppercase = []
lowercase.each do |word|
    uppercase << word.upcase
end

print upper_case
#=> ["YES", "THIS", "IS", "AN", "EXAMPLE"]
{% endhighlight %}

Basically, what the above code does is this: every element of the array is looped through and transformed <code>(word.upcase)</code>, however, you need to use the shovel operator <code><<</code> to make a new array with the newly transformed elements.
Ruby can do this for you using the collect method

Check it out:

{% highlight ruby %}
lowercase = ["yes", "this", "is", "an", "example"]
uppercase = lowercase.collect {|word| word.upcase}

#=> ["YES", "THIS", "IS", "AN", "EXAMPLE"]
{% endhighlight %}

That is obviously much more readable and expressive code.

### Using the Select Method

Sometimes you only want to select certain items from an array that fufill a certain criteria.
For example, if you only want to find the numbers divisible by 3 in an array that has numbers from 1 to 15, you *could* do it like this:
{% highlight ruby %}
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
div_by_three = []

numbers.each do |num|
    div_by_three << num if num%3 == 0
end

#=> [3, 6, 9, 12, 15]
{% endhighlight %}

The above code is looping through each element and checking to see if the number is divisible by three. If the number meets that criteria, it adds it to the array <code>div_by_three</code>.
It works, yes, but there are *better* ways to get to this conclusion.

{% highlight ruby %}
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
div_by_three = numbers.select {|num| num%3 == 0}

#=> [3, 6, 9, 12, 15]
{% endhighlight %}

Isn't that so much nicer? There's no need to create a new array first, no ifs, just simple, readable, expressive code. Are you sensing a theme here, yet?

### Using the Reject Method

Sometimes you need to do the opposite of the <code>.select</code> method, you need the elements that **don't** meet the criteria you set.
The way <code>.reject</code> works is simply by rejecting items which meet the criteria, leaving you with elements that don't. Let's now reject the numbers that are divisible by three.

{% highlight ruby %}
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
div_by_three = []

numbers.each do |num|
     div_by_three << num unless num%3 == 0
end

#=> [1, 2, 4, 5, 7, 8, 10, 11, 13, 14]
{% endhighlight %}

Once again, we have to create an empty array first, then use the shovel operator, and use <code>unless</code> instead of <code>if</code>, still can you guess why this isn't the best way to do this?
If you said it's not very readable or expressive enough, you are catching on.

{% highlight ruby %}
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
div_by_three = numbers.reject {|num| num%3 == 0}

#=> [1, 2, 4, 5, 7, 8, 10, 11, 13, 14]
{% endhighlight %}

I'm not even going to say it, you know why this code is *better*.


## Take Away
Ruby gives us some pretty good methods for array manipulation. We went over 'Each', 'select', 'reject' and 'collect' in this post, and those are only some of the methods, but these are the ones that I've found so far to be the ones that are most useful.
I am going to make it my goal to write more readable, expressive code, so it's easier to maintain, you should, too!

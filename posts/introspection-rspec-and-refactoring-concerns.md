---
title: "Introspection RSpec and Refactoring Concerns"
date: 2013-04-10
categories: ["Apprenticeship"]
---



Today has been a very productive day for me! The gears finally started turning and concepts and ideas are really starting to click.  I still have a few gaps in my knowledge, but I have a much better idea of where to start now when it comes to making a Ruby test driven tic tac toe game. It's easy enough to code along with a book, and I find that it is even pretty easy to predict what the next step will be, but I always had a somewhat difficult time taking that knowledge and applying it to whatever I wanted to make.
I remember picking up programming books in the past, or following along with tutorials online and thinking: "That's great, we just made [insert some simple program here], but how do I use this to make [something I actually want to make]?"
I honestly think it all comes down to confidence in myself and with my work, or more so lack of confidence I suppose. I am really new to software development, and it can be pretty intimidating to work with people who are so good at what they do.
I just have to keep reminding myself that these smart people decided I was a good fit, that I could do this, and they believe in me, that's got to mean something.
But, even if I repeat that to myself I have found myself struggling with believing that I am good enough for this -- that I can actually create and work alongside the craftsman. I have really high expectations for myself, that might seem contrary to what I just said, but my lack of confidence probably stems from not meeting my unrealistic expectations that I set for myself time and time again. However, that stops now. This is my documentation, it’s out there for anyone to read, from today on I will not compare myself to others, I will not look only at the big picture and say that it’s impossible, I will break things down, I will figure out where my knowledge stops and supplement it by talking to someone who is willing to help (that’s basically anyone in the office which is awesome), I’ll read a book, or I’ll search for an answer online. I will not give up when the end seems out of reach because in my apprenticeship especially it’s not just about getting to the end goal, it’s about the process and truly understanding how to get something to do what you want it to do.

Okay, I started my blog post saying how productive today was and I meant it, today was a great, productive day for me.
I spent the day in The RSpec Book and worked through the codebreaker program that is in the book. I got to go over RSpec, Cucumber, and some Ruby.
However, my main focus was on just getting a better understanding of RSpec and Cucumber. The codebreaker program definitely has some flaws, first
being that it’s only testing on specific ‘secret’ code, so once you guess it, that’s it. Also, say the secret code is ‘1234’ and you enter in ‘1155’
the return will show that there is a number that is an exact match, and a number that is a match but it’s not in the right spot, that’s obviously not
true, 1 only appears once in the secret code. The chapter I left off on is hitting on that exact topic, but I don’t really care about the codebreaker
game, I might try to make it work myself if I ever have any downtime. I’m sure I’ll work through more of the book however, I went from not knowing where
to even start with my tic tac toe tests to finally seeing the light and forming some ideas on how to get started. Also, I really like Cucumber a lot,
it reminds me of the way I took notes in high school, which is odd,  it’s very natural for me.

I thought test driven development was going to be very tedious, but I think in the end it will save me a significant amount of time, especially since I am so new to programming. One of the first Python tic tac toe’s I wrote seemed to be working great, the computer either won or tied every time I played it. I was feeling really good about it, but then I won against the computer and nothing happened, the game continued despite me having my three ‘X’s in a row, so I attempted to fix that only to come across another error, then another, I ended up scrapping the whole thing and starting over. If I would’ve taken it one step at a time and ran tests I could’ve caught it before I wrote the entire program and would’ve saved myself a few late nights and unnecessary stress.
I am actually looking forward to starting my game now, I am hoping I can have it done by Friday, and I am actually quite confident that I’ll be able to have some sort of test driven tic tac toe finished. I think refactoring is going to give me a hard time just because my sense for “bad smelling code” isn’t quite as sharp as it will be down the road, I understand why we refactor, I understand that it’s important, I just have a hard time right now deciding when, how, and how far to refactor. I do know that the only way to improve that sense is to just do it so that’s exactly what I intend to do.

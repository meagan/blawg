---
title: "Refactoring Coin Changer Kata"
date: 2014-02-04
categories: ["Code", "Ruby"]
---

A few posts back[ I posted about the Coin Changer kata that I was working on](http://blog.meaganwaller.com/coin-changer-kata). I was wanting to refactor the kata down because I had a lot of repetition. I was able to refactor the kata down quite a bit. Originally it looked like this:

````
class CoinChanger

  def initialize
    @purse = Hash.new(0)
  end

  def make_change(amount)
    if amount >= 25
      change = amount % 25
      @purse[:quarter] = amount/25
      make_change(change)
    elsif amount >= 10
      change = amount % 10
      @purse[:dime] = amount/10
      make_change(change)
    elsif amount >= 5
      change = amount % 5
      @purse[:nickel] = amount/5
      make_change(change)
    elsif amount > 0 && amount < 5
      change = amount % 1
      @purse[:penny] = amount
      make_change(change)
    end
    @purse
  end
end
`````

As you can see in the `make_change` method there is a lot of repetition. When you see lots of repetition like that you can start to pull things out and refactor them. Really the only difference between all the repetition is the value, it's either 25, 10, 5, or 1, and what type of coin that is. To me, it made the most sense to pull this out into a hash like this: ` { :quarter => 25, :dime => 10, :nickel => 5, :penny => 1}` and to make the `make_change` method iterate the hash in one `if` block instead of all of the `if/elsif` blocks I had above. I accomplished this so:

````
class CoinChanger

  def initialize
    @purse = Hash.new(0)
    @coin_values = { :quarter => 25, :dime => 10, :nickel => 5, :penny => 1}
  end

  def make_change(amount)
    @coin_values.each_value do |value|
      if amount >= value
        coin = @coin_values.invert[value]
        @purse[coin] = amount/value
        amount %= value
      end
    end
    @purse
  end
end
````

Another apprentice, Zach, showed me the `%=` operator which is really awesome, because it reassigns the `amount` variable to what the remainder of amount/value is. I had a lot of fun with this kata, and was happy to be able to implement it in a totally different way than I had in the past.

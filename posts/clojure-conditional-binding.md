---
title: "Conditional Binding in Clojure"
date: 2014-04-09
categories: ["Clojure", "Code", "Tutorial"]
---

I've been working on a client project in Clojure for the past month to prepare
me for my upcoming pairing tour and challenges. This project has been a blast to
be on, and I've definitely leveled up my Clojure skills.

One of the stories I picked up this week was for doing some autocompleting based
on some given data in the database. We already had a very specific autocomplete
function for some exisiting autocomplete functionality. Adding the new
autocomplete functionality meant that I was writing additional functions that
looked nearly identical to the existing function, except for a very minor
difference in each function so it would use the correct suggestion function for
the given entity.

When it comes to refactoring I tend to stick to the three strikes and you're out
type of mindset. If I repeat myself twice, that's starting to become a smell,
but extraction isn't quite necessary yet (*this varies on a case-to-case basis,
of course*). However, once I have three functions that look nearly identical,
it's time to make them as general as possible to avoid the duplication and to
DRY up my code. After writing almost the same function for the third time I knew
it was time to extract and generalize.

## Conditional Binding
The actual scenario of autocompletion isn't relevant to the code I'm going to
share today. I found myself wanting to be able to conditionally create a
binding based on an argument the function received. Something that I could do in
Ruby like this:

````
def general-method(arg)
  diff-arg = ""
  if arg == "something"
    diff-arg = "something else"
  elsif arg == "another thing"
    diff-arg = "a new something"
  elsif arg == "the last thing"
    diff-arg = "something different"
  end
  # do something with the value of diff-arg
end
````

This is a pretty contrived example, but this is more of less what I wanted to
duplicate in Clojure. I wanted to give my general function an argument, and
based on that argument it would create a binding that I could use to do
something in my function.

Before I generalized the function I had three functions that looked like this:

````
(defn first-function [args]
 (let [first (first/suggest (:input args))]
  {:status 200 :headers {"Content-Type" "application/json"} :body {:first-entity
first}}))

(defn second-function [args]
 (let [second (second/suggest (:input args))]
  {:status 200 :headers {"Content-Type" "application/json"} :body {:second-entity
second}}))

(defn third-function [args]
 (let [third (third/suggest (:input args))]
  {:status 200 :headers {"Content-Type" "application/json"} :body {:third-entity
third}}))
````

As you can see, these functions look extremely similar to each other, the only
difference between them is that they use different entities to get suggestions,
and have different keywords in the body.

I wanted to create a function that could do something like I did with the Ruby
example above.

````
def general-function (keyword arg)
  entity-suggest = ""
  if keyword == "first"
    entity-suggest = first/suggest
  elsif keyword == "second"
    entity-suggest = second/suggest
  elsif keyword == "third"
    entity-suggest = third/suggest
  end
  body-value = (entity-suggest (:input args))
  {:status 200 :headers {"Content-Type" "application/json"} :body {keyword
body-value}}
end
````

That's roughly what I wanted to be able to do, I wanted to create a new binding
(represented by the `entity-suggest` variable), and conditionally change what
the value was based on the `keyword` argument. After I had that binding I wanted
to use it.

I was able to achieve this in Clojure like so:

````
(defn general-function [keyword arg]
(let [entity-suggest (case keyword
                      :first first/suggest
                      :second second/suggest
                      :third third/suggest)
      body-value (entity-suggest (:input arg))]
      {:status 200 :headers {"Content-Type" "application/json"} :body {keyword
      body-value}}))
````

This allowed me to remove those three functions in favor of this one. The
function that calls this function now uses two arguments instead of one, the
keyword, and the original argument that it always passed in.

## Wrap Up
I'm quite happy with the generalized function. When I first approached Clojure I
was frustrated because I had a difficult time expressing myself, I knew how to
do what I wanted in Ruby but I couldn't translate that over to Clojure.

Learning a new programming language is a lot like learning how to speak a new language.
You know what you want to say in your fluent language, but the translation isn't
one-to-one, so you have to learn new syntaxes and new grammars to do the
translations in your head. I know very little Spanish, but when I took classes
in high school I never got to the point where I was able to hear a Spanish
sentence and know what it meant without having to mentally translate it to
English.

Learning Clojure has been a lot like that, I'd read a function and
immediately try and translate it to Ruby so I had a frame of reference. I'm
finding myself being able to think about things in the scope of Clojure, I don't
have to do those mental translations as often anymore, and I'm more fluent at
speaking about Clojure, and using context clues to figure out what's going on
without having to do the translation to Ruby.

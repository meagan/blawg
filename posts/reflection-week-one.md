---
title: "Reflection Week One"
date: 2013-04-12
categories: ["Apprenticeship"]
---

This is how I've decided to post my Friday postings. It will be broken down into a few sections: **Ways I Grew**, **Challenges I Faced**, **Goals for Next Week**.

##Ways I Grew

This was my first week, so I guess I can just really explain what I mean by "Ways I Grew". It would be difficult to quantify how far I've grown from week to week. I
really mean grow as a direct response to the goals that I'll place for myself. This could also be considered, "Goals I Met", but I think overall "Ways I Grew" is a more accurate depiction in the sense that over the span of my apprenticeship I will be growing by meeting those goals. I plan
on looking back at this blog so it will be nice to have an absolute list of my growth from week to week to see all the progress I've made.

##Challenges I Faced

This week I faced a number of challenges, most of them were challenges that I expected to face, but some I didn't expect. My main challenge
was that of having unrealistic expectations for myself. I set myself up for failure by making success dependent on producing a perfectly crafted TicTacToe
in 5 days. It took me nearly 3 weeks to even get out an unbeatable TicTacToe (I turned in my first version after a week, but it was completely beatable) and that code had no tests, and it was poorly written. I've written on this already
this week in more depth if you're interested in learning more about my frustrations. My other challenges were managing my home and work life, my house keeping, sleep schedule, other work assignments (reading all my book, writing for writing class) all suffered.
I need to remember that logging 60 hour work weeks is basically a guarantee during my apprenticeship. However, I am going to have days like I had yesterday if I over work myself.
The last challenge that I'm going to touch on is being too embarrassed to ask for help, it hindered me and slowed me down to simply try and google my problems when I know if someone would've
explained it to me I would've understood way quicker, and probably with a deeper understanding as well.

##Goals for Next Week
My goals for next week are:

    1. To ask more questions if I need help
    2. Don't let my chores and daily housekeeping routines slip between the cracks
    3. Get more sleep
    4. Read my books in small, manageable chunks
    5. Continue writing a blog post every single day
    6. Write code right away instead of using BDUF (big design up front) use TDD, watch the tests fail, and then try and fix them. Write -> Red -> Green -> Refactor -> Repeat, it's just that simple.

---
title: "Apprenticeship Patterns"
date: 2013-11-26
categories: ["Apprenticeship", "Book Reviews"]
---

The book Apprenticeship Patterns by Dave Hoover and Adewale Oshineye was the book that I needed to read. I wish that I would’ve read it sooner in my apprenticeship because it outlined so many problems that I have, and how to solve them, along with an action plan. I’m moving to Chicago in January to finish up my apprenticeship under Mike Jansen’s mentorship, and he recommended I read this book, and think about which apprenticeship patterns I could apply while I'm in Chicago. It probably would've been more beneficial to write down the patterns that I couldn't apply because I ended up finding value in nearly every single pattern.

## What It Means To Be An Apprentice

Martin Gustafson answered what it means to be an apprentice with,
> “I guess it basically means having the attitude that there’s always a better/smarter/faster way to do what you just did and what you're currently doing. Apprenticeship is the state/process of evolving and looking for better ways and finding people, companies, and situations that force you to learn those better/smarter/faster ways”

I really like this answer for what it means to be an apprentice. I've never really thought about what it means to be an apprentice beyond what it means to be an apprentice at 8th Light, not as an apprentice in general. I think that the apprenticeship at 8th Light definitely falls in this definition, as well as other things that we do at 8th Light, like housekeeping, blog posts, reading, and more, but those things still fall in line with the definition, when we write blog posts and read books we're bettering ourselves, we're learning more, we're evolving and looking for better ways to solve problems.

Since the beginning of my apprenticeship, my tagline has been “My Journey to Craftsmanship”, and that’s exactly what an apprenticeship is. In an apprenticeship you benefit from your peers, and the craftspeople in the office, however you must also learn to grow on your own, and it’s imperative to learn how you learn so you can progress in the best, most effective way possible. The focus on yourself and your need to grow is the pinnacle of what it means to be an apprentice.


## Emptying the Cup

An apprenticeship pattern, according to the book:
> “[An apprenticeship pattern] attempts to offer guidance to someone working with the craft model on the ways in which they can improve the progress of their career.”

The authors of this book have used personal experiences, and the personal experience of other people who have underwent apprenticeships, to pick out and shape the patterns. I really like the premise of “Emptying the Cup”, this chapter started off with the quote: *“Can’t you see the cup is already full and overflowing?” This notion of “emptying the cup”* comes from the story of a Zen master who was visited by a young philosopher, the philosopher and the master sat under a tree and talked.

Every time the master would try to describe something, like his meditation techniques, or how to be attuned to nature and the universe, or how he saw humor in every situation, and every single time the young philosopher would interrupt him to tell the master about his experience, or to tell the master about what he knew already. The master would patiently wait for the young philosopher to end his exciting explanations and then would continue with what he was saying. The master invited the philosopher in for tea, and when the master was pouring tea for the philosopher, and the master continued to pour the tea in the cup, to the point that it was overflowing, at this point the philosopher exclaimed:
> “Stop pouring! Can't you see the cup is already full and overflowing?”.

The master then put the teapot back, and said, “If you come to me with a cup that is already full, how can you expect me to give you something to drink?”.

The reason I really like this story is because it really describes the right mindset for an apprenticeship. If you come into with a lot of experience, you need to exert more effort to “empty your cup”, this includes ridding yourself of bad habits, setting aside pride, and opening yourself up to learning new things from your colleagues. I’m fortunate in this regard, since I came into the apprenticeship with almost no prior experience, I mean no prior experience, I didn’t even know what it meant to “test” something when I started, and I had only been programming for about 3 weeks when I accepted my residency.


## Applying Apprenticeship Patterns

Like I said before, it would’ve been easier to write down the patterns I couldn’t find a way to apply while I was in Chicago. I’m not going to go over every pattern, I’ll go over some of my favorites, and then you should definitely buy the book and explore it if you’re interested in learning about the other awesome patterns.

## The White Belt

To wear the white belt you need to have a deep understanding of your first language, you’re comfortable with it, and your colleagues recognize your abilities and sometimes call on you to help them solve problems. You will generally have a sense of pride in your skills at this level.

However, when you’re wearing the white belt things aren’t just pride and comfort, you’re struggling to learn new things, and it seems harder than before to acquire new skills, unfortunately it feels like your self-education seems to be slowing down despite your best efforts, you feel like your personal development has stalled. How do we remedy this? We remember that “wearing the white belt is based on the realization that while the black belt knows the way, the white belt has no choice but to learn the way”. The benefit of adopting this mindset is that when you’re learning a new language, tool, or business domain is that you are open to learning how to express yourself idiomatically, thus smoothing your communication with the existing community. What this means is that you’ll gain a deeper understanding of the new knowledge, you avoid the old problem of “writing Fortran in any language”.

I can apply the white belt by finding an opportunity to unlearn something, and ideally this will be something that forces me to put aside your previous experience. I can specifically apply this by rewriting programs, or katas in a language that uses a different paradigm than I’m used to.



## Unleash Your Enthusiasm:

Unleashing Your Enthusiasm is for when you find yourself holding back, despite having an insatiable excitement and curiosity regarding the craft of software development, because you’re conscious of how much more enthusiasm you have for the work than your colleagues do. This definitely hit home for me. Sometimes I stop myself from gushing on twitter, distracting my co-workers, and instead talk my boyfriend’s ear off when I get home.

How do I stop myself from holding back? Should I stop myself? The solution seems to be to not allow anyone to dampen your excitement for the craft, because it’s a precious commodity and will accelerate your learning. However, since most software developers work as part of a team, and as group settings go there is a tendency to conform to the norm. Since teams as a whole on average aren’t usually super passionate or enthusiastic about technology. Enthusiastic apprentices might repress their enthusiasm altogether, or allow it to manifest only outside of their day jobs. But, when it comes down to it unleashing your enthusiasm is one of the relatively few responsibilities of the apprentice.

> “Craftsmen learn from the apprentices, even as the apprentices learn from them. Enthusiastic beginners not only renew the craftsmen, but also challenge the craftsmen by bringing in new ideas from the outside. A well chosen apprentice can make even a master craftsman more productive” — Pete McBreen

I’ve often had an idea but kept it to myself, or stopped myself from being too excited about something. While I’m in Chicago I will try and seek out people and describe my ideas, or describe my thoughts, I can use that opportunity to see if they see flaws, and figure out ways to improve it with the help of others.


## Record What Your Learn:

A part of the 8th Light apprenticeship is that you’re supposed to write daily blog posts. I have been terrible at keeping up this, and I need to reconcile and get in the habit of doing that. This pattern is something that could help me be better about writing my daily blog posts.

Recording what you learn is for when you keep relearning the same lessons over and over again, because they never seem to stick. This is for when you find yourself doing similar things in the past, but the exact details escape you. And as the saying goes, “Those who do not learn from history are doomed to repeat it” The solution is to blog more. A record of the lessons I’ve learn can help other apprentices who will come through the program.

I can’t even count how many times I’ve found the answer to a problem I was having on a former 8th Light apprentice’s blog. I’ve started trying to do this some, if I solve a problem that I couldn’t find the answer to, or find myself doing a thing over and over again, I’ll blog about it because if I’m having this problem there is probably someone else having the same problem.


## Wrap Up

I really enjoyed the Apprenticeship Patterns book, look I said earlier. There are tons more patterns, and I could’ve made this post way longer than I already have. As an apprentice, it’s important to remember to not be scared to unlearn things to learn things. We should be excited about our craft, and share it with those around us, and we can share it face-to-face, and we should remember to also share it with others by recording what we learn.

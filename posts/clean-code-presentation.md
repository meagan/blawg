---
title: "Clean Code Presentation"
date: 2013-12-20
categories: ["Apprenticeship"]
---

On Friday Taryn and I gave a presentation on Clean Code. It was interesting to give the presentation remotely, since I'm still in Tampa, and everyone else was in Chicago, but all in all, I think it went pretty well.

We talked about what makes clean code, we used code examples to illustrate our points, and took turns going over the slides. We ended our presentation with information about code reviews and talked about how we benefited from reviewing each others code.

## What Makes Clean Code?
The overall purpose of the presentation was to talk about what makes code clean. Clean code is code that clearly conveys its purpose. It's simply designed, and thoroughly tested. When code isn't clean, it becomes more and more difficult to maintain. The intent of the design is clear, not just for the writer, but for any future developers who will have to dig into the code base.

### Meaningful Names
An important first measure of clean code is that it has meaningful names. They need to communicate intent, and make sense within the context. They should also be consistent with other names in the code.

````
# Parallel Naming
request_difficulty_level & request_player_type
set_difficulty_level & set_player_type
````

### Functions
Functions should contain only one level of abstraction, and should contain one or two (rarely three) arguments. We should also raise errors when appropriate, meaning we shouldn't return `nil` or error codes.

### Comments
Comments are usually outdated, people don't change them when they update code, and they provide inaccurate descriptions. Our test suite should serve as our documentation because they provide us security that comments don't.

### Formatting
Formatting is a measure of clean code. We should use consistent indentation and vertical spacing, and avoiding super long lines of code. You should also order your methods from most to least important. If you find it difficult to determine the most/least important methods, it probably means your class has more than one responsibility.

### Objects and Data Structures
Objects hide their data behind abstractions, and expose functions that operate on that data, while data structures expose their data and have no meaningful functions. They are virtually opposites. The Law of Demeter also plays a role in this. The Law of Demeter says that objects should have limited exposure to the inner workings of other objects.

````
// Exposing Implementation
public interface Vehicle {
  double getFuelTankCapacityInGallons();
  double getGallonsOfGasoline();
}
````
This example doesn't hide implementation. We shouldn't push our variables through getters and setters, we should expose abstract interfaces that allow its users to manipulate the essence of the data, without having to know its implementation.

````
// Expose Abstract Interface
public interface Vehicle {
  double getPercentFuelRemaining();
}
````
Now we're expressing our data in abstract terms. In this example we don't have a clean about how it gets the percentage of remaining fuel, we just know what the data is, not the implementation.

### Classes
Classes should have only one responsibility, and a small number of instance variables. They should minimize dependencies, and allow for easy extension/modification of key functionality.

### Boundaries
We should encapsulate third-party objects in generic methods to protect code from unexpected changes. We should also write tests for API methods based on how we will use it. Taryn said she added a `get_game` helper to her Sinatra application to return a `Game` object with configuration options captured in sessions. She did this because now if the `Game` inputs change, she only has to update the `get_game` method, rather than the many times that the `Game` object is called in the controller.

### Tests
Our tests should be readable and thorough. We should test only one single concept at a time, this means one assert per test, and they should run independently of each other. The best way to have your tests run independently of each other is to run them randomly. This eliminates the possibility that your tests are dependent on another test in your suite to pass. You can remove duplication from your tests with `before` blocks, and `let` statements.




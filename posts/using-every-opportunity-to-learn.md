---
title: "Using Every Opportunity to Learn"
date: 2013-12-05
categories: ["Learning", "Apprenticeship"]
---

Today I was feeling really happy about my "win" last night with getting the test to pass, and finally wrapping my head around those mock objects. However, there's something important to be learned from what I went through.

When I first started learning to program in Ruby, I was able to easily Google my problems, and since the Ruby community is so incredibly active I was usually able to find an answer that fixed my problem almost immediately. I never had to dig *that* deep to get something to work. And more times than not I found myself having to re-Google the same issues over and over.

Using Google isn't a bad way to supplement my knowledge, but maybe I should apply the DRY principle to it. If I find myself repeating an action, like if I search for a certain error message 3 times, I should refactor. In this case, that means learning something so I can avoid the Google step altogether.

## Exposing My Ignorance
I know that as an apprentice I need to be exposing my ignorance, and finding opportunities to get feedback. I've been trying to do that a lot more lately.

If I find myself staring at the same failing test for an extended period of time I will seek out help from someone. This usually means I'm asking on twitter if it's something not too specific, asking someone in the office, or sending an email to someone in Chicago, usually Mike.

I used to be so scared to do this, I felt like I was bothering people, or worse, that they'd realize I wasn't progressing enough if I was asking *this* simple of a question, (but my imposter syndrome is probably a topic for another blog post). However, I'm trying to conquer that feeling by exposing my ignorance, but learning from the experience.

## Learning from the Experience
There are so many emails that I've started that never got sent because simply running through my steps and my process usually lead me to an answer, or at least down a new path I hadn't considered.

I try and make sure to ask thoughtful, detailed, and direct questions. If I'm sending an email I include Gists of the code I'm having trouble with, along with details about the paths I took, what I'm trying to assert, and the error message I'm getting, if any. I don't want to get 5 replies deep because I had to explain my intentions better, that's why its important to be thorough in the initial message.

> If I'm able to find an answer without sending an email, I open up my Evernote application, and write up a quick note about it.

For example, yesterday I was having all the trouble with my mocks, and the solution was adding in a `setter`. After discovering that, I opened up my Evernote note with notes about debugging Java and I added in "NullPointerException? Check for a setter".

Not only did I write about it there, I also blogged about. Blogging is another way to expose my ignorance and explain what I learned, while also providing a form of documentation. A lot of the answers that I find are because I read something on a blog post, or something sparked while reading a blog post.

I encourage everyone to do something this week to expose their ignorance, and use it as an opportunity to learn. You might even find that simply the act of explaining your process out loud, or writing it up will lead you to the answer.

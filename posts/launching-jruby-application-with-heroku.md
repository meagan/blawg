---
title: "Launching JRuby Application with Heroku"
date: 2014-01-07
categories: ["Ruby", "Java", "Code", "Tutorial"]
---

Launching a JRuby application with [Heroku](http://heroku.com) is a pretty pain free process, I pleasantly discovered this when I launched my [JRuby TTT running with Sinatra](http://jrubyttt.herokuapp.com).

It's really simple to launch a new application with JRuby. Just specify the version of Ruby you want to run, the engine, and the engine version in your `Gemfile`:

# From Scratch
````
ruby '1.9.3', engine: 'jruby', engine_version: '1.7.6'
````

Run `bundle install` with JRuby locally, then commit the results and push to Heroku.

````
$ git add .
$ git commit -m "JRuby on Heroku"
$ git push heroku master
Counting objects: 11, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (6/6), 1.17 KiB | 0 bytes/s, done.
Total 6 (delta 3), reused 0 (delta 0)

-----> Ruby app detected
-----> Compiling Ruby/Rack
-----> Using Ruby version: ruby-1.9.3-jruby-1.7.6
-----> Installing JVM: openjdk7-latest
-----> Installing dependencies using Bundler version 1.3.2
# ...
````

# From Existing Application

If you've already got an application and you want to move it to JRuby you can do that as well.

First you need to install JRuby locally if you haven't, and have rvm use JRuby locally.

````
$ rvm get latest
$ rvm install jruby
$ rvm use jruby
$ ruby -v
jruby 1.7.6 (1.9.3p392) 2013-10-22 6004147 on Java HotSpot(TM) 64-Bit Server VM 1.7.0_45-b18 [darwin-x86_64]
````

Next, after JRuby is installed, add this line to your `Gemfile` to tell bundler that you want the app to run on JRuby under compatability mode with Ruby 1.9.3.

````
ruby '1.9.3', :engine => 'jruby', :engine_version => '1.7.6'
````

Next you're going to want to replace your server with JRuby compatible server since many popular Ruby server libraries rely on C bindings. You need a server that can run your JRuby applications inside of a dyno without exceeding available memory. [Puma](http://puma.io/) is a pure ruby rack server:

````
gem 'puma'
````

We can now deploy to Heroku.

````
$ git add .
$ git commit -m "JRuby on Heroku"
$ heroku create --remote jruby-heroku
````

Now we can deploy by pushing to the `jruby-heroku` branch:

````
$ git push jruby-heroku master
Counting objects: 11, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (6/6), 1.17 KiB | 0 bytes/s, done.
Total 6 (delta 3), reused 0 (delta 0)

-----> Ruby app detected
-----> Compiling Ruby/Rack
-----> Using Ruby version: ruby-1.9.3-jruby-1.7.6
-----> Installing JVM: openjdk7-latest
-----> Installing dependencies using Bundler version 1.3.2
# ...
````


If you have a Rails application you want to move over to JRuby, there are a few additional steps that you can read about on the Heroku post [Moving an Existing Rails App to run on JRuby](https://devcenter.heroku.com/articles/moving-an-existing-rails-app-to-run-on-jruby?preview=1)

Congrats, you are now running JRuby on Heroku! Heroku makes things super simple because a couple years ago they launched the Cedar stack along with the ability to run Java on their platform.  The great thing about using Heroku is that if you've used it for any of your other production application it works the same way, push your code and they take care of the rest. No need to worry about the details of running a new language, leaves you able to work on features and not about deploying or keeping your systems up!


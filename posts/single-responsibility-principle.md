---
title: "Single Responsibility Principle"
date: 2013-10-28
categories: ["Ruby", "Code", "SOLID Principles", "Tutorials"]
---

The SOLID principles are very important principles when it comes to object-orientated programming and design. The principles, when applied correctly and together help to create systems that are easier to maintain and extend over time. The principles of SOLID are guidelines that can be applied while working on software to remove code smells by showing the programmer when and where to refactor code until it is both readable and extensible.

This post is going to be about the S in SOLID. The S stands for Single Responsibility Principle, or SRP. SRP states that a class or module should have only a single responsibility, and that responsibility should be entirely encapsulated by that class. All its services should be narrowly aligned with that responsibility.

Example of SRP from my TTT Gem:
````
module TicTacToe
  class PlayerFactory
    def self.create(input)
      case input[:type]
      when :ai
        AI.new(input[:mark])
      when :human
        Human.new(input[:mark])
      else
        raise "Invalid Player Type"
      end
    end
  end
end
````

This is the PlayerFactory from my TTT Gem. It's only responsibility is to create Players, with an AI or a Human player.

  There are some things we can be aware of that will help us to better follow and adhere to this principle. For instance, our classes should be small. This doesn't necessarily mean that they should have as few lines of code as possible. When it comes to measuring classes, we count responsibilities, not line numbers. Our class name should also describe the responsibility of the class. If you can't think of a concise name that describes the class, it's probably time to break it out into other classes. Another indicator is that we should be able to write a brief description of a class in about 25 words without using the words, "if", "and", "or", "but".

Our classes should also have a small number of instance variables, and each of the methods of a class should manipulate one or more of those variables. Generally, the more variables a method manipulates the more cohesive that method is to its class. A class in which each variable is used by each method is maximally cohesive. When cohesion is high it means that the methods and variables of the class are codependent and hang together as a logical whole.  When we maintain cohesion the result is many small classes. Just the act of breaking large methods into smaller methods causes this to happen. We can promote parameters to instance variables of the class to extract the code without passing variables at all. This would make it easy to break the function up into small pieces. Unfortunately, this means our classes lose cohesion because they accumulate more and more instance variables that exist solely to allow a few functions to share them. But, if there are a few functions that want to share certain variables wouldn't it make sense to split them into their own class? Yes! When classes lose cohesion, it's time to split them. By breaking a large function into many smaller functions it often gives us the opportunity to split several classes out as well. This gives our program a much better organization and a more transparent structure.

 This idea shouldn't be that groundbreaking or unexpected, yet it's one of the principles that gets overlooked, or abused the most. The reason for this is because we tend to have a limited amount of room in our heads, so we focus on getting our code to work more than we focus on organization and cleanliness, which is totally fine. Maintaining a separation of concerns is just as important in our programming activities as it is in our programs. The issue is that many developers think that once the program works the work is complete, and they fail to switch to the other concern of organization and cleanliness. Maybe many fear that a large number of small, single purpose classes make it more difficult to understand the bigger picture. They might be concerned because they must navigate from class to class in order to figure out how a larger piece of work gets accomplished. But, a system with many small classes has no more moving parts than a system with a few large classes, there would be just as much to understand and learn. So, the question to answer is: Do you want your tools to be organized into many drawers making it easier to find what you're looking for, or do you want your tools to be shoved into a few big drawers that require you to dig around to find what you're looking for?

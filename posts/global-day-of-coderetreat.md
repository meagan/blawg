---
title: "Global Day of Code Retreat"
date: 2013-12-14
categories: ["Apprenticeship"]
---

Today was the Global Day of Code Retreat 2013! We held one at the Tampa office, and Justin and I facilitated it. I had a lot of fun, and it was cool to see a code retreat from a new perspective, as I'd only participated in the code portion in the past.

My first code retreat was a few months at Ruby DCamp, which was such an amazing Unconference, and I can't wait to hopefully attend it next year. The second retreat I attended was in Chicago before SCNA, although I didn't get to participate in all the sessions, it was still a fun experience.

We had about 25 attendees, and had people from a few different walks of code-life. We had Ruby programmers, C#, Java, and Python programmers. We also had quite a few people who didn't really test in their daily code ventures, so the concept of TDD was interesting to introduce to those people.

After Justin explained the goal of code retreat, stressing that there was no failure state, and that the goal isn't to finish the game but instead to write reall y clean code, we kicked things off.

If you've been to a code retreat you know that every 45 minute session a new constraint is introduced. Our first constraint was just that you had to pair. A lot of our code retreaters don't pair in their day-to-day jobs, or even at all, so that constraint was more than enough to get some out of their comfort zones.

We introduced other constraints throughout the day like: ping-pong pairing, mute pairing, small methods, functional, no array literals. It was interesting to see how everyone approached the same problem with new constraints.

Justin and I didn't have much time to prepare for the facilitation of the event, but I think it went pretty well, everyone who attended seemed to really be enjoying it, and stuck around to chat and help clean up, and discuss code for over an hour afterwards, even though the A.C was no longer running, and they had been coding for about 6, 45 minute, intensive coding sessions. I wasn't even the one coding and I was exhausted at the end of it.

However, I think next year I'd like to attend again, but probably as a code retreat-er and not a facilitator. Towards the middle of the retreat I was wanting to open my laptop and code the Game of Life with our attendees.

(Get more information about the [Global Day of Code Retreat on their website](http://globalday.coderetreat.org/))


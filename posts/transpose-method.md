---
title: "Transpose Method"
date: 2013-04-18
categories: ["Ruby", "Code", "Tutorial"]
---

While building my tic tac toe board I came across a method that definitely came in handy. It's the <code>transpose</code> method.
Transpose assumes that <code>self</code> is an array of arrays and transposes the rows and columns. This obviously comes in handy when writing a tic-tac-toe game.

### board.rb
{% highlight ruby %}
class Board
    def initialize
        @spaces = Array.new(9) {" "}
    end

    #=> [" ", " ", " ", " ", " ", " ", " ", " ", " "]

    def rows
        [ @spaces[0..2],
          @spaces[3..5],
          @spaces[6..8] ]
    #=> [[" ", " ", " "], [" ", " ", " "], [" ", " ", " "]]

    def columns
        rows.transpose
    end

    #=> [[" ", " ", " "], [" ", " ", " "], [" ", " ", " "]]
{% endhighlight %}

### board_spec.rb
{% highlight ruby %}
describe Board do
    let(:board) {described_class.new}

    ...

    describe "check board" do
        before(:each) do
            board.place_mark(0, "X")
            board.place_mark(4, "O")
            board.place_mark(8, "X")
        end

        it 'has rows' do
            board.rows.should == [[ "X", " ", " "],
                                  [" ", "O", " "],
                                  [" ", " ", "X"]]
        end

        it 'has columns' do
            board.columns.should == [[ "X", " ", " "],
                                      [" ", "O", " "],
                                      [" ", " ", "X"]]
        end
{% endhighlight %}

Now that things are really starting to click I am having a lot of fun learning Ruby. As I've said previously, I don't come from a development background, I come from mostly a design background, using HTML/CSS and very limited PHP.
My initial Tic-Tac-Toe was the first program I ever wrote, or really even attempted to write. I'm feeling more and more like a developer every single day.
Tomorrow's 8th Light University should be great, Cory is giving a talk on Test Driven Development, and I will take all the information, help, and tips I can while I'm learning it.
Reading about TDD during my first couple of days definitely made testing seem almost kind of daunting, however I really enjoy writing the tests, and watching them pass (or fail, and then figuring out why). It was frustrating at first because
I didn't have all the details, I knew that things had to be a certain way, but I didn't know why. Now instead of just finding out how to do something, I also try and figure out *how* and *why* it does whatever it does.



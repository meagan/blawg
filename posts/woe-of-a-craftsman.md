---
title: "Woe of a Craftsman"
date: 2013-04-11
categories: ["Apprenticeship"]
---


Honestly, today has been one of those days where it seemed like everything that could go wrong, did. I left yesterday's post on an optimistic note, something I told myself when starting this blog that I was going to do.
    But today I wasn't feeling quite as optimistic as I was yesterday, and that's just the reality that comes with this position I believe. Yesterday I was optimistic because my head was full of ideas on how to get started, on a vague structure of how it should be laid out, an optimism that I could put it all together by Friday.
    Alas, today was one of those days where it felt like I had thumbs for all my fingers. I was making mistakes that I shouldn't have been, mistakes regarding things that hadn't even faltered me recently. I found myself needing to look up simple things just from lack of being able to recall it.
    When I started questioning how to push to GitHub, something I've done probably a hundred times by now, that's when I knew something was up, something was a bit off.
    I'm not sure if it's the lack of sleep from late nights and waking up with the sun or the lack of food from just sheer forgetting because I'd worked straight from my arrival home to nearly midnight, or maybe it's a combination of both. Whatever it is today my brain was in haze.
    I struggled over simple concepts, simple tasks that have become routine, and even just having trouble figuring out what word I wanted to say.
    The fact that I snoozed my alarm clock not 1 but 5 times this morning should have been a good indicator that I wasn't going to be on top of everything today.
    But I still found myself getting frustrated, anxious habits that I haven't found myself doing since college exams or AP tests have reared their head. On the drive home traffic annoyed me, and usually I'm still feeling pleasant and optimistic on my drive home,
    if not I'm usually able to be sharp enough to try and work stuff out in the car that puzzled me at work. Today though I found myself feeling angry on the drive home, not at anything in particular, traffic wasn't even all that bad to be honest.
    I was angry at myself for once again just not getting it. I mean, I get it in theory, I can see it in my head, hell I can explain it my mom who uses the internet for Facebook and online banking. But when I'm staring at a screen that's been sitting with the same describe do block, the same few lines of code, almost feeling like I'm trying to will it appear on the screen.
    If I dig deep enough in my brain the answer might just appear, sometimes it does, however today my digging capabilities must have played hooky, because it wasn't happening.
I took to twitter with my thoughts, maybe someone could validate them for me. I wondered how I could convey my thoughts in 140 characters or less, and this is what came out: "Frustration doesn't really begin to cover it, lots of ideas & expectations, not sure how to get those ideas out, there's not enough hours...".
    Then I went and got food, I put my phone down, I stepped away from all computer screens. I came back to a reply that simply said "The woe of a craftsman".
    That's what I needed to hear -- or I guess read, but still, validation. What I'm feeling is normal, I suppose even the most talented developers hit road blocks, hit places where it's just not going to happen at that moment.
    I wanted to read more stories, stories from people who went through what I'm currently going through so I browsed through the apprentice's blogs on the 8th Light website. When I came across a few blog posts that really hit close to home on (Angeleah's)["http://angeleah.com/blog/] blog.
    It was like I was reading someone explain exactly how I was feeling, I found myself nodding along, getting ideas of how to solve my problems because I was viewing them instead of living them, and the gears started turning again.

I will try and ask for help when I need it. I will remember everyone gets frustrated, so I shouldn't feel bad about being stuck. I will remember that I shouldn't feel lazy for taking a break and stepping away from the computer to collect my thoughts, especially when I spend roughly 8am-12pm, 1pm-5pm, and most of 6-11pm in front of my laptop trying to absorb every bit of knowledge I can.
    I don't want to burn out, I don't think I will, but I don't even want it to even be an option. As I said yesterday, my high expectations are going to hinder me rather than help me, and I need to remember that craftsmanship doesn't happen overnight.
    I need to remember that I'm not going to produce code that is as elegant and polished as a craftsman at this point in my apprenticeship. This is a note to me,
    this is a note to any other future apprentices who are just looking to be validated, to know that someone else knows exactly how they're feeling: The most important thing you can do is to just write code. Even if it doesn't end up doing what you want it to do. Just write it down, test it, and listen to the code and fix it. That's the only way you will develop your nose for sniffing out smelly code.


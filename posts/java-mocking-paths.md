---
title: "Java Mocking Paths"
date: 2013-12-03
categories: ["Java", "Code"]
---

Today I've been revisiting mocks. I've had a lot of trouble wrapping my head around them, especially how to mock System.in/System.out classes. I've gone down some dark paths, and most of the time I hadn't gone down a path but instead stared blankly at my screen waiting for an answer to present itself to me.

Yesterday I sought out help from a fellow apprentice, [Arlandis](http://immutablearlandis.blogspot.com/), and he offered some insight. He suggested that I place a thin wrapper around System.out, something like this:

```
public class Printer implements MyPrinterInterface(){

  @Override
  public void printToScreen(String toPrint){
    System.out.println(toPrint);
  }
}
````
And that made sense to me, but it didn't seem like what I was supposed to do. So, I talked to Mike about it today. I sent him an e-mail detailing my path, and my questions. I wanted to test the `CommandLineInterface` and assert that `displayWelcomeMessage` had the three lines that I was expecting. I was thinking of doing that, with the wrapper above.  If I had the wrapper, I could set up a Mock like so:

````
 public class MyPrinterMock implements MyPrinterInterface() {
     private ArrayList<String> stringHistory = new ArrayList<String>();

      @Override
      public void printToScreen(String toPrint){
      history.push(toPrint);
    }

     public Boolean lastCallWas(String message){
         return history.peek().equals(message);
     }
}
````
And then my question was, how do I assert? What am I asserting? I know I want to check that `displayWelcomeMessage` has three lines of messages for the user with the rules.

````
@Test
public void displaysWelcomeMessage() {
      CommandLineInterface cli = new CommandLineInterface();
      MyPrinterMock printer = new MyPrinterMock();
      assertTrue(printer.lastCallWas(cli.displaysWelcomeMessage().last);
}
````
My frustrations were because I didn't know the proper way to assert this. I felt like I was overthinking or missing something crucial.

After this, Mike and I talked and he told me that yeah, this is a valid approach, but it's not the path I should be taking. I shouldn't be putting a wrapper around `System.out`, but rather mocking `System.out`. He gave me some hints and advice about where to look. Maybe I should try to create a field called `output` that was a `PrintStream` type and create a setter for it like this:
````
public void setOutput(PrintStream output) {
    this.output = output;
}
````
So I can pass in a mock `PrintStream` that extends the `PrintStream` class.

I was feeling like things were starting to become a little clearer, but wasn't sure how to go about mocking `PrintStream`, so I went a little too low-level when I decided to instead mock `OutputStream` after talking with my co-worker Justin.

I came up with something like this:

````
public class MockOutputStream extends OutputStream {
    ArrayList<String> stringHistory = new ArrayList<String>();

    @Override
    public void write(int b) throws IOException {
    }

    public void write(String message) throws IOException {
        stringHistory.add(message);
    }

    public ArrayList<String> getStringHistory() {
        System.out.println("hi");
        return stringHistory;
    }
}
````

and a test that looked like this, set up to fail:
````
public class CommandLineInterfaceTest {
  private CommandLineInterface cli = new CommandLineInterface();
  ..

  @Test
  public void itDisplaysAWelcomeMessage() {
      MockOutputStream out = new MockOutputStream();
      PrintStream printStream = new PrintStream(out);
      cli.setOutput(printStream);
      cli.displayWelcomeMessage();
      assertEquals("", out.getStringHistory());
  }
}
````

However, instead of returning the strings from my `displayWelcomeMessage` method, all I got back was an empty array.  That feeling when you think you're getting somewhere and you think you know what you're going to get and it's totally wrong is super frustrating. I wanted to throw my hands up and give up, but instead I went for a walk.

My goal for this blog post was to have this problem solved tonight, but I've been staring at my screen to no avail and usually the solution doesn't come that way, at least not in my experience. I did however take one more route, and I tried to just mock the `PrintStream` instead.

````
public class MockPrintStream extends PrintStream {

    public ArrayList<String> stringHistory;

    public MockPrintStream(OutputStream out) {
        super(out);
    }

    @Override
    public void println(String message) {
        stringHistory.add(message);
    }

    public ArrayList<String> getStringHistory() {
        return stringHistory;

    }
}
````

And my test looks like this

````
    @Test
    public void itDisplaysAWelcomeMessage() {
        MockOutputStream output = new MockOutputStream();
        MockPrintStream printStream = new MockPrintStream(output);
        cli.setOutput(printStream);
        cli.displayWelcomeMessage();
        assertEquals("", printStream.getStringHistory());
    }
````

Except now I'm getting a NullPointerException, I'm sure it's something obvious that I'm missing, but I need to look at this with fresh eyes tomorrow instead. Hopefully the blog post I'll be writing tomorrow will have an answer to this problem.


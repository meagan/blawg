---
title: "Software Craftsmanship Book Review"
date: 2013-04-22
categories: ["Apprenticeship", "Book Reviews"]
---

On my first week I was given the book Software Craftsmanship by Pete McBreen. I took a little longer to read it than I thought I would, but
it seemed like every time I would sit down to read I would inevitably have to reread the entire page because I just couldn't focus, or I was struggling to keep my heavy eyes open.
However, I finally did get the book finished and am going to be reviewing it today. You can view my presentation below, or feel free to download the presentation <a href="http://www.meaganwaller.com/projects/SoftwareCraftsmanshipPresentation.key">here</a>.
<div class="presentation"><iframe src="http://www.slideshare.net/slideshow/embed_code/19631679?rel=0" width="597" height="486" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC;border-width:1px 1px 0;margin-bottom:5px" allowfullscreen webkitallowfullscreen mozallowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="http://www.slideshare.net/meaganwaller/software-craftsmanshippresentation" title="Software craftsmanshippresentation" target="_blank">Software Craftsmanship Presentation</a> </strong> from <strong><a href="http://www.slideshare.net/meaganwaller" target="_blank">meaganwaller</a></strong> </div>
</div>
I enjoyed reading this book and learning the differences between software engineering and software craftsmanship.
It got me excited to one day become a master craftsman. The book also reminded me that I still have a ways to go, but I now know what traits a master craftsman has.
I learned that one of the most important things for an apprentice to do is to ask thoughtful, intelligent questions. Asking those questions, however, is something I still struggle with, but I have made progress since my first week.




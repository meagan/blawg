---
title: "Pairing Remotely and Tic Tac Toe"
date: 2013-12-09
categories: ["Apprenticeship"]
---


Today was my first time really remotely pair programming with someone. I haven't had the opportunity to do a ton of pairing, so I'm not too well versed in it. Taryn and I are working on an ETL (Extract, Transform, Load) apprentice project together, and since she's in Chicago and I'm in Florida remotely pairing it is!

We are using [TeamViewer](http://www.teamviewer.com/en/index.aspx) to do this, and since Taryn uses Sublime, and I stick to vim (or Eclipse with Java, but with a vim plugin), we decided to use Sublime in Vintage mode, so it has the vim key bindings.

Using TeamViewer is super easy, and the experience wasn't too bad, it was a little bit laggy at times, but I think that has more to do with our internet connections than the software. We both just had to sign up for an account, download the desktop client. Once you open the client and enter your username and password it gives you an ID and password to give to someone else and they can join your session. We just used Skype for the audio, so I'm not sure about those features of TeamViewer. But it got the job done, and worked out alright.

Today I also started working on my Java Tic Tac Toe that Mike wants me to finish this week. It's been a while since I've written Tic Tac Toe, and I'm actually kind of looking forward to it, especially getting to do it another language. I'll probably be sharing a bit about that in the upcoming blog posts. Entirely test driving it out, knowing how to use Mocks, is going to lead me to an entirely different design than my last iterations of it, so it'll be neat to see that play out, as well.

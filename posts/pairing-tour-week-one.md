---
title: "Pairing Tour Week One"
date: 2014-04-28
categories: ["Apprenticeship", "Pairing"]
---

As you might already know if you've been reading my blog, or following me on [twitter](Http://twitter.com/meaganewaller), at [8th Light](Http://www.8thlight.com) we have [apprenticeships](http://www.8thlight.com/apprenticeship). I've been a resident apprentice since [April 8th, 2013](http://meaganwaller.com/posts/my-first-day). During the apprenticeship you are paired with a [mentor](http://www.8thlight.com/team/mike-jansen), and undergo an intense period of learning and coding. For me, I had no software development experience coming into 8th Light. I had done some HTML/CSS and a little bit of PHP/MySQL (I set up quite a few Wordpress sites for myself, friends, and family when I was younger).

Everyone's apprenticeship is a little bit different. The core concepts are the
same, we learn how to write clean, well-tested code. We learn about Agile
methodologies, and from time to time we get to pair on projects with other
apprentices. However, every apprentice goes through roughly the same final
month.

When reaching the end of the apprenticeship we get to "roll our
board". Rolling the board involves being randomly assigned eight craftsmen that
will act as the review board for that apprentice. During the last month of the apprenticeship the
first two weeks are spent on a pairing tour. The pairing tour is when an
apprentice gets to sit down and spend an entire day pairing with each of the people on their
board.

## The First Week
I finished the first week of my pairing tour last week, and started on the
second week of my pairing tour today. Last week I paired with
[Patrick](http://www.8thlight.com/team/patrick-gombert),
[Kevin](http://www.8thlight.com/team/kevin-buchanan),
[Dave](http://www.8thlight.com/team/dave-moore), and
[Mike](http://www.8thlight.com/team/mike-jansen).

So far on my tour I haven't been writing code everyday, but I have gotten to
better understand some of the roles of my co-workers. For example, when I paired
with Dave I attented two IPMs, helped write emails to clients, and learned more
about how our studio projects work. I also got to help with some of the
beginning stages of writing a proposal for a would-be client.

When I paired with Mike we spent a lot of time working on internal things at 8th
Light. Things like making sure all of our clients were properly staffed, and
figuring out how to staff incoming clients. I didn't know much about that
process, so it was really interesting getting to see that side of things. I also
got to go over [my Ruby on Rack
smalltalk](http://www.meetup.com/8th-light-university/events/178849452/) with
him and got some good feedback before actually giving the talk.

When I paired with Patrick and Kevin I got to work on code. It was interesting
because both of the projects were in Ruby, and I had spent the last five weeks in a
Clojure stack. Coming back to Ruby wasn't as difficult as I worried it might be,
and I feel I was able to add value to both of those projects in the limited
amount of time I got to work on them.

## Lessons Learned While Pairing
One of important things I took away from last week's pairing tour was
understanding that not only do we as consultants need to be able to have
knowledge of the language we're writing, whether that's Ruby, Clojure, Java or
some other language, we also need to have knowledge of the business domain of
the client. Having knowledge of the business domain means that we can talk in
terms that are meaningful to our client to ensure we're on the same page. It
also means that when we have a feature we need to build out we can understand
the use case of the feature in a way that is important to the client.

As consultants, we roll on new projects and really need to get up to speed on
what's going on quickly so that we can start adding value right away. This means
understanding that we're not going to get the entire context of things. It's
important to understand that we may not know the entire context of something,
and that's okay.

There isn't always time for deep dives, and we'll never be
able to hold the entire system in our head coming onto the project initially,
and maybe not ever, and that's okay. It's important that we understand the
business domain of our client, so we can wrap our head around what our
immediate tasks are, and maybe what they're touching, but digging into multiple
degrees of separation will just be a rabbit hole that we shouldn't go down.
That's not to say that explorations are out of the question, and that's not to
say that getting clarification on why a feature is important, or what it's use
case is isn't important, because it definitely is, making sure we're on the same
page as our clients is our job as consultants.

## Week Two Starts Today
This week starts the beginning of my second week of my pairing tour. I'll be
pairing with [Myles](http://www.8thlight.com/team/myles-megyesi),
[Arlandis](http://www.8thlight.com/team/arlandis-lawrence),
[Colin](http://www.8thlight.com/team/colin-jones), and
[Chris](http://www.8thlight.com/team/chris-peak). This week will probably be a
bigger emphasis on code and less on administrative things so I'm looking forward
to being able to participate in some [ping-pong
pairing](http://c2.com/cgi/wiki?PairProgrammingPingPongPattern).

---
title: "Ruby Arrays"
date: 2014-01-19
categories: ["Ruby", "Code", "Tutorials"]
---

Ruby Arrays are something a Rubyist works with every day.  They are probably the most used of the Ruby collections, and in fact most methods that operate on collections return an Array as a result, even if it didn't start out as one. Arrays in Ruby operate a bit different than Arrays in other languages because you can make them behave like queues, set, or stacks.

## Creation

To create an Array you can do this:

````
>> vowels = [ "a", "e", "i", "o", "u"]
>> vowels[0]
=> "a"
>> vowels.size
=> 5
````

But an Array doesn't to include consistent data types like the one above that only includes Strings, check it out:

````
>> our_array = [ "a", 1, 1.4567, false]
````

You can also construct an Array like you would any other object

````
>> odds = Array.new([1, 3, 5, 7])
=> [1, 3, 5, 7]
````

You can pass in a size to the Array constructor and it'll create an Array with that many nil objects

````
>> array = Array.new(5)
=> [ nil, nil, nil, nil, nil]
````

But, if you pass `Array#new` a second argument it becomes the fill value instead of nil

````
>> Array.new(5, "a")
=> ["a", "a", "a", "a", "a"]

>> Array.new(3, 0)
=> [0, 0, 0]
````

You can also create an array with %notation

````
>> %W{1 2 3 4}
=> [1, 2, 3, 4]

>> %w{This is an array}
=> ["This", "is", "an", "array"]
````

The `%W` can handle interpolation while the `%w` cannot, see:

````
>> name = "meagan"
>> %W{My name is #{name}}
=> ["My", "name", "is", "meagan"]

>> %w{My name is #{name}}
=> ["My", "name", "is", "\#{name}"]
````

## Indices

Unlike some other languages, in Ruby if you try to access an array index that doesn't exist it returns nil, or if you write to a non-existent index Ruby will just insert nil into the array up to that index:

````
>> months = []
>> months[5]
=> nil

>> months[5] = "june"
=> [nil, nil, nil, nil, nil, "june"]
````

Ruby arrays also let you access negative array indices. Negative indices in Ruby start at the end of an Array, and work backwards towards as they increase:

````
>> [1, 2, 3, 4][-1]
=> 4
>> ["a", "e", "i", "o", "u"][-2]
=> "o"
>> [1, 2, 3][-4]
=> nil
````

## Transforming

In Ruby you can perform lots of different operations on Arrays:

### Adding:

````
>> [1, 2] << 3
=> [1, 2, 3]

>> [1, 2, 3].push(4)
=> [1, 2, 3, 4]

>> [1, 2, 3] << [3, 2, 1]
=> [1, 2, 3, [3, 2, 1]]
````

### Removing:

````
>> array = [1, 2, 3]
>> array.pop
=> 3
>> array
=> [1, 2]

>> array = [1, 2, 3, 4]
>> array.shift
=> 4
>> array
=> [1, 2, 3]

>> array = [ 1, 2, 3, 4]
>> array.delete(1)
=> 1
>> array
=> [2, 3, 4]
>> array.delete_at(2)
=> 4
>> array
=> [2, 3]
````

## Moving:

````
>> [1, 2, 3].reverse
=> [3, 2, 1]

>> [1, 2, 3].rotate
=> [2, 3, 1]

>> [1, 2, 3].rotate(-2)
=> [2, 3, 1]
````

## Removing Duplication

````
>> ["a", "a", "b", "c", "b"].uniq
=> ["a", "b", "c"]
````

## Query

````
>> [1, 2, 3].include?(2)
=> true
>> [1, 2, 3].include?("a")
=> false
````

## Iteration

My favorite thing about Ruby arrays is the iteration. There's no need to write a for loop in Ruby because of how well iteration is handled.

````
>> [1, 2, 3, 4, 5, 6].each { |num| puts num }
1
2
3
4
5
6
````

`#each` is the core iterator in Ruby, but there are so many others:

````
>> [1, 2, 3, 4, 5, 6].reverse_each { |num| puts num }
6
5
4
3
2
1

>> ["a", "b", "c", "d"].each_with_index do |letter, index|
>> puts "#{letter}: #{index}"
a: 0
b: 1
c: 2
d: 3
````

This is just the tip of the iceberg of what you can do with Ruby arrays. Check out the Ruby standard library for more, [I also wrote a blog post on Array manipulation in Ruby a few months back](http://blog.meaganwaller.com/array-manipulation).

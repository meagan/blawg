class Blog < Sinatra::Base
  register Sinatra::Glorify

  get '/style.css' do
    content_type 'text/css', :charset => 'utf-8'
    scss :style
  end

  get '/' do
    erb :index
  end

  get "/blog" do
    @posts = Content.recent("posts").take(9)
    @title = "Blog"
    erb :blog
  end

  get "/archive" do
    count = 10
    @page = params[:page].to_i || 0
    @max_page = Content.recent("posts").count/count
    @posts = Content.recent("posts")[(@page * count).. ((@page * count) + count - 1)]

    erb :archive
  end

  get "/rss.xml" do
    @posts = Content.recent("posts").take(10)
    content_type "application/atom+xml"
    builder :feed
  end

  get "/blog/:slug.md" do
    post = Content.find params[:slug]
    content_type "text/plain"
    "# #{post.title}\n\n#{post.content}"
  end

  get "/blog/:slug" do
    @post = Content.new("posts", params[:slug])
    @title = @post.title
    @formatted_date = @post.formatted_date
    @categories = @post.categories
    erb :blog_post
  end

  get "/code" do
    @title = "Meagan Waller &middot; Code"
    erb :code
  end

  get "/talks" do
    @title = "Meagan Waller &middot; Talks"
    count = 20
    @page = params[:page].to_i || 0
    @max_page = Content.recent("talks").count/count
    @talks = Content.recent("talks")[(@page * count)..((@page * count) + count)]
    erb :talk_index
  end

  get "/talks/:id" do
    @talk = Content.new("talks", params[:id])
    @title = @talk.title
    @formatted_date = @talk.formatted_date
    erb :talk
  end
end

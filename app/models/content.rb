class Content
  class Renderer < Redcarpet::Render::HTML
    def initialize(title, *args)
      @title = title
      super(*args)
    end

    def image(link, title, alt_text)
      unless link.match(/^http|^\//)
        link = "/images/#{@slug}/#{link}"
      end
      "</p><p class='image'><img src='#{link}' title='#{title}' alt='#{alt_text}' /><br />
      <span class='caption'>#{alt_text}</span>"
    end

  end
end

# Adding Ordinalize to Fixnum Class like Rails offers for appending st, nd, rd, th to days.
class Fixnum
  def ordinalize
    if (11..13).include?(self % 100)
      "#{self}th"
    else
      case self % 10
      when 1; "#{self}st"
      when 2; "#{self}nd"
      when 3; "#{self}rd"
      else    "#{self}th"
      end
    end
  end
end


class Content
  attr_reader :name, :title, :date, :categories

  def initialize(content_type, name)
    @name = name
    begin
      content = File.read("#{content_type}/#{name}.md")
    rescue
      return
    end

    match = content.match(/^---$(.*?)^---$(.*)/m)
    unless match.nil?
      meta_data = match[1]
      @content_raw = match[2]

      meta_data = YAML.load(meta_data)

      @title = meta_data["title"]
      @date = meta_data["date"]
      @categories = meta_data["categories"]
    end
  end

  def content
    @content ||= begin
                   renderer = Content::Renderer.new(@name)
                   r = Redcarpet::Markdown.new(renderer, :fenced_code_blocks => true)
                   r.render(@content_raw)
                 end
  end

  def self.find_all(content_type)
    @all_content = Dir.glob("#{content_type}/*.md").map do |ct|
      content = ct[/#{content_type}\/(.*?).md$/,1]
        Content.new("#{content_type}", content)
    end
  end

  def self.recent(content_type)
    all_content = find_all(content_type)
    all_content.reject { |con| con.date > Date.today }
    all_content.sort_by(&:date).reverse
  end

  def truncated(source)
    if source.include?("//speakerdeck.com/assets/embed.js")
      return source.split("\n\n").take(2).join("\n\n")
    else
      return source.split("\n\n").take(3).join("\n\n")
    end
  end

  def formatted_date
    day = ordinalize_day_in_date
    @formatted_date ||= @date.strftime("%a, #{day} %b %Y")
  end

  def full_date
    day = ordinalize_day_in_date
    @full_date ||= @date.strftime("%B #{day}, %Y")
  end

  def content_date
    day = ordinalize_day_in_date
    @content_date ||= @date.strftime("%B #{day}")
  end

  private
  def self.find_all(content_type)
    Dir.glob("#{content_type}/*.md").map do |ct|
      content = ct[/#{content_type}\/(.*?).md$/,1]
        Content.new("#{content_type}", content)
    end
  end

  def ordinalize_day_in_date
    date_string = @date.to_s
    day = date_string.match(/(?<=\d{4}-\d{2}-)(\d{2})/)
    day[1].to_i.ordinalize
  end
end

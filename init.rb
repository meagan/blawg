require 'bundler'
Bundler.require
require 'sinatra'
require 'sinatra/base'
require 'glorify'
require 'sass'
require 'date'
require 'yaml'
require_relative 'helpers'

class Blog < Sinatra::Base
  register Sinatra::Disqus
  include Helpers
  configure do
    root = File.expand_path(File.dirname(__FILE__))
    CONFIG = YAML.load_file "config.yml"
    set :views, File.join(root, 'app', 'views')
  end

  configure :production do
    set :disqus_shortname, "meaganwallerblog"
  end

  configure :development do
    set :disqus_shortname, "meaganwallerblog"
  end

  not_found do
    status 404
    erb :not_found
  end
end

Dir["app/controllers/*.rb"].each { |file| load file }
Dir["app/models/*.rb"].each { |file| load file }

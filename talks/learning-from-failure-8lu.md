---
title: "Learning from Failure - 8LU Small Talk"
date: 2014-07-11
---

I gave a [small talk at 8th Light
  University](http://www.meetup.com/8th-light-university/events/193756312/) on
July 11th.

Failure doesn't have to derail all our progress and productivity. Wouldn't it be
great if instead of viewing failure as something to be avoided we actively
sought out and enjoyed it?

This talk is about how we can learn from our failures, how we can use our
failures to our advantage, and even enjoy them. It will explore why many of us
avoid failure at all costs, and how we can change the way we view and process
failures. We'll discover how we can turn our failure into feedback, and in the
process improve our work conditions for ourselves personally, for those on our
teams, and for future team members.


<script async class="speakerdeck-embed"
data-id="b2c36d10eaeb013123dd0a9d819df929" data-ratio="1.33333333333333"
src="//speakerdeck.com/assets/embed.js"></script>

---
title: "Learning from Failure - GDI Lighting Talk"
date: 2014-06-05
---

I gave a [lightning talk at a Girl Develop It Chicago
meetup](http://www.meetup.com/Girl-Develop-It-Chicago-IL/events/183611992/)
hosted at Dev Bootcamp.

I gave a modified, condensed version of my "Learning from Failure" talk. I
focused on how beginners to the technology field can rethink their failures to
view them not as shortcomings, but as feedback.

<script async class="speakerdeck-embed"
data-id="8f276000cf1d013149f87ab67bbd5fb5" data-ratio="1.33333333333333"
src="//speakerdeck.com/assets/embed.js"></script>

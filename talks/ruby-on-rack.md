---
title: "Ruby on Rack"
date: 2014-04-25
---

I gave a [small talk at 8th Light
University](http://www.meetup.com/8th-light-university/events/178849452/) on
April 25th as part of the final stages of my apprenticeship.

Rack is the minimal, modular, and adaptable interface that Ruby frameworks like
Rails and Sinatra are built on. In this smalltalk we’ll explore what makes Rack
an adaptable interface for building Ruby applications and middleware, and then
walk through a Rack application utilizing custom Rack middleware.

<script async class="speakerdeck-embed"
data-id="2e1775f0aee1013157f056ebe6298001" data-ratio="1.33333333333333"
src="//speakerdeck.com/assets/embed.js"></script>

## Resources

  - [Introducing Rack blog post by Christian
Neukirchen](http://chneukirchen.org/blog/archive/2007/02/introducing-rack.html)
  - [Ruby on Rack demo
application](https://github.com/meaganewaller/ruby-on-rack-example)
  - [Forked Rack with useful puts statements](https://github.com/meaganewaller/rack)
  - [Rails on Rack in the Rails Guides](http://guides.rubyonrails.org/v3.2.13/rails_on_rack.html)
  - [A Quick Introduction to
Rack](http://rubylearning.com/blog/a-quick-introduction-to-rack/)
  - [Sinatra](https://github.com/sinatra/sinatra/)

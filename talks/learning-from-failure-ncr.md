---
title: "Learning from Failure - Nickel City Ruby, Buffalo, NY"
date: 2014-10-03
---

I gave this talk at [Nickel City Ruby](http://www.nickelcityruby.com) in
Buffalo, NY on October 3rd, 2014.

Failure doesn't have to derail all our progress and productivity. Wouldn't it be
great if instead of viewing failure as something to be avoided we actively
sought out and enjoyed it?

This talk is about how we can learn from our failures, how we can use our
failures to our advantage, and even enjoy them. It will explore why many of us
avoid failure at all costs, and how we can change the way we view and process
failures. We'll discover how we can turn our failure into feedback, and in the
process improve our work conditions for ourselves personally, for those on our
teams, and for future team members.

If you're ready to start enjoying and benefiting from your failures, this talk
is for you.

<script async class="speakerdeck-embed" data-id="8680ec702e19013202ad6240ea34932a" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>

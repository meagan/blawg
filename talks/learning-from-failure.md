---
title: "Learning From Failure"
date: 2014-05-16
---

I gave my final presentation of my apprenticeship to my review board on May
16th.

I talked about Learning from Failure, drawing on my personal experiences, and
various studies. I illustrated the toxicity that comes from using the word
"smart" to describe people, and talked about how the label of smart left me
scared to fail. When I came to 8th Light I was expected to fail as a part of my
job everyday in the learning process, and in our TDD cycle (red, green,
refactor/ fail, pass, refactor), I highlighted how I learned to value failures.


<script async class="speakerdeck-embed"
data-id="ca147670c26b013182a006bc48385697" data-ratio="1.77777777777778"
src="//speakerdeck.com/assets/embed.js"></script>

## Resources
 - [The role of practice in the development of performing
   musicians](http://onlinelibrary.wiley.com/doi/10.1111/j.2044-8295.1996.tb02591.x/abstract)
- [The Effort
  Effect](http://alumni.stanford.edu/get/page/magazine/article/?article_id=32124)
- [Ada Lovelace Biography](http://www.biography.com/people/ada-lovelace-20825323)
- [Gravitation and the General Theory of
  Relativity](http://csep10.phys.utk.edu/astr162/lect/cosmology/gravity.html)
- [Apprenticeship Patterns: Guidance for the Aspiring Software
  Craftsman](http://www.amazon.com/Apprenticeship-Patterns-Guidance-Aspiring-Craftsman/dp/0596518382)

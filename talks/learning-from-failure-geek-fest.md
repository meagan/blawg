---
title: "Learning from Failure - Geek Fest"
date: 2014-07-15
---

I gave a presentation at [Geek
  Fest](http://www.meetup.com/Geekfest/events/188313272/) at
[Groupon](http://www.groupon.com) on July 15th.

Failure doesn't have to derail all our progress and productivity. Wouldn't it be
great if instead of viewing failure as something to be avoided we actively
sought out and enjoyed it?

This talk is about how we can learn from our failures, how we can use our
failures to our advantage, and even enjoy them. It will explore why many of us
avoid failure at all costs, and how we can change the way we view and process
failures. We'll discover how we can turn our failure into feedback, and in the
process improve our work conditions for ourselves personally, for those on our
teams, and for future team members.

If you're ready to start enjoying and benefiting from your failures, this talk
is for you.

<iframe src="//player.vimeo.com/video/101338625" width="500" height="281"
frameborder="0" webkitallowfullscreen mozallowfullscreen
allowfullscreen></iframe> <p><a href="http://vimeo.com/101338625">Learning From
Failure by Meagan Waller</a> from <a
href="http://vimeo.com/grouponengineering">Groupon Engineering</a> on <a
href="https://vimeo.com">Vimeo</a>.</p>


<script async class="speakerdeck-embed"
data-id="4d1fa430ef250131c342321b30b95d29" data-ratio="1.33333333333333"
src="//speakerdeck.com/assets/embed.js"></script>
